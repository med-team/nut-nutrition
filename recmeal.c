/* recmeal.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "anameal.h"
#include "recmeal.h"
#include "util.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>

void record_meals()
{
struct food *food_ptr;
struct meal *meal_ptr;
char meal_date[9];
char new_meal_date[9];
char theusual_id[9];
char substring[61];
char buff[61];
char key[61];
char *token, *tokenx;
int meal = 0, modfood, junk, savescreen;
float ratio = 0, one = 1;
key[0] = '\0';
today(meal_date);
for ( ; ; )
 {
 header("NUT:  Record Meals");
 printf("\n\n\n\n\n\nMeal Date:    %s\n\n",meal_date);
 spacer(8);
 printf("\nType meal date (yyyymmdd) or just press <enter> for today's date:  ");
 get_string(new_meal_date,8);
 if (new_meal_date[0] == '-' || new_meal_date[0] == '+') time_machine(new_meal_date);
 if (strlen(new_meal_date) == 8) strcpy(meal_date,new_meal_date);
 if (strlen(new_meal_date) == 8 || strlen(new_meal_date) == 0) break;
 }
if (options.mealsperday > 1)
 {
 header("NUT:  Record Meals");
 printf("\n\n\n\n\n\nMeal Date:    %s\n\n",meal_date);
 printf("Meal Number:\n\n\n\n");
 meal_list(meal_date);
 spacer(15);
 printf("\nType meal number (1 to %d) or just <enter> to quit:  ",options.mealsperday);
 meal = get_int();
 if (meal < 1 || meal > options.mealsperday) return;
 }
if (options.mealsperday == 1) meal = 1;
for ( ; ; )
 {
 for ( ; ; )
  {
  if ((ratio != -383838) && (meal_find(meal_date,meal) != 0))
   {
   header("NUT:  Record Meals");
   if (! meal_show(meal_date,meal))
    {
    printf("\nPress <enter> to continue...");
    junk = get_int();
    }
   else
    {
    printf("\nEnter food name, # to del/chg/pcf, \".\" to analyze:  ");
    get_string(key,60);
    if (strcmp(key,"") == 0)
     {
     delete_meals(options.delopt); 
     if (meal_find(meal_date,meal) != 0) options.temp_meal_root = prev_meal(meal_find(meal_date,meal));
     pcfclear(NULL);
     return;
     }
    }
   }
  if (( strcmp(key,".") == 0 || strcmp(key,",") == 0 )) analyze_meals(prev_meal(meal_find(meal_date,meal)),1);
  else
   {
   modfood = atoi(key);
   strncpy(buff,key,60);
   tokenx = strtok(buff, ", ");
   token = strtok(NULL,", ");
   if (strcmp(key,"") == 0 || modfood == 0) break;
   if (token == NULL)
    {
    if (strchr(tokenx,'p') != NULL) modify_meal(meal_date,meal,modfood,"p");
    else if (strchr(tokenx,'P') != NULL) modify_meal(meal_date,meal,modfood,"p");
    else if (strchr(tokenx,'c') != NULL) modify_meal(meal_date,meal,modfood,"c");
    else if (strchr(tokenx,'C') != NULL) modify_meal(meal_date,meal,modfood,"c");
    else if (strchr(tokenx,'f') != NULL) modify_meal(meal_date,meal,modfood,"f");
    else if (strchr(tokenx,'F') != NULL) modify_meal(meal_date,meal,modfood,"f");
    else if (strchr(tokenx,'t') != NULL) modify_meal(meal_date,meal,modfood,"t");
    else if (strchr(tokenx,'T') != NULL) modify_meal(meal_date,meal,modfood,"t");
    else if (strchr(tokenx,'n') != NULL) modify_meal(meal_date,meal,modfood,"n");
    else if (strchr(tokenx,'N') != NULL) modify_meal(meal_date,meal,modfood,"n");
    else if (strchr(tokenx,'e') != NULL) modify_meal(meal_date,meal,modfood,"e");
    else if (strchr(tokenx,'E') != NULL) modify_meal(meal_date,meal,modfood,"e");
    else if (strchr(tokenx,'l') != NULL) modify_meal(meal_date,meal,modfood,"l");
    else if (strchr(tokenx,'L') != NULL) modify_meal(meal_date,meal,modfood,"l");
    else if (strchr(tokenx,'i') != NULL) modify_meal(meal_date,meal,modfood,"i");
    else if (strchr(tokenx,'I') != NULL) modify_meal(meal_date,meal,modfood,"i");
    else if (strchr(tokenx,'k') != NULL) modify_meal(meal_date,meal,modfood,"k");
    else if (strchr(tokenx,'K') != NULL) modify_meal(meal_date,meal,modfood,"k");
    else if (strchr(tokenx,'z') != NULL) modify_meal(meal_date,meal,modfood,"z");
    else if (strchr(tokenx,'Z') != NULL) modify_meal(meal_date,meal,modfood,"z");
    else modify_meal(meal_date,meal,modfood,token);
    }
   else modify_meal(meal_date,meal,modfood,token);
   options.temp_meal_root = prev_meal(meal_find(meal_date,meal));
   pcfrun();
   strcpy(key,"");
   }
  }
 key_put(key);
 food_ptr = food_choice("NUT:  Record Meals", 1);
 if (food_ptr == (struct food *) -1 && meal_find(meal_date,meal) != 0)
  {
  options.temp_meal_root = prev_meal(meal_find(meal_date,meal));
  savescreen = options.screen;
  options.screen = 0;
  load_foodwork(1,options.temp_meal_root);
  options.screen = savescreen;
  auto_cal();
  }
 if (food_ptr == (struct food *) -1)
  {
  pcfclear(NULL);
  return;
  }
 if (food_ptr == (struct food *)  0) key_clean();
 if (food_ptr == (struct food *)  0) key[0] = '\0';
 if (food_ptr == (struct food *)  0) continue;
 if (food_ptr != (struct food *) -2 && food_ptr != (struct food *) -3)
  {
  header("NUT:  Record Meals");
  if (DVnotOK)
   {
   savescreen = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = savescreen;
   }
  food_show(food_ptr, &one);
  get_qty(&ratio, &(food_ptr->grams), &(food_ptr->nutrient[ENERC_KCAL]));
  if (ratio == -383838)
   {
   key_take();
   strcpy(key,key_take());
   }
  if (ratio != 0 && ratio != -383838)
   {
   if ((new_meal = (struct meal *)malloc(sizeof(struct meal))) == NULL)
    {
    printf("We are out of memory.  Bummer.\n");
    abort();
    } 
   new_meal->ndb_no = food_ptr->ndb_no;
   new_meal->food_no = food_ptr->food_no;
   strcpy(new_meal->meal_date,meal_date);
   new_meal->meal = meal;
   new_meal->grams = food_ptr->grams * ratio;
   order_new_meal();
   options.temp_meal_root = prev_meal(meal_find(meal_date,meal));
   pcfrun();
   }
  }
 if (food_ptr == (struct food *) -2)
  {
  key_decode(substring,key);
  meal_ptr = theusual_choice("NUT:  Record Meals",substring);
  if (meal_ptr == 0) continue;
  strcpy(theusual_id,meal_ptr->meal_date);
  while (strcmp(meal_ptr->meal_date,theusual_id) == 0)
   {
   if ((new_meal = (struct meal *)malloc(sizeof(struct meal))) == NULL)
    {
    printf("We are out of memory.  Bummer.\n");
    abort();
    } 
   memcpy(new_meal,meal_ptr,sizeof(struct meal));
   strcpy(new_meal->meal_date,meal_date);
   new_meal->meal = meal;
   order_new_meal();
   meal_ptr = meal_ptr->next;
   if (meal_ptr == NULL) break;
   }
  options.temp_meal_root = prev_meal(meal_find(meal_date,meal));
  pcfrun();
  }
 if (food_ptr == (struct food *) -3)
  {
  key_decode(substring,key);
  options.temp_meal_root = prev_meal(meal_find(meal_date,meal));
  pcf(meal_date,meal,substring);
  }
 }
}

void today(char *whatever)
{
int c, month = 0;
struct tm *p;
time_t t;
char timestamp[26];
char meal_date[9];
char *thismonth;
char *Months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
time(&t);
p = localtime(&t);
strcpy(timestamp,asctime(p));
meal_date[0] = timestamp[20];
meal_date[1] = timestamp[21];
meal_date[2] = timestamp[22];
meal_date[3] = timestamp[23];
thismonth = timestamp + 4;
for (c = 0; c < 12 ; c++) if (strncmp(thismonth,Months[c],3) == 0) month = c+1;
sprintf(meal_date+4,"%02d",month);
meal_date[6] = timestamp[8];
meal_date[7] = timestamp[9];
meal_date[8] = '\0';
if (meal_date[6] == ' ') meal_date[6] = '0';
strncpy(whatever,meal_date,9);
}

void time_machine(char *whatever)
{
int c, month = 0, time_travel;
struct tm *p;
time_t t;
char timestamp[26];
char meal_date[9];
char *thismonth;
char *Months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
time_travel = 86400 * atoi(whatever);
time(&t);
t += time_travel;
p = localtime(&t);
strcpy(timestamp,asctime(p));
meal_date[0] = timestamp[20];
meal_date[1] = timestamp[21];
meal_date[2] = timestamp[22];
meal_date[3] = timestamp[23];
thismonth = timestamp + 4;
for (c = 0; c < 12 ; c++) if (strncmp(thismonth,Months[c],3) == 0) month = c+1;
sprintf(meal_date+4,"%02d",month);
meal_date[6] = timestamp[8];
meal_date[7] = timestamp[9];
meal_date[8] = '\0';
if (meal_date[6] == ' ') meal_date[6] = '0';
strncpy(whatever,meal_date,9);
}

int reverse_time_machine(char *meal_date)
{
struct tm *p;
time_t t, newtime;
int i;
char year[5]; 
char month[3];
char day[3];
time(&t);
p = localtime(&t);
if (strspn(meal_date,"0123456789") != 8) return ((time_t) ((14486 + atof(meal_date)) * 86400) - t) / 86400;
for (i = 0; i < 4; i++) year[i] = *(meal_date+i);
for (i = 0; i < 2; i++) month[i] = *(meal_date+i+4);
for (i = 0; i < 2; i++) day[i] = *(meal_date+i+6);
year[4] = month[2] = day[2] = '\0';
p->tm_year = atoi(year) - 1900;
p->tm_mon  = atoi(month) - 1;
p->tm_mday = atoi(day);
newtime = mktime(p);
return (int) (newtime - t) / 86400;
}

void today_plus(char *whatever, int *minutes)
{
int c, month = 0;
struct tm *p;
time_t t;
char timestamp[26];
char meal_date[9], hour[3], minute[3];
char *thismonth;
char *Months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
time(&t);
p = localtime(&t);
strcpy(timestamp,asctime(p));
meal_date[0] = timestamp[20];
meal_date[1] = timestamp[21];
meal_date[2] = timestamp[22];
meal_date[3] = timestamp[23];
thismonth = timestamp + 4;
for (c = 0; c < 12 ; c++) if (strncmp(thismonth,Months[c],3) == 0) month = c+1;
sprintf(meal_date+4,"%02d",month);
meal_date[6] = timestamp[8];
meal_date[7] = timestamp[9];
meal_date[8] = '\0';
hour[0] = timestamp[11];
hour[1] = timestamp[12];
hour[2] = '\0';
minute[0] = timestamp[14];
minute[1] = timestamp[15];
minute[2] = '\0';
*minutes = atoi(hour) * 60 + atoi(minute);
if (meal_date[6] == ' ') meal_date[6] = '0';
strncpy(whatever,meal_date,9);
}

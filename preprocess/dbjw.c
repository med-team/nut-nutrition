#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
char line[301];
char unit[3];
char targetstring[20];
char *fields[8], *ndb, *pri, *qty = NULL, *uni = NULL, *gra = NULL, *fdg, *nam;
char priority;
int i, fieldcount, length, oldndb = 0;
float target, prifloat, grams;

strcpy(unit,"oz");
while (fgets(line,300,stdin) != NULL)
 {
 fieldcount = 0;
 length = strlen(line);
 for (i = 0; i < 300; i++) 
  {
  if (line[i] == '^')
   {
   line[i] = '\0';
   fields[fieldcount++] = &line[i]+1; 
   }
  }
 if (*argv[1] != '4')
  {
  ndb = line;
  pri = fields[0];
  qty = fields[1];
  uni = fields[2];
  gra = fields[3];
  fdg = fields[4];
  nam = fields[5];
  }
 if (*argv[1] == '4')
  {
  line[length+1] = '9';
  line[length+2] = '\0';
  line[length+3] = '1';
  line[length+4] = '\0';
  line[length+5] = 'o';
  line[length+6] = 'z';
  line[length+7] = '\0';
  line[length+8] = '2';
  line[length+9] = '8';
  line[length+10] = '.';
  line[length+11] = '3';
  line[length+12] = '5';
  line[length+13] = '\0';
  ndb = line;
  pri = line+length+1;
  qty = line+length+3;
  uni = line+length+5;
  gra = line+length+8;
  fdg = fields[0];
  nam = fields[1];
  }
 switch (atoi(fdg))
  {
  case  100 : target = 8.6;
              if (strncmp(nam,"Beverage",8) == 0) target   =  1.0;
              if (strncmp(nam,"Butter",6) == 0) target     =  0.5;
              if (strncmp(nam,"Cheese",6) == 0) target     =  1.0;
              if (strncmp(nam,"USDA Commodity, cheese",22) == 0) target     =  1.0;
              if (strncmp(nam,"Imitation cheese",16) == 0) target     =  1.0;
              if (strncmp(nam,"Parmesan cheese",15) == 0) target     =  1.0;
              if (strncmp(nam,"Cheese, cottage",15) == 0) target     =  4.0;
              if (strncmp(nam,"Cheese, ricotta",15) == 0) target     =  4.0;
              if (strncmp(nam,"Cream",5) == 0) target      =  0.5;
              if (strncmp(nam,"Cream",5) == 0 && strstr(nam,"powdered") != NULL) target      =  0.1;
              if (strncmp(nam,"Sour",4) == 0) target      =  0.5;
              if (strncmp(nam,"Sour",4) == 0&& strstr(nam,"imitation") != NULL) target      =  1;
              if (strncmp(nam,"Dessert",7) == 0) target      =  0.15;
              if (strncmp(nam,"Dessert",7) == 0 && strstr(nam,"powdered") != NULL) target      =  0.05;
              if (strncmp(nam,"Dessert",7) == 0 && strstr(nam,"prepared") != NULL) target      =  0.15;
              if (strncmp(nam,"Dulce de Leche",14) == 0) target      =  3;
              if (strncmp(nam,"Milk",4) == 0 && strstr(nam,"dry") != NULL) target      =  1;
              if (strncmp(nam,"Milk",4) == 0 && strstr(nam,"canned") != NULL) target      =  1;
              if (strncmp(nam,"Milk, buttermilk, dried",23) == 0) target = .25;
              if (strncmp(nam,"Whey",4) == 0 && strstr(nam,"dried") != NULL) target      =  .16;
              if (strncmp(nam,"Egg,",4) == 0) target      = 1.75;
              if (strncmp(nam,"Egg, yolk",9) == 0) target      = .5;
              if (strncmp(nam,"Egg,",4) == 0 && strstr(nam,"dried") != NULL) target      = .16;
              if (strncmp(nam,"Egg, white, dried",17) == 0) target      = .5;
              if (strncmp(nam,"Egg, goose",10) == 0) target      = 5;
              if (strncmp(nam,"Egg, quail",10) == 0) target      = .33;
              if (strncmp(nam,"Egg Mix",7) == 0) target      = 1;
              if (strncmp(nam,"Egg substitute",14) == 0) target      = 1.75;
              if (strncmp(nam,"Egg substitute, powder",22) == 0) target      = .33;
              if (strncmp(nam,"Cheese, parmesan, shredded",26) == 0) target      = .16;
              if (strncmp(nam,"Cheese fondue",13) == 0) target = 2;
              if (strncmp(nam,"Protein supplement",18) == 0) target = 1.5;
              if (strncmp(nam,"KRAFT",5) == 0) target = 1;
              if (strncmp(nam,"KRAFT BREYERS",13) == 0) target = 8;
              if (strncmp(nam,"Whipped topping",15) == 0) target = .5;
              if (strncmp(nam,"Reddi Wip",9) == 0) target = .5;
              if (strstr(nam,"dehydrated") != NULL && strstr(nam,"prepared with water") != NULL) target = 8;
              break;
  case  200 : target = .1;
              if (strncmp(nam,"Spices, parsley",15) == 0) target = .05;
              if (strncmp(nam,"Dill weed, fresh",16) == 0) target = .05;
              if (strncmp(nam,"Salt",4) == 0) target = .2;
              if (strncmp(nam,"Vinegar",7) == 0) target = .5;
              if (strncmp(nam,"Thyme, fresh",12) == 0) target = .03;
              if (strncmp(nam,"Capers",6) == 0) target = .33;
              if (strncmp(nam,"Spearmint, fresh",16) == 0) target = .33;
              break;
  case  300 : target = 2.5;
              if (strncmp(nam,"Babyfood, dinner",16) == 0) target = 6;
              if (strncmp(nam,"Babyfood, cereal",16) == 0) target = .75;
              if (strncmp(nam,"Babyfood, dessert",17) == 0) target = 1;
              if (strncmp(nam,"Babyfood, juice",15) == 0) target = 4.4;
              if (strncmp(nam,"Babyfood, vegetables",20) == 0) target = 4;
              if (strncmp(nam,"Babyfood, fruit",15) == 0) target = 4;
              if (strncmp(nam,"Child",5) == 0) target = 1;
              if (strncmp(nam,"Fluid",5) == 0) target = 8.8;
              if (strncmp(nam,"Infant",6) == 0) target = 1;
              if (strncmp(nam,"Zwieback",8) == 0) target = .25;
              if (strstr(nam,"cereal") != NULL) target = .75;
              if (strncmp(nam,"Babyfood, cookie",16) == 0) target = .25;
              if (strncmp(nam,"Babyfood, teething",18) == 0) target = .25;
              if (strncmp(nam,"Babyfood, cracker",17) == 0) target = .25;
              if (strncmp(nam,"Babyfood, pretzel",17) == 0) target = .25;
              if (strncmp(nam,"Babyfood, pretzel",17) == 0) target = .25;
              if (strstr(nam,"not reconstituted") != NULL && strstr(nam,"liquid concentrate") == NULL) target = .33;
              if (strstr(nam,"Alaska Native") != NULL) target = 1;
              break;
  case  400 : target = .5;
              if (strncmp(nam,"Salad dressing",14) == 0) target = .75;
              if (strncmp(nam,"Fish",4) == 0) target = .16;
              break;
  case  500 : target = 3;
              if (strncmp(uni,"unit",4) == 0 && *argv[1] == '1') target = 3 * atof(gra) / 28.35;
              if (strncmp(uni,"cup",3) == 0 && *argv[1] == '1') target = 3 * atof(gra) / 28.35;
              break;
  case  600 : target = 9;
              if (strncmp(nam,"Gravy",5) == 0) target = 1;
              if (strncmp(nam,"Gravy",5) == 0 && strstr(nam,"dry") != NULL) target = .5;
              if (strncmp(nam,"Peppers",7) == 0) target = .5;
              if (strncmp(nam,"Sauce",5) == 0) target = 1;
              if (strncmp(nam,"Adobo",5) == 0) target = .66;
              if (strstr(nam,"LIPTON") != NULL) target = .5;
              if (strncmp(nam,"Soup",4) == 0  && strstr(nam,"dry") != NULL) target = .5;
              if (strncmp(nam,"Potato soup",11) == 0  && strstr(nam,"dry") != NULL) target = .5;
              if (strstr(nam,"dehydrated") != NULL) target = .5;
              if (strstr(nam,"dehydrated") != NULL && strstr(nam,"prepared with water") != NULL) target = 9;
              break;
  case  700 : target = 1.5;
              if (strncmp(nam,"Bockwurst",9) == 0) target = 3;
              if (strncmp(nam,"Salami",6) == 0 && strstr(nam,"dry") != NULL) target = 1;
              if (strncmp(nam,"Salami",6) == 0 && strstr(nam,"hard") != NULL) target = 1;
              break;
  case  800 : target = 1.5;
              if (strstr(nam,"water") != NULL) target = 6;
              if (strstr(nam,"prepared") != NULL) target = 6;
              break;
  case  900 : target = 6;
              if (strstr(nam,"raw") != NULL) target = atof(gra) / 28.35;
              if (strstr(nam,"juice,") != NULL) target = 8.5;
              if (strstr(nam,"juice") != NULL && strstr(nam,"undiluted") != NULL) target = 2.1;
              if (strncmp(uni,"cup",3) == 0 && strstr(nam,"undiluted") == NULL) target = 1.5 * atof(gra) / 28.35;
              if (strncmp(nam,"Durian",6) == 0) target = 8.5;
              if (strncmp(nam,"Fruit, mixed",12) == 0 && strstr(nam,"dried") != NULL) target = 2;
              if (strncmp(nam,"Candied fruit",13) == 0) target = 2;
              if (strncmp(nam,"Cranberry sauce",15) == 0) target = 2;
              if (strncmp(nam,"Dates",5) == 0) target = 2;
              if (strncmp(nam,"Raisins",7) == 0) target = 2;
              if (strncmp(nam,"Prune",5) == 0) target = 2;
              if (strncmp(nam,"Jujube",6) == 0) target = 2;
              if (strncmp(nam,"Longans",7) == 0) target = 2;
              if (strncmp(nam,"Litchis",7) == 0) target = 2;
              if (strncmp(nam,"Cranberry-orange relish",23) == 0) target = 1;
              if (strncmp(nam,"Mammy-apple",11) == 0) target = 8;
              if (strncmp(nam,"Soursop",7) == 0) target = 8;
              if (strncmp(nam,"Olives",6) == 0) target = 1;
              if (strstr(nam,"dried") != NULL && strstr(nam,"stewed") == NULL) target = 2;
              if (strstr(nam,"dried") != NULL && strstr(nam,"stewed") != NULL) target = 3;
              if (strstr(nam,"dried") != NULL && strstr(nam,"uncooked") != NULL) target = 2;
              if (strstr(nam,"dehydrated") != NULL && strstr(nam,"stewed") == NULL) target = 2;
              if (strstr(nam,"dehydrated") != NULL && strstr(nam,"stewed") != NULL) target = 3;
              if (strstr(nam,"dehydrated") != NULL && strstr(nam,"uncooked") != NULL) target = 2;
              if (strstr(nam,"banana powder") != NULL) target = 1;
              if (strstr(nam,"frozen") != NULL) target = 3;
              if (strstr(nam,"canned") != NULL) target = 3;
              if (strncmp(nam,"Acerola juice",13) == 0) target = 1;
              if (strncmp(nam,"Lemon juice",11) == 0) target = 1;
              if (strncmp(nam,"Pummelo",7) == 0) target = 7;
              if (strncmp(nam,"Guava sauce",11) == 0) target = 2;
              if (strncmp(nam,"Strawberries, raw",17) == 0) target = 5;
              break;
  case 1000 : target = 3;
              if (strstr(nam,"leaf fat") != NULL) target = 1;
              if (strstr(nam,"breakfast strips, cooked") != NULL) target = 1;
              if (strstr(nam,"breakfast strips, raw") != NULL) target = 2;
              if (strstr(nam,"rendered fat") != NULL) target = 1;
              if (strstr(nam,"jowl, raw") != NULL) target = 1;
              if (strstr(nam,"backfat, raw") != NULL) target = 1;
              if (strstr(nam,"salt pork, raw") != NULL) target = 1;
              if (strstr(nam,"separable fat") != NULL) target = 1;
              if (strstr(nam,"bacon") != NULL && strstr(nam,"raw") != NULL) target = 2;
              if (strstr(nam,"bacon") != NULL && strstr(nam,"cooked") != NULL) target = 1;
              if (strncmp(nam,"Pork, oriental style, dehydrated",32) == 0) target = 1;
              if (strncmp(nam,"Pork, fresh, belly, raw",23) == 0) target = 1;
              break;
  case 1100 : target = 3;
              if (strncmp(nam,"Seaweed",7) == 0) target = atof(gra) / 28.35;
              if (strncmp(nam,"Shallots",8) == 0) target = atof(gra) / 28.35;
              if (strstr(nam,"mashed, dehydrated, granules") != NULL) target = 1;
              if (strstr(nam,"dried") != NULL) target = 1;
              if (strstr(nam,"dry mix") != NULL) target = 1;
              if (strstr(nam,"relish") != NULL) target = .5;
              if (strstr(nam,"dehydrated") != NULL) target = 1;
              if (strstr(nam,"powder") != NULL) target = 1;
              if (strncmp(nam,"Catsup",6) == 0) target = .5;
              if (strncmp(nam,"Yeast extract",13) == 0) target = .33;
              if (strncmp(nam,"Pickles",7) == 0) target = 1;
              if (strncmp(nam,"Lettuce",7) == 0) target = 2;
              if (strncmp(nam,"Potato flour",12) == 0) target = 1;
              if (strncmp(nam,"Onion rings",11) == 0) target = 2.5;
              if (strncmp(nam,"Garlic",6) == 0) target = .33;
              if (strncmp(nam,"Radishes, oriental, dried",25) == 0) target = 1;
              if (strncmp(nam,"Broccoli raab",13) == 0) target = 7.7;
              break;
  case 1200 : target = 1;
              break;
  case 1300 : target = 3;
              if (strstr(nam,"suet") != NULL) target = 1;
              if (strstr(nam,"breakfast strips, cooked") != NULL) target = 1;
              if (strstr(nam,"breakfast strips, raw") != NULL) target = 2;
              if (strstr(nam,"separable fat, raw") != NULL) target = 1;
              if (strstr(nam,"separable fat, cooked") != NULL) target = 1;
              break;
  case 1400 : target = 8.36;
              if (strstr(nam,"frozen concentrate") != NULL) target = 2;
              if (strstr(nam,"powder") != NULL) target = atof(gra) / 28.35;
              if (strstr(nam,"mix") != NULL) target = atof(gra) / 28.35;
              if (strstr(nam,"Mix") != NULL) target = atof(gra) / 28.35;
              if (strstr(nam,"syrup") != NULL) target = atof(gra) / 28.35;
              if (strncmp(nam,"Alcoholic",9) == 0) target = atof(gra) / 28.35;
              if (strncmp(nam,"Coffee, brewed, espresso",24) == 0) target = 2.1;
              if (strncmp(nam,"Alcoholic beverage, beer",24) == 0) target = 12.5;
              break;
  case 1500 : target = 3;
              if (strstr(nam,"salted") != NULL) target = 1;
              break;
  case 1600 : target = 3;
              if (strstr(nam,"raw") != NULL) target = 1;
              if (strncmp(nam,"Peanut",6) == 0) target = 1;
              if (strstr(nam,"Peanut Butter") != NULL) target = 1;
              if (strncmp(nam,"Noodles",7) == 0) target = 2;
              if (strncmp(nam,"Vermicelli",10) == 0) target = 2;
              if (strstr(nam,"flour") != NULL) target = 1;
              if (strstr(nam,"meatless") != NULL) target = 3;
              if (strncmp(nam,"Meat extender",13) == 0) target = 2;
              if (strncmp(nam,"Bacon, meatless",15) == 0) target = 2;
              if (strncmp(nam,"Bacon bits, meatless",20) == 0) target = 2;
              if (strncmp(nam,"Soy milk",8) == 0) target = 8.6;
              if (strncmp(nam,"Soybeans, mature seeds",22) == 0 && strstr(nam,"roasted") != NULL) target = 1;
              if (strncmp(nam,"Tofu, dried",11) == 0) target = 1;
              if (strncmp(nam,"Soy protein",11) == 0) target = atof(gra) / 28.35;
              if (strncmp(nam,"Soy sauce",9) == 0) target = atof(gra) / 28.35;
              break;
  case 1700 : target = 3;
              if (strstr(nam,"separable fat") != NULL) target = 1;
              break;
  case 1800 : target = 1;
              if (strncmp(nam,"Leavening agents",16) == 0) target = .33;
              break;
  case 1900 : target = 1;
              if (strncmp(nam,"Ice creams",9) == 0) target = 3;
              if (strncmp(nam,"Sugars",6) == 0) target = .15;
              if (strncmp(nam,"Honey",5) == 0) target = .15;
              if (strncmp(nam,"Desserts, rennin, tablets",25) == 0) target = .33;
              break;
  case 2000 : target = 2;
              break;
  case 2100 : target = 8;
              if (*argv[1] != '4') target = atof(gra) / 28.35;
              if (strncmp(nam,"Fast foods, nachos",18) == 0) target = 4;
              break;
  case 2200 : target = 8;
              if (*argv[1] != '4') target = atof(gra) / 28.35;
              break;
  case 2500 : target = 1;
              break;
  case 3500 : target = 1;
              break;
  default :   target = 3;
              break;
  }
 if (*argv[1] == '1')
  {
  prifloat = 9 * (atof(gra) / (target * 28.35) - 1);
  if (prifloat < 0) prifloat = 9 * ((target * 28.35) / atof(gra) - 1);
  if (prifloat > 9) prifloat = 9;
  priority = (int) prifloat;
  sprintf(pri,"%1d",priority);
  printf("%s^%s^%s^%s^%s^%s^%s",ndb,pri,qty,uni,gra,fdg,nam);
  }
 if (*argv[1] == '2')
  {
  if (oldndb != atoi(ndb))
   {
   oldndb = atoi(ndb);
   printf("%s^%s^%s^%s^%s^%s^%s",ndb,pri,qty,uni,gra,fdg,nam);
   }
  }
 if (*argv[1] == '3' || *argv[1] == '4')
  {
  if (*pri != '9') printf("%s^%s^%s^%s^%s^%s^%s",ndb,pri,qty,uni,gra,fdg,nam);
  else
   {
   grams = target * 28.35;
   sprintf(targetstring,"%g",target);
   printf("%s^%s^%s^%s^%1.2f^%s^%s",ndb,pri,targetstring,unit,grams,fdg,nam);
   }
  }
 if (*argv[1] == '5') if (*pri == '9') printf("%s^%s^%s^%s^%s^%s^%s",ndb,pri,qty,uni,gra,fdg,nam);
 }
return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../abbrev.h"

int main(int argc, char *argv[])
{
char line[301], dupline[301];
int i, j, a, c, d, abbrevlen[Abbrev_Count], abbrevlist[50][3], linelength;
int cumsave, count = 0;

for (i = 0; i < Abbrev_Count; i++) abbrevlen[i] = strlen(Abbreviations[i]); 

while (fgets(line,300,stdin) != NULL)
 {
 d = c = 0;
 linelength = strlen(line);
 if (linelength <= 60) printf("%s",line);
 else
  {
  count++;
  cumsave = a = 0;
  while (c < linelength)
   {
   for (i = Abbrev_Count - 2; i >= 0; i -= 2) if (strncmp(Abbreviations[i],line+c,abbrevlen[i]) == 0)
    {
    abbrevlist[a][0] = c;
    abbrevlist[a][1] = i + 1;
    abbrevlist[a++][2] = abbrevlen[i] - abbrevlen[i+1];
    c += abbrevlen[i];
    }
   while (line[c] >= 'A' && line[c] <= 'Z') c++;
   while (line[c] <  'A' || line[c] >  'Z') c++;
   }
  for (i = a - 1; i >=0; i--)
   {
   cumsave += abbrevlist[i][2];
   if (linelength - cumsave <= 60) break;
   }
  if (i < 0) i = 0;
  if (linelength - cumsave > 60) fprintf(stderr,"not enough savings... need %d %s\n",linelength - cumsave - 60,line);
  c = 0;
  for (j = i; j < a; j++)
   {
   while (c < abbrevlist[j][0]) dupline[d++] = line[c++];
   dupline[d] = '\0';
   strcat(dupline,Abbreviations[abbrevlist[j][1]]);
   c += abbrevlen[abbrevlist[j][1]-1];
   d += abbrevlen[abbrevlist[j][1]];
   dupline[d] = '\0';
   }
  while (c <= linelength) dupline[d++] = line[c++];
  printf("%s",dupline); 
  if (linelength - cumsave > 60) fprintf(stderr,"Best we can do:  %s\n",dupline);
  }
 }
return 0;
}

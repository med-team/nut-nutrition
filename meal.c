/* meal.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "util.h"
#include "options.h"
#include "db.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct meal theusual_root, *new_theusual, meal_root, *new_meal;
struct meal *pcfmeal[10] = { NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL };
float pcfmatrix[10][12];

void no_op(void) {}

void p_func(void)
{
float change;
change = (DV[PROCNT] - food_work.nutrient[PROCNT]) / pcfmatrix[0][0];
food_work.nutrient[PROCNT] += change * pcfmatrix[0][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[0][1];
food_work.nutrient[FAT] += change * pcfmatrix[0][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[0][3];
food_work.nutrient[VITE] += change * pcfmatrix[0][4];
food_work.nutrient[CA] += change * pcfmatrix[0][5];
food_work.nutrient[THIA] += change * pcfmatrix[0][6];
food_work.nutrient[K] += change * pcfmatrix[0][7];
food_work.nutrient[FE] += change * pcfmatrix[0][8];
food_work.nutrient[ZN] += change * pcfmatrix[0][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[0][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[0][11];
pcfmeal[0]->grams += 100 * change / options.mealsperday;
}

void c_func(void)
{
float change;
change = (DV[CHO_NONFIB] - food_work.nutrient[CHO_NONFIB]) / pcfmatrix[1][1];
food_work.nutrient[PROCNT] += change * pcfmatrix[1][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[1][1];
food_work.nutrient[FAT] += change * pcfmatrix[1][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[1][3];
food_work.nutrient[VITE] += change * pcfmatrix[1][4];
food_work.nutrient[CA] += change * pcfmatrix[1][5];
food_work.nutrient[THIA] += change * pcfmatrix[1][6];
food_work.nutrient[K] += change * pcfmatrix[1][7];
food_work.nutrient[FE] += change * pcfmatrix[1][8];
food_work.nutrient[ZN] += change * pcfmatrix[1][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[1][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[1][11];
pcfmeal[1]->grams += 100 * change / options.mealsperday;
}

void c_cal_func(void)
{
float change;
change = (DV[ENERC_KCAL] - food_work.nutrient[ENERC_KCAL]) / pcfmatrix[1][10];
food_work.nutrient[PROCNT] += change * pcfmatrix[1][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[1][1];
food_work.nutrient[FAT] += change * pcfmatrix[1][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[1][3];
food_work.nutrient[VITE] += change * pcfmatrix[1][4];
food_work.nutrient[CA] += change * pcfmatrix[1][5];
food_work.nutrient[THIA] += change * pcfmatrix[1][6];
food_work.nutrient[K] += change * pcfmatrix[1][7];
food_work.nutrient[FE] += change * pcfmatrix[1][8];
food_work.nutrient[ZN] += change * pcfmatrix[1][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[1][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[1][11];
pcfmeal[1]->grams += 100 * change / options.mealsperday;
}

void f_func(void)
{
float change;
change = (DV[FAT] - food_work.nutrient[FAT]) / pcfmatrix[2][2];
food_work.nutrient[PROCNT] += change * pcfmatrix[2][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[2][1];
food_work.nutrient[FAT] += change * pcfmatrix[2][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[2][3];
food_work.nutrient[VITE] += change * pcfmatrix[2][4];
food_work.nutrient[CA] += change * pcfmatrix[2][5];
food_work.nutrient[THIA] += change * pcfmatrix[2][6];
food_work.nutrient[K] += change * pcfmatrix[1][7];
food_work.nutrient[FE] += change * pcfmatrix[1][8];
food_work.nutrient[ZN] += change * pcfmatrix[1][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[2][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[2][11];
pcfmeal[2]->grams += 100 * change / options.mealsperday;
}

void f_cal_func(void)
{
float change;
change = (DV[ENERC_KCAL] - food_work.nutrient[ENERC_KCAL]) / pcfmatrix[2][10];
food_work.nutrient[PROCNT] += change * pcfmatrix[2][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[2][1];
food_work.nutrient[FAT] += change * pcfmatrix[2][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[2][3];
food_work.nutrient[VITE] += change * pcfmatrix[2][4];
food_work.nutrient[CA] += change * pcfmatrix[2][5];
food_work.nutrient[THIA] += change * pcfmatrix[2][6];
food_work.nutrient[K] += change * pcfmatrix[2][7];
food_work.nutrient[FE] += change * pcfmatrix[2][8];
food_work.nutrient[ZN] += change * pcfmatrix[2][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[2][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[2][11];
pcfmeal[2]->grams += 100 * change / options.mealsperday;
}

void f_cal30_func(void)
{
float change;
change = (0.3 * DV[ENERC_KCAL] - food_work.nutrient[FAT_KCAL]) / pcfmatrix[2][11];
food_work.nutrient[PROCNT] += change * pcfmatrix[2][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[2][1];
food_work.nutrient[FAT] += change * pcfmatrix[2][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[2][3];
food_work.nutrient[VITE] += change * pcfmatrix[2][4];
food_work.nutrient[CA] += change * pcfmatrix[2][5];
food_work.nutrient[THIA] += change * pcfmatrix[2][6];
food_work.nutrient[K] += change * pcfmatrix[2][7];
food_work.nutrient[FE] += change * pcfmatrix[2][8];
food_work.nutrient[ZN] += change * pcfmatrix[2][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[2][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[2][11];
pcfmeal[2]->grams += 100 * change / options.mealsperday;
}

void n_func(void)
{
float change;
change = (DV[PANTAC] - food_work.nutrient[PANTAC]) / pcfmatrix[3][3];
food_work.nutrient[PROCNT] += change * pcfmatrix[3][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[3][1];
food_work.nutrient[FAT] += change * pcfmatrix[3][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[3][3];
food_work.nutrient[VITE] += change * pcfmatrix[3][4];
food_work.nutrient[CA] += change * pcfmatrix[3][5];
food_work.nutrient[THIA] += change * pcfmatrix[3][6];
food_work.nutrient[K] += change * pcfmatrix[3][7];
food_work.nutrient[FE] += change * pcfmatrix[3][8];
food_work.nutrient[ZN] += change * pcfmatrix[3][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[3][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[3][11];
pcfmeal[3]->grams += 100 * change / options.mealsperday;
}

void e_func(void)
{
float change;
change = (DV[VITE] - food_work.nutrient[VITE]) / pcfmatrix[4][4];
food_work.nutrient[PROCNT] += change * pcfmatrix[4][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[4][1];
food_work.nutrient[FAT] += change * pcfmatrix[4][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[4][3];
food_work.nutrient[VITE] += change * pcfmatrix[4][4];
food_work.nutrient[CA] += change * pcfmatrix[4][5];
food_work.nutrient[THIA] += change * pcfmatrix[4][6];
food_work.nutrient[K] += change * pcfmatrix[4][7];
food_work.nutrient[FE] += change * pcfmatrix[4][8];
food_work.nutrient[ZN] += change * pcfmatrix[4][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[4][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[4][11];
pcfmeal[4]->grams += 100 * change / options.mealsperday;
}

void l_func(void)
{
float change;
change = (DV[CA] - food_work.nutrient[CA]) / pcfmatrix[5][5];
food_work.nutrient[PROCNT] += change * pcfmatrix[5][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[5][1];
food_work.nutrient[FAT] += change * pcfmatrix[5][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[5][3];
food_work.nutrient[VITE] += change * pcfmatrix[5][4];
food_work.nutrient[CA] += change * pcfmatrix[5][5];
food_work.nutrient[THIA] += change * pcfmatrix[5][6];
food_work.nutrient[K] += change * pcfmatrix[5][7];
food_work.nutrient[FE] += change * pcfmatrix[5][8];
food_work.nutrient[ZN] += change * pcfmatrix[5][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[5][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[5][11];
pcfmeal[5]->grams += 100 * change / options.mealsperday;
}

void t_func(void)
{
float change;
change = (DV[THIA] - food_work.nutrient[THIA]) / pcfmatrix[6][6];
food_work.nutrient[PROCNT] += change * pcfmatrix[6][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[6][1];
food_work.nutrient[FAT] += change * pcfmatrix[6][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[6][3];
food_work.nutrient[VITE] += change * pcfmatrix[6][4];
food_work.nutrient[CA] += change * pcfmatrix[6][5];
food_work.nutrient[THIA] += change * pcfmatrix[6][6];
food_work.nutrient[K] += change * pcfmatrix[6][7];
food_work.nutrient[FE] += change * pcfmatrix[6][8];
food_work.nutrient[ZN] += change * pcfmatrix[6][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[6][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[6][11];
pcfmeal[6]->grams += 100 * change / options.mealsperday;
}

void k_func(void)
{
float change;
change = (DV[K] - food_work.nutrient[K]) / pcfmatrix[7][7];
food_work.nutrient[PROCNT] += change * pcfmatrix[7][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[7][1];
food_work.nutrient[FAT] += change * pcfmatrix[7][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[7][3];
food_work.nutrient[VITE] += change * pcfmatrix[7][4];
food_work.nutrient[CA] += change * pcfmatrix[7][5];
food_work.nutrient[THIA] += change * pcfmatrix[7][6];
food_work.nutrient[K] += change * pcfmatrix[7][7];
food_work.nutrient[FE] += change * pcfmatrix[7][8];
food_work.nutrient[ZN] += change * pcfmatrix[7][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[7][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[7][11];
pcfmeal[7]->grams += 100 * change / options.mealsperday;
}

void i_func(void)
{
float change;
change = (DV[FE] - food_work.nutrient[FE]) / pcfmatrix[8][8];
food_work.nutrient[PROCNT] += change * pcfmatrix[8][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[8][1];
food_work.nutrient[FAT] += change * pcfmatrix[8][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[8][3];
food_work.nutrient[VITE] += change * pcfmatrix[8][4];
food_work.nutrient[CA] += change * pcfmatrix[8][5];
food_work.nutrient[THIA] += change * pcfmatrix[8][6];
food_work.nutrient[K] += change * pcfmatrix[8][7];
food_work.nutrient[FE] += change * pcfmatrix[8][8];
food_work.nutrient[ZN] += change * pcfmatrix[8][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[8][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[8][11];
pcfmeal[8]->grams += 100 * change / options.mealsperday;
}

void z_func(void)
{
float change;
change = (DV[ZN] - food_work.nutrient[ZN]) / pcfmatrix[9][9];
food_work.nutrient[PROCNT] += change * pcfmatrix[9][0];
food_work.nutrient[CHO_NONFIB] += change * pcfmatrix[9][1];
food_work.nutrient[FAT] += change * pcfmatrix[9][2];
food_work.nutrient[PANTAC] += change * pcfmatrix[9][3];
food_work.nutrient[VITE] += change * pcfmatrix[9][4];
food_work.nutrient[CA] += change * pcfmatrix[9][5];
food_work.nutrient[THIA] += change * pcfmatrix[9][6];
food_work.nutrient[K] += change * pcfmatrix[9][7];
food_work.nutrient[FE] += change * pcfmatrix[9][8];
food_work.nutrient[ZN] += change * pcfmatrix[9][9];
food_work.nutrient[ENERC_KCAL] += change * pcfmatrix[9][10];
food_work.nutrient[FAT_KCAL] += change * pcfmatrix[9][11];
pcfmeal[9]->grams += 100 * change / options.mealsperday;
}

void order_new_meal()
{
struct meal *meal_ptr = &meal_root;
while (meal_ptr->next != NULL &&
       strcmp(new_meal->meal_date, meal_ptr->next->meal_date) < 0)
            meal_ptr = meal_ptr->next;
while (meal_ptr->next != NULL &&
       strcmp(new_meal->meal_date, meal_ptr->next->meal_date) <= 0 &&
       new_meal->meal < meal_ptr->next->meal)
            meal_ptr = meal_ptr->next;
while (meal_ptr->next != NULL &&
       strcmp(new_meal->meal_date, meal_ptr->next->meal_date) <= 0 &&
       new_meal->meal <= meal_ptr->next->meal &&
       strcmp(FoodIndex[new_meal->food_no]->name,FoodIndex[meal_ptr->next->food_no]->name) > 0) 
            meal_ptr = meal_ptr->next;
new_meal->next = meal_ptr->next;
meal_ptr->next = new_meal;
}

void order_new_theusual()
{
struct meal *theusual_ptr = &theusual_root;
while (theusual_ptr->next != NULL &&
       strcmp(new_theusual->meal_date, theusual_ptr->next->meal_date) > 0)
            theusual_ptr = theusual_ptr->next;
while (theusual_ptr->next != NULL &&
       strcmp(new_theusual->meal_date, theusual_ptr->next->meal_date) >= 0 &&
       strcmp(FoodIndex[new_theusual->food_no]->name,FoodIndex[theusual_ptr->next->food_no]->name) > 0) 
            theusual_ptr = theusual_ptr->next;
new_theusual->next = theusual_ptr->next;
theusual_ptr->next = new_theusual;
}

struct meal *theusual_choice(char *screentitle, char *key)
{
struct meal *theusual_ptr;
char meal_date[9];
char new_meal_date[9];
int c;
for (c = 8; c < 17; c++) meal_date[c-8] = key[c];
for ( ; ; )
 {
 theusual_ptr = theusual_find(meal_date);
 if (theusual_ptr != NULL) return theusual_ptr;
 header(screentitle);
 spacer(theusual_list()-1);
 printf("\nType \"The Usual\" Identifier (max. 8 characters):  ");
 get_string(new_meal_date,8);
 if (strlen(new_meal_date) == 0) return (struct meal *) 0;
 for (c = 0 ; c < 9 ; c++) meal_date[c] = toupper(new_meal_date[c]);
 theusual_ptr = theusual_find(meal_date);
 if (theusual_ptr != NULL) return theusual_ptr;
 }
}

int meal_show(char *meal_date, int meal)
{
struct meal *meal_ptr = &meal_root;
char namebuf[60];
int count = 0;
printf("Meal Date:  %s                                            Meal Number:  %d\n\n",meal_date,meal);
for ( ; ; )
 {
 if (strcmp(meal_date,meal_ptr->meal_date) == 0 && meal == meal_ptr->meal) 
  {
  count++;
  strncpy(namebuf,FoodIndex[meal_ptr->food_no]->name,59); namebuf[59] = '\0';
  if (options.grams) printf("%2d. %-59s   %9.1f g %c\n",count,namebuf,meal_ptr->grams,FoodIndex[meal_ptr->food_no]->pcf);
  if (!options.grams) printf("%2d. %-59s   %8.1f oz %c\n",count,namebuf,meal_ptr->grams/GRAMS_IN_OUNCE,FoodIndex[meal_ptr->food_no]->pcf);
  }
 if (meal_ptr->next == NULL) break;
 meal_ptr = meal_ptr->next;
 }
if (count == 0) 
 {
 printf("\n\n\nNo foods have yet been recorded for this meal.\n");
 spacer(6);
 return 0;
 } 
spacer(count + 2);
return 1;
}

int theusual_show(char *meal_date)
{
struct meal *theusual_ptr = &theusual_root;
char namebuf[60];
int count = 0;
printf("\"The Usual\" Identifier:  %s\n\n",meal_date);
for ( ; ; )
 {
 if (strcmp(meal_date,theusual_ptr->meal_date) == 0) 
  {
  count++;
  strncpy(namebuf,FoodIndex[theusual_ptr->food_no]->name,59); namebuf[59] = '\0';
  if (options.grams) printf("%2d. %-59s     %9.1f g\n",count,namebuf,theusual_ptr->grams);
  if (!options.grams) printf("%2d. %-59s     %8.1f oz\n",count,namebuf,theusual_ptr->grams/GRAMS_IN_OUNCE);
  }
 if (theusual_ptr->next == NULL) break;
 theusual_ptr = theusual_ptr->next;
 }
if (count == 0) 
 {
 printf("\n\n\nNo foods have yet been recorded for this customary meal.\n");
 spacer(6);
 return 0;
 } 
spacer(count + 2);
return 1;
}

struct meal *meal_find(char *meal_date,int meal)
{
struct meal *meal_ptr = &meal_root;
while (meal_ptr->next != NULL)
 {
 meal_ptr = meal_ptr->next;
 if (strcmp(meal_ptr->meal_date,meal_date) == 0 && meal_ptr->meal == meal) return meal_ptr;
 }
return NULL;
}

struct meal *theusual_find(char *meal_date)
{
struct meal *theusual_ptr = &theusual_root;
while (theusual_ptr->next != NULL)
 {
 theusual_ptr = theusual_ptr->next;
 if (strcmp(theusual_ptr->meal_date,meal_date) == 0) return theusual_ptr;
 }
return NULL;
}

struct meal *prev_meal(struct meal *meal_after)
{
struct meal *meal_ptr = &meal_root;
while (meal_ptr->next != meal_after) meal_ptr = meal_ptr->next;
return meal_ptr;
}

void modify_meal(char *meal_date, int meal, int num, char *qty)
{
struct meal *m = NULL, *meal_ptr = &meal_root;
int count = 0, nut = -1;
float newqty;
if (qty != NULL && (*qty == 'P' || *qty == 'p')) nut = PROCNT;
else if (qty != NULL && (*qty == 'C' || *qty == 'c')) nut = CHO_NONFIB;
else if (qty != NULL && (*qty == 'F' || *qty == 'f')) nut = FAT;
else if (qty != NULL && (*qty == 'T' || *qty == 't')) nut = THIA;
else if (qty != NULL && (*qty == 'N' || *qty == 'n')) nut = PANTAC;
else if (qty != NULL && (*qty == 'E' || *qty == 'e')) nut = VITE;
else if (qty != NULL && (*qty == 'L' || *qty == 'l')) nut = CA;
else if (qty != NULL && (*qty == 'I' || *qty == 'i')) nut = FE;
else if (qty != NULL && (*qty == 'K' || *qty == 'k')) nut = K;
else if (qty != NULL && (*qty == 'Z' || *qty == 'z')) nut = ZN;
if (nut != -1) while (meal_ptr->next != NULL)
 {
 if (strcmp(meal_date,meal_ptr->next->meal_date) == 0 && meal == meal_ptr->next->meal) 
  {
  count++;
  if (count == num) m = meal_ptr;
  }
 meal_ptr = meal_ptr->next;
 }
if (nut != -1 && m != NULL)
 {
 meal_ptr = m->next;
 pcftweak(nut,meal_ptr);
 return;
 }
if (nut == -1) while (meal_ptr->next != NULL)
 {
 if (strcmp(meal_date,meal_ptr->next->meal_date) == 0 && meal == meal_ptr->next->meal) 
  {
  count++;
  if (count == num)
   {
   newqty = evaluate_qty(food_number(meal_ptr->next->food_no),qty);
   if (newqty == 0)
    {
    m = meal_ptr->next;
    meal_ptr->next = meal_ptr->next->next;
    pcfclear(m);
    free(m);
    return;
    }
   else meal_ptr->next->grams = newqty;
   return;
   }
  }
 meal_ptr = meal_ptr->next;
 }
}

void pcf(char *meal_date, int meal, char *substring)
{
struct meal *start_ptr = &meal_root, *end_ptr;
char buffer[128];
char *token;
int p, c, f, count = 0;

strncpy(buffer,substring,127);
token = strtok(buffer," ");

token = (strtok(NULL," "));
if (token != NULL) p = atoi(token);
else p = 0;

token = (strtok(NULL," "));
if (token != NULL) c = atoi(token);
else c = 0;

token = (strtok(NULL," "));
if (token != NULL) f = atoi(token);
else f = 0;

if (p < 1 || c < 1 || f < 1) return;
if (p == c || p == f || c == f) return;

while (start_ptr != NULL)
 {
 if ((strcmp(start_ptr->next->meal_date,meal_date) == 0) && start_ptr->next->meal == meal) break;
 start_ptr = start_ptr->next;
 }

if (start_ptr == NULL) return;

end_ptr = start_ptr;

while (end_ptr != NULL)
 {
 count++;
 if (end_ptr->next == NULL) break;
 if ((strcmp(end_ptr->next->meal_date,meal_date) != 0) || end_ptr->next->meal != meal) break;
 if (p == count)
  {
  if (pcfmeal[0] != NULL) FoodIndex[pcfmeal[0]->food_no]->pcf = ' ';
  pcfmeal[0] = end_ptr->next;
  }
 else if (c == count)
  {
  if (pcfmeal[1] != NULL) FoodIndex[pcfmeal[1]->food_no]->pcf = ' ';
  pcfmeal[1] = end_ptr->next;
  }
 else if (f == count)
  {
  if (pcfmeal[2] != NULL) FoodIndex[pcfmeal[2]->food_no]->pcf = ' ';
  pcfmeal[2] = end_ptr->next;
  }
 end_ptr = end_ptr->next;
 }

if (end_ptr == NULL) return;
if (pcfmeal[0] == NULL) return;
if (pcfmeal[1] == NULL) return;
if (pcfmeal[2] == NULL) return;

if (pcfmeal[0] != NULL) FoodIndex[pcfmeal[0]->food_no]->pcf = 'p';
if (pcfmeal[1] != NULL) FoodIndex[pcfmeal[1]->food_no]->pcf = 'c';
if (pcfmeal[2] != NULL) FoodIndex[pcfmeal[2]->food_no]->pcf = 'f';

pcfrun();
}

void pcfrun(void)
{
typedef void (*xfunc)(void);
xfunc pfunc = &no_op, cfunc = &no_op, ffunc = &no_op, nfunc = &no_op, efunc = &no_op, lfunc = &no_op, tfunc = &no_op, kfunc = &no_op, ifunc = &no_op, zfunc = &no_op;
int count = 0, iterate, savescreen;

DVnotOK = 1;
options.comparison = 0;

if (pcfmeal[0] != NULL)
 {
 pcfmatrix[0][0] = FoodIndex[pcfmeal[0]->food_no]->nutrient[PROCNT];
 pcfmatrix[0][1] = FoodIndex[pcfmeal[0]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[0][2] = FoodIndex[pcfmeal[0]->food_no]->nutrient[FAT];
 pcfmatrix[0][3] = FoodIndex[pcfmeal[0]->food_no]->nutrient[PANTAC];
 pcfmatrix[0][4] = FoodIndex[pcfmeal[0]->food_no]->nutrient[VITE];
 pcfmatrix[0][5] = FoodIndex[pcfmeal[0]->food_no]->nutrient[CA];
 pcfmatrix[0][6] = FoodIndex[pcfmeal[0]->food_no]->nutrient[THIA];
 pcfmatrix[0][7] = FoodIndex[pcfmeal[0]->food_no]->nutrient[K];
 pcfmatrix[0][8] = FoodIndex[pcfmeal[0]->food_no]->nutrient[FE];
 pcfmatrix[0][9] = FoodIndex[pcfmeal[0]->food_no]->nutrient[ZN];
 pcfmatrix[0][10] = FoodIndex[pcfmeal[0]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[0][11] = FoodIndex[pcfmeal[0]->food_no]->nutrient[FAT_KCAL];
 pfunc = &p_func;
 }
if (pcfmeal[1] != NULL)
 {
 pcfmatrix[1][0] = FoodIndex[pcfmeal[1]->food_no]->nutrient[PROCNT];
 pcfmatrix[1][1] = FoodIndex[pcfmeal[1]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[1][2] = FoodIndex[pcfmeal[1]->food_no]->nutrient[FAT];
 pcfmatrix[1][3] = FoodIndex[pcfmeal[1]->food_no]->nutrient[PANTAC];
 pcfmatrix[1][4] = FoodIndex[pcfmeal[1]->food_no]->nutrient[VITE];
 pcfmatrix[1][5] = FoodIndex[pcfmeal[1]->food_no]->nutrient[CA];
 pcfmatrix[1][6] = FoodIndex[pcfmeal[1]->food_no]->nutrient[THIA];
 pcfmatrix[1][7] = FoodIndex[pcfmeal[1]->food_no]->nutrient[K];
 pcfmatrix[1][8] = FoodIndex[pcfmeal[1]->food_no]->nutrient[FE];
 pcfmatrix[1][9] = FoodIndex[pcfmeal[1]->food_no]->nutrient[ZN];
 pcfmatrix[1][10] = FoodIndex[pcfmeal[1]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[1][11] = FoodIndex[pcfmeal[1]->food_no]->nutrient[FAT_KCAL];
 if (options.abnuts[ENERC_KCAL] >= 0 && options.abnuts[CHO_NONFIB] == 0) cfunc = &c_cal_func; 
 else cfunc = &c_func;
 }
if (pcfmeal[2] != NULL)
 {
 pcfmatrix[2][0] = FoodIndex[pcfmeal[2]->food_no]->nutrient[PROCNT];
 pcfmatrix[2][1] = FoodIndex[pcfmeal[2]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[2][2] = FoodIndex[pcfmeal[2]->food_no]->nutrient[FAT];
 pcfmatrix[2][3] = FoodIndex[pcfmeal[2]->food_no]->nutrient[PANTAC];
 pcfmatrix[2][4] = FoodIndex[pcfmeal[2]->food_no]->nutrient[VITE];
 pcfmatrix[2][5] = FoodIndex[pcfmeal[2]->food_no]->nutrient[CA];
 pcfmatrix[2][6] = FoodIndex[pcfmeal[2]->food_no]->nutrient[THIA];
 pcfmatrix[2][7] = FoodIndex[pcfmeal[2]->food_no]->nutrient[K];
 pcfmatrix[2][7] = FoodIndex[pcfmeal[2]->food_no]->nutrient[FE];
 pcfmatrix[2][8] = FoodIndex[pcfmeal[2]->food_no]->nutrient[ZN];
 pcfmatrix[2][10] = FoodIndex[pcfmeal[2]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[2][11] = FoodIndex[pcfmeal[2]->food_no]->nutrient[FAT_KCAL];
 if (options.abnuts[ENERC_KCAL] >= 0 && options.abnuts[FAT] == 0 && options.abnuts[CHO_NONFIB] != 0) ffunc = &f_cal_func; 
 else if (options.abnuts[ENERC_KCAL] >= 0 && options.abnuts[FAT] == 0 && options.abnuts[CHO_NONFIB] == 0) ffunc = &f_cal30_func; 
 else ffunc = &f_func;
 }
if (pcfmeal[3] != NULL)
 {
 pcfmatrix[3][0] = FoodIndex[pcfmeal[3]->food_no]->nutrient[PROCNT];
 pcfmatrix[3][1] = FoodIndex[pcfmeal[3]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[3][2] = FoodIndex[pcfmeal[3]->food_no]->nutrient[FAT];
 pcfmatrix[3][3] = FoodIndex[pcfmeal[3]->food_no]->nutrient[PANTAC];
 pcfmatrix[3][4] = FoodIndex[pcfmeal[3]->food_no]->nutrient[VITE];
 pcfmatrix[3][5] = FoodIndex[pcfmeal[3]->food_no]->nutrient[CA];
 pcfmatrix[3][6] = FoodIndex[pcfmeal[3]->food_no]->nutrient[THIA];
 pcfmatrix[3][7] = FoodIndex[pcfmeal[3]->food_no]->nutrient[K];
 pcfmatrix[3][8] = FoodIndex[pcfmeal[3]->food_no]->nutrient[FE];
 pcfmatrix[3][9] = FoodIndex[pcfmeal[3]->food_no]->nutrient[ZN];
 pcfmatrix[3][10] = FoodIndex[pcfmeal[3]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[3][11] = FoodIndex[pcfmeal[3]->food_no]->nutrient[FAT_KCAL];
 nfunc = &n_func;                                              
 }
if (pcfmeal[4] != NULL)
 {
 pcfmatrix[4][0] = FoodIndex[pcfmeal[4]->food_no]->nutrient[PROCNT];
 pcfmatrix[4][1] = FoodIndex[pcfmeal[4]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[4][2] = FoodIndex[pcfmeal[4]->food_no]->nutrient[FAT];
 pcfmatrix[4][3] = FoodIndex[pcfmeal[4]->food_no]->nutrient[PANTAC];
 pcfmatrix[4][4] = FoodIndex[pcfmeal[4]->food_no]->nutrient[VITE];
 pcfmatrix[4][5] = FoodIndex[pcfmeal[4]->food_no]->nutrient[CA];
 pcfmatrix[4][6] = FoodIndex[pcfmeal[4]->food_no]->nutrient[THIA];
 pcfmatrix[4][7] = FoodIndex[pcfmeal[4]->food_no]->nutrient[K];
 pcfmatrix[4][8] = FoodIndex[pcfmeal[4]->food_no]->nutrient[FE];
 pcfmatrix[4][9] = FoodIndex[pcfmeal[4]->food_no]->nutrient[ZN];
 pcfmatrix[4][10] = FoodIndex[pcfmeal[4]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[4][11] = FoodIndex[pcfmeal[4]->food_no]->nutrient[FAT_KCAL];
 efunc = &e_func;
 }
if (pcfmeal[5] != NULL)
 {
 pcfmatrix[5][0] = FoodIndex[pcfmeal[5]->food_no]->nutrient[PROCNT];
 pcfmatrix[5][1] = FoodIndex[pcfmeal[5]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[5][2] = FoodIndex[pcfmeal[5]->food_no]->nutrient[FAT];
 pcfmatrix[5][3] = FoodIndex[pcfmeal[5]->food_no]->nutrient[PANTAC];
 pcfmatrix[5][4] = FoodIndex[pcfmeal[5]->food_no]->nutrient[VITE];
 pcfmatrix[5][5] = FoodIndex[pcfmeal[5]->food_no]->nutrient[CA];
 pcfmatrix[5][6] = FoodIndex[pcfmeal[5]->food_no]->nutrient[THIA];
 pcfmatrix[5][7] = FoodIndex[pcfmeal[5]->food_no]->nutrient[K];
 pcfmatrix[5][8] = FoodIndex[pcfmeal[5]->food_no]->nutrient[FE];
 pcfmatrix[5][9] = FoodIndex[pcfmeal[5]->food_no]->nutrient[ZN];
 pcfmatrix[5][10] = FoodIndex[pcfmeal[5]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[5][11] = FoodIndex[pcfmeal[5]->food_no]->nutrient[FAT_KCAL];
 lfunc = &l_func;
 }
if (pcfmeal[6] != NULL)
 {
 pcfmatrix[6][0] = FoodIndex[pcfmeal[6]->food_no]->nutrient[PROCNT];
 pcfmatrix[6][1] = FoodIndex[pcfmeal[6]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[6][2] = FoodIndex[pcfmeal[6]->food_no]->nutrient[FAT];
 pcfmatrix[6][3] = FoodIndex[pcfmeal[6]->food_no]->nutrient[PANTAC];
 pcfmatrix[6][4] = FoodIndex[pcfmeal[6]->food_no]->nutrient[VITE];
 pcfmatrix[6][5] = FoodIndex[pcfmeal[6]->food_no]->nutrient[CA];
 pcfmatrix[6][6] = FoodIndex[pcfmeal[6]->food_no]->nutrient[THIA];
 pcfmatrix[6][7] = FoodIndex[pcfmeal[6]->food_no]->nutrient[K];
 pcfmatrix[6][8] = FoodIndex[pcfmeal[6]->food_no]->nutrient[FE];
 pcfmatrix[6][9] = FoodIndex[pcfmeal[6]->food_no]->nutrient[ZN];
 pcfmatrix[6][10] = FoodIndex[pcfmeal[6]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[6][11] = FoodIndex[pcfmeal[6]->food_no]->nutrient[FAT_KCAL];
 tfunc = &t_func;
 }
if (pcfmeal[7] != NULL)
 {
 pcfmatrix[7][0] = FoodIndex[pcfmeal[7]->food_no]->nutrient[PROCNT];
 pcfmatrix[7][1] = FoodIndex[pcfmeal[7]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[7][2] = FoodIndex[pcfmeal[7]->food_no]->nutrient[FAT];
 pcfmatrix[7][3] = FoodIndex[pcfmeal[7]->food_no]->nutrient[PANTAC];
 pcfmatrix[7][4] = FoodIndex[pcfmeal[7]->food_no]->nutrient[VITE];
 pcfmatrix[7][5] = FoodIndex[pcfmeal[7]->food_no]->nutrient[CA];
 pcfmatrix[7][6] = FoodIndex[pcfmeal[7]->food_no]->nutrient[THIA];
 pcfmatrix[7][7] = FoodIndex[pcfmeal[7]->food_no]->nutrient[K];
 pcfmatrix[7][8] = FoodIndex[pcfmeal[7]->food_no]->nutrient[FE];
 pcfmatrix[7][9] = FoodIndex[pcfmeal[7]->food_no]->nutrient[ZN];
 pcfmatrix[7][10] = FoodIndex[pcfmeal[7]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[7][11] = FoodIndex[pcfmeal[7]->food_no]->nutrient[FAT_KCAL];
 kfunc = &k_func;
 }
if (pcfmeal[8] != NULL)
 {
 pcfmatrix[8][0] = FoodIndex[pcfmeal[8]->food_no]->nutrient[PROCNT];
 pcfmatrix[8][1] = FoodIndex[pcfmeal[8]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[8][2] = FoodIndex[pcfmeal[8]->food_no]->nutrient[FAT];
 pcfmatrix[8][3] = FoodIndex[pcfmeal[8]->food_no]->nutrient[PANTAC];
 pcfmatrix[8][4] = FoodIndex[pcfmeal[8]->food_no]->nutrient[VITE];
 pcfmatrix[8][5] = FoodIndex[pcfmeal[8]->food_no]->nutrient[CA];
 pcfmatrix[8][6] = FoodIndex[pcfmeal[8]->food_no]->nutrient[THIA];
 pcfmatrix[8][7] = FoodIndex[pcfmeal[8]->food_no]->nutrient[K];
 pcfmatrix[8][8] = FoodIndex[pcfmeal[8]->food_no]->nutrient[FE];
 pcfmatrix[8][9] = FoodIndex[pcfmeal[8]->food_no]->nutrient[ZN];
 pcfmatrix[8][10] = FoodIndex[pcfmeal[8]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[8][11] = FoodIndex[pcfmeal[8]->food_no]->nutrient[FAT_KCAL];
 ifunc = &i_func;
 }
if (pcfmeal[9] != NULL)
 {
 pcfmatrix[9][0] = FoodIndex[pcfmeal[9]->food_no]->nutrient[PROCNT];
 pcfmatrix[9][1] = FoodIndex[pcfmeal[9]->food_no]->nutrient[CHO_NONFIB];
 pcfmatrix[9][2] = FoodIndex[pcfmeal[9]->food_no]->nutrient[FAT];
 pcfmatrix[9][3] = FoodIndex[pcfmeal[9]->food_no]->nutrient[PANTAC];
 pcfmatrix[9][4] = FoodIndex[pcfmeal[9]->food_no]->nutrient[VITE];
 pcfmatrix[9][5] = FoodIndex[pcfmeal[9]->food_no]->nutrient[CA];
 pcfmatrix[9][6] = FoodIndex[pcfmeal[9]->food_no]->nutrient[THIA];
 pcfmatrix[9][7] = FoodIndex[pcfmeal[9]->food_no]->nutrient[K];
 pcfmatrix[9][8] = FoodIndex[pcfmeal[9]->food_no]->nutrient[FE];
 pcfmatrix[9][9] = FoodIndex[pcfmeal[9]->food_no]->nutrient[ZN];
 pcfmatrix[9][10] = FoodIndex[pcfmeal[9]->food_no]->nutrient[ENERC_KCAL];
 pcfmatrix[9][11] = FoodIndex[pcfmeal[9]->food_no]->nutrient[FAT_KCAL];
 zfunc = &z_func;
 }

savescreen = options.screen;
options.screen = 0;

for (iterate = 0; iterate < 3; iterate++)
 {
 load_foodwork(1,options.temp_meal_root);
 auto_cal();

 for (count = 0; count < 5; count++)
  {
  pfunc();
  cfunc();
  ffunc();
  nfunc();
  efunc();
  lfunc();
  tfunc();
  kfunc();
  ifunc();
  zfunc();
  }
 }

for (count = 0; count < 10; count++) if (pcfmeal[count] != NULL)
 {
 if (pcfmeal[count]->grams != pcfmeal[count]->grams) pcfmeal[count]->grams = 0;
 if (pcfmeal[count]->grams < -1000) pcfmeal[count]->grams = 0;
 if (pcfmeal[count]->grams > 1000) pcfmeal[count]->grams = 0;
 }

options.screen = savescreen;
write_meal_db();
}

void pcftweak(int nut, struct meal *ptr)
{
int i;

for (i = 0; i < 10; i++) if (ptr == pcfmeal[i])
 {
 FoodIndex[pcfmeal[i]->food_no]->pcf = ' ';
 pcfmeal[i] = NULL;
 break;
 }
switch (nut)
 {
 case PROCNT :
  if (pcfmeal[0] != NULL) FoodIndex[pcfmeal[0]->food_no]->pcf = ' ';
  else if (i == 0) break;
  if (options.abnuts[PROCNT] != -1)
   {
   pcfmeal[0] = ptr;
   FoodIndex[pcfmeal[0]->food_no]->pcf = 'p';
   }
  break;
 case CHO_NONFIB :          
  if (pcfmeal[1] != NULL) FoodIndex[pcfmeal[1]->food_no]->pcf = ' ';
  else if (i == 1) break;
  if (options.abnuts[CHO_NONFIB] != -1)
   {
   pcfmeal[1] = ptr;
   FoodIndex[pcfmeal[1]->food_no]->pcf = 'c';
   }
  break;
 case FAT :              
  if (pcfmeal[2] != NULL) FoodIndex[pcfmeal[2]->food_no]->pcf = ' ';
  else if (i == 2) break;
  if (options.abnuts[FAT] != -1)
   {
   pcfmeal[2] = ptr;
   FoodIndex[pcfmeal[2]->food_no]->pcf = 'f';
   }
  break;
 case PANTAC :
  if (pcfmeal[3] != NULL) FoodIndex[pcfmeal[3]->food_no]->pcf = ' ';
  else if (i == 3) break;
  pcfmeal[3] = ptr;
  FoodIndex[pcfmeal[3]->food_no]->pcf = 'n';
  break;
 case VITE :
  if (pcfmeal[4] != NULL) FoodIndex[pcfmeal[4]->food_no]->pcf = ' ';
  else if (i == 4) break;
  pcfmeal[4] = ptr;
  FoodIndex[pcfmeal[4]->food_no]->pcf = 'e';
  break;
 case CA :
  if (pcfmeal[5] != NULL) FoodIndex[pcfmeal[5]->food_no]->pcf = ' ';
  else if (i == 5) break;
  pcfmeal[5] = ptr;
  FoodIndex[pcfmeal[5]->food_no]->pcf = 'l';
  break;
 case THIA :
  if (pcfmeal[6] != NULL) FoodIndex[pcfmeal[6]->food_no]->pcf = ' ';
  else if (i == 6) break;
  pcfmeal[6] = ptr;
  FoodIndex[pcfmeal[6]->food_no]->pcf = 't';
  break;
 case K :
  if (pcfmeal[7] != NULL) FoodIndex[pcfmeal[7]->food_no]->pcf = ' ';
  else if (i == 7) break;
  pcfmeal[7] = ptr;
  FoodIndex[pcfmeal[7]->food_no]->pcf = 'k';
  break;
 case FE :
  if (pcfmeal[8] != NULL) FoodIndex[pcfmeal[8]->food_no]->pcf = ' ';
  else if (i == 8) break;
  pcfmeal[8] = ptr;
  FoodIndex[pcfmeal[8]->food_no]->pcf = 'i';
  break;
 case ZN :
  if (pcfmeal[9] != NULL) FoodIndex[pcfmeal[9]->food_no]->pcf = ' ';
  else if (i == 9) break;
  pcfmeal[9] = ptr;
  FoodIndex[pcfmeal[9]->food_no]->pcf = 'z';
  break;
 default : break;
 }
}

void pcfclear(struct meal *ptr)
{
int i;

if (ptr == NULL)
 {
 if (pcfmeal[0] != NULL)
  {
  FoodIndex[pcfmeal[0]->food_no]->pcf = ' ';
  pcfmeal[0] = NULL;
  }
 if (pcfmeal[1] != NULL)
  {
  FoodIndex[pcfmeal[1]->food_no]->pcf = ' ';
  pcfmeal[1] = NULL;
  }
 if (pcfmeal[2] != NULL)
  {
  FoodIndex[pcfmeal[2]->food_no]->pcf = ' ';
  pcfmeal[2] = NULL;
  }
 if (pcfmeal[3] != NULL)
  {
  FoodIndex[pcfmeal[3]->food_no]->pcf = ' ';
  pcfmeal[3] = NULL;
  }
 if (pcfmeal[4] != NULL)
  {
  FoodIndex[pcfmeal[4]->food_no]->pcf = ' ';
  pcfmeal[4] = NULL;
  }
 if (pcfmeal[5] != NULL)
  {
  FoodIndex[pcfmeal[5]->food_no]->pcf = ' ';
  pcfmeal[5] = NULL;
  }
 if (pcfmeal[6] != NULL)
  {
  FoodIndex[pcfmeal[6]->food_no]->pcf = ' ';
  pcfmeal[6] = NULL;
  }
 if (pcfmeal[7] != NULL)
  {
  FoodIndex[pcfmeal[7]->food_no]->pcf = ' ';
  pcfmeal[7] = NULL;
  }
 if (pcfmeal[8] != NULL)
  {
  FoodIndex[pcfmeal[8]->food_no]->pcf = ' ';
  pcfmeal[8] = NULL;
  }
 if (pcfmeal[9] != NULL)
  {
  FoodIndex[pcfmeal[9]->food_no]->pcf = ' ';
  pcfmeal[9] = NULL;
  }
 }
else for (i = 0; i < 10; i++) if (pcfmeal[i] == ptr)
 {
 FoodIndex[pcfmeal[i]->food_no]->pcf = ' ';
 pcfmeal[i] = NULL;
 }
}

void delete_meal_with_ptr(struct meal *target)
{
struct meal *meal_ptr = &meal_root;
while (meal_ptr->next != NULL)
 {
 if (meal_ptr->next == target)
  {
  meal_ptr->next = target->next;
  free(target);
  DVnotOK = 1;
  return;
  }
 meal_ptr = meal_ptr->next;
 }
}

void delete_theusual_with_ptr(struct meal *target)
{
struct meal *meal_ptr = &theusual_root;
while (meal_ptr->next != NULL)
 {
 if (meal_ptr->next == target)
  {
  meal_ptr->next = target->next;
  free(target);
  return;
  }
 meal_ptr = meal_ptr->next;
 }
}

void modify_theusual(char *meal_date, int num, char *qty)
{
struct meal *m = NULL, *theusual_ptr = &theusual_root;
int count = 0, nut = -1, junk;
float newqty, total = 0, thiscontrib = 0;
if (nut != -1) while (theusual_ptr->next != NULL)
 {
 if (strcmp(meal_date,theusual_ptr->next->meal_date) == 0) 
  {
  count++;
  if (count == num) m = theusual_ptr;
  total += options.mealsperday * theusual_ptr->next->grams / 100 * FoodIndex[theusual_ptr->next->food_no]->nutrient[nut];
  }
 theusual_ptr = theusual_ptr->next;
 }
if (nut != -1 && m != NULL)
 {
 thiscontrib = options.mealsperday * m->next->grams / 100 * FoodIndex[m->next->food_no]->nutrient[nut];
 if (DVnotOK)
  {
  junk = options.screen;
  options.screen = 0;
  load_foodwork(options.defanal,options.temp_meal_root);
  auto_cal();
  options.screen = junk;
  }
 newqty = (thiscontrib + DV[nut] - total) / thiscontrib * m->next->grams;
 if (newqty <= 0)
  {
  theusual_ptr = m->next;
  m->next = m->next->next;
  free(theusual_ptr);
  }
 else if (FoodIndex[m->next->food_no]->nutrient[nut] > 0) m->next->grams = newqty;
 return;
 }
if (nut == -1) while (theusual_ptr->next != NULL)
 {
 if (strcmp(meal_date,theusual_ptr->next->meal_date) == 0) 
  {
  count++;
  if (count == num)
   {
   newqty = evaluate_qty(food_number(theusual_ptr->next->food_no),qty);
   if (newqty == 0)
    {
    m = theusual_ptr->next;
    theusual_ptr->next = theusual_ptr->next->next;
    free(m);
    }
   else theusual_ptr->next->grams = newqty;
   return;
   }
  }
 theusual_ptr = theusual_ptr->next;
 }
}

int meal_count(struct meal *meal_ptr)
{
int count = 0;
char lastdate[9];
int lastmeal;
if (meal_ptr->next == NULL) return 0;
meal_ptr = meal_ptr->next;
strcpy(lastdate,meal_ptr->meal_date);
lastmeal = meal_ptr->meal;
count = 1;
while ((meal_ptr = meal_ptr->next))
 {
 if (strcmp(lastdate,meal_ptr->meal_date) != 0 || lastmeal != meal_ptr->meal)
  {
  count++;
  strcpy(lastdate,meal_ptr->meal_date);
  lastmeal = meal_ptr->meal;
  }
 } 
return count;
}

void delete_meals(int keep)
{
struct meal *meal_ptr = &meal_root, *last_meal_ptr = NULL;
int count = 0;
char meal_date[9], meal = 0;
if (keep < 0) return;
strcpy(meal_date,"");
while (count <= keep && meal_ptr->next != NULL)
 {
 last_meal_ptr = meal_ptr;
 meal_ptr = meal_ptr->next;
 if (strcmp(meal_date,meal_ptr->meal_date) != 0  || meal != meal_ptr->meal) 
  {
  count++;
  strcpy(meal_date,meal_ptr->meal_date);
  meal = meal_ptr->meal;
  }
 }  
if (count <= keep) return;
last_meal_ptr->next = NULL;
while (meal_ptr != NULL)
 {
 last_meal_ptr = meal_ptr;
 meal_ptr = meal_ptr->next;
 free(last_meal_ptr);
 DVnotOK = 1;
 }
}

int theusual_list()
{ 
struct meal *theusual_ptr = &theusual_root;
char last_meal_date[9];
int c = 0;
last_meal_date[0] = '\0';
printf("Customary Meals so far:\n\n");
while (theusual_ptr->next != NULL)
 {
 theusual_ptr = theusual_ptr->next;
 if (strcmp(last_meal_date,theusual_ptr->meal_date) == 0) continue;
 strcpy(last_meal_date,theusual_ptr->meal_date);
 printf(" %-8s",theusual_ptr->meal_date);
 c++;
 if (c%7 == 0) printf("\n");
 else printf("  ");
 }
return c/7+3;
}

void meal_list(char *bufptr)
{ 
struct meal *meal_ptr = &meal_root;
int c;
int missing[20];
for (c=0; c<20; c++) missing[c] = 0;
while (meal_ptr->next != NULL && strcmp(bufptr,meal_ptr->meal_date) != 0) meal_ptr = meal_ptr->next;
if (meal_ptr->next == NULL && strcmp(bufptr,meal_ptr->meal_date) != 0)
 {
 for (c = options.mealsperday; c >= 1; c--)
  {
  missing[0]++;
  missing[c] = 1;
  }
 }
else for (c = options.mealsperday; c >= 1; c--)
 {
 if (c != meal_ptr->meal)
  {
  missing[0]++;
  missing[c] = 1;
  }
 while (c == meal_ptr->meal && meal_ptr->next != NULL && strcmp(bufptr,meal_ptr->next->meal_date) == 0) meal_ptr = meal_ptr->next;
 }
if (missing[0] == 0) printf("\n\n\n");
else
 {
 printf("Missing Meals:\n\n");
 for (c=1; c<20; c++) if (missing[c] == 1) printf("%-4d",c);
 printf("\n");
 }
}

void reindex_meals(int foodnum)
{
struct meal *meal_ptr = &meal_root;
while (meal_ptr->next != NULL)
 {
 meal_ptr = meal_ptr->next;
 if (meal_ptr->food_no >= foodnum) meal_ptr->food_no++;
 }
meal_ptr = &theusual_root;
while (meal_ptr->next != NULL)
 {
 meal_ptr = meal_ptr->next;
 if (meal_ptr->food_no >= foodnum) meal_ptr->food_no++;
 }
}

void full_meal_reindexing()
{
struct meal *meal_ptr = &meal_root;
printf("\nStarting to re-index meals...\n");
while (meal_ptr->next != NULL)
 {
 if (meal_ptr->next->ndb_no == 0 && version(0) < 9) meal_ptr->next->ndb_no = 42231;
 meal_ptr->next->food_no = find_ndbno(meal_ptr->next->ndb_no);
 while (meal_ptr->next->food_no == -1)
  {
  delete_meal_with_ptr(meal_ptr->next);
  if (meal_ptr->next == NULL) break;
  if (meal_ptr->next->ndb_no == 0 && version(0) < 9) meal_ptr->next->ndb_no = 42231;
  meal_ptr->next->food_no = find_ndbno(meal_ptr->next->ndb_no);
  }
 if (meal_ptr->next != NULL) meal_ptr = meal_ptr->next;
 }
if (meal_ptr->ndb_no == 0 && version(0) < 9) meal_ptr->ndb_no = 42231;
meal_ptr->food_no = find_ndbno(meal_ptr->ndb_no);
if (meal_ptr->food_no == -1) delete_meal_with_ptr(meal_ptr);

meal_ptr = &theusual_root;
while (meal_ptr->next != NULL)
 {
 if (meal_ptr->next->ndb_no == 0 && version(0) < 9) meal_ptr->next->ndb_no = 42231;
 meal_ptr->next->food_no = find_ndbno(meal_ptr->next->ndb_no);
 while (meal_ptr->next->food_no == -1)
  {
  delete_theusual_with_ptr(meal_ptr->next);
  if (meal_ptr->next == NULL) return;
  if (meal_ptr->next->ndb_no == 0 && version(0) < 9) meal_ptr->next->ndb_no = 42231;
  meal_ptr->next->food_no = find_ndbno(meal_ptr->next->ndb_no);
  }
 if (meal_ptr->next != NULL) meal_ptr = meal_ptr->next;
 }
if (meal_ptr->ndb_no == 0 && version(0) < 9) meal_ptr->ndb_no = 42231;
meal_ptr->food_no = find_ndbno(meal_ptr->ndb_no);
if (meal_ptr->food_no == -1) delete_theusual_with_ptr(meal_ptr);
}

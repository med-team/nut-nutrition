/* meal.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MEAL_H
#define MEAL_H


struct meal
 {
 char  meal_date[9];
 char  meal;
 int   ndb_no;
 int   food_no;
 float grams;
 struct meal *next;
 };

extern struct meal meal_root, theusual_root, *new_meal, *new_theusual;

#ifdef __cplusplus
extern "C" {
#endif


void order_new_meal();
void order_new_theusual();
struct meal *theusual_choice(char *, char *);
int meal_show(char *, int);
int theusual_show(char *);
struct meal *meal_find(char *,int);
struct meal *theusual_find(char *);
struct meal *prev_meal(struct meal *);
void modify_meal(char *, int, int, char *);
void pcf(char *, int, char *);
void pcfrun(void);
void pcftweak(int, struct meal *);
void pcfclear(struct meal *);
void delete_meal_with_ptr(struct meal *);
void modify_theusual(char *, int, char *);
void delete_theusual_with_ptr(struct meal *);
int meal_count(struct meal *);
void delete_meals(int);
int theusual_list(void);
void meal_list(char *);
void reindex_meals(int);
void full_meal_reindexing(void);

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif

/* options.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef OPTIONS_H
#define OPTIONS_H

#include "food.h"
#include "meal.h"

struct opt
 {
 int delopt;
 int defanal;
 int defanalanal;
 int defanalrec;
 int screen;
 int custom;
 float n6hufa;
 float lastwlw;
 float lastwlbfp;
 char lastwldate[9];
 int wltweak;
 int wlpolarity;
 int next_recipe;
 float abnuts[NUTRIENT_COUNT];
 int mealsperday;
 char grams;
 char autocal;
 char comparison;
 struct meal *temp_meal_root;
 };

extern float DefaultTarget, DefaultEPADHA;

#define DELOPT        1000 
#define DEFANAL       1001
#define SCREEN        1002
#define NEXT_RECIPE   1008
#define MEALSPERDAY   1009
#define GRAMS         1010 
#define AUTOCAL       1011
#define N6HUFA        1016
#define DEFANALANAL   1019
#define DEFANALREC    1020
#define LASTWLW       1021
#define LASTWLBFP     1022
#define LASTWLDATE    1023
#define WLTWEAK       1024
#define WLPOLARITY    1025

#define OPT_CHO_NONFIB 2000
#define OPT_LA         2001
#define OPT_AA         2002
#define OPT_ALA        2003
#define OPT_EPA        2004
#define OPT_DHA        2005
#define OPT_OMEGA6     2006
#define OPT_OMEGA3     2007
#define OPT_VITE       2008

extern struct opt options;

#ifdef __cplusplus
extern "C" {
#endif

void initialize_options(void);
void current_settings(int);
void set_calories(void);
void set_fat(void);
void set_protein(void);
void set_carb(void);
void set_fiber(void);
void set_sat(void);
void set_efa(void);
void set_n6(void);
void auto_cal(void);
void auto_del(void);
void screen(void);
void screen_previous(void);
void get_cals(float *);
void new_nut_levels(void);
void restore_defaults(void);
void efa_dynamics(void);
void weight_log();

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif
 
#endif

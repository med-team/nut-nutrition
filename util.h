/* util.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef UTIL_H
#define UTIL_H

#include "food.h"

#ifdef __cplusplus
extern "C" {
#endif

int get_char(void);
int get_int(void);
float get_float();
void get_string(char *, int);
void get_qty(float *, float *, float *);
float evaluate_qty(struct food *, char *);
int evaluate_action(char *);
void get_select(float *);
void spacer(int);
void header(char *);
void key_clean();
void key_put(char *);
void key_encode(char *, char *);
char *key_take();
void key_decode(char *, char *);
void test_signsense(float *);
int test_for_negative_zero(float *);

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif

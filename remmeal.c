/* remmeal.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "remmeal.h"
#include "util.h"
#include "db.h"
#include "options.h"

void remove_meals()
{
int menu_choice, junk;
header("NUT:  Delete Meals and Set Meals Per Day");
remove_meals_menu();
spacer(14);
printf("\nEnter your choice (or just <enter> to quit):  ");
menu_choice = get_int();
if (menu_choice < 1 || menu_choice > 6) return;
if (menu_choice == 6)
 {
 auto_del();
 return;
 }
if (meal_count(&meal_root) > 0)
 {
 header("NUT:  Delete Meals and Set Meals Per Day");
 remove_meals_menu();
 spacer(14);
 printf("\nAre you sure you want to ");
 switch (menu_choice)
  {
  case 1 : printf("delete all except last month\'s meals?  (y/n):  ");
           break; 
  case 2 : printf("delete all except last week\'s meals?  (y/n):  ");
           break; 
  case 3 : printf("delete all except last day\'s meals?  (y/n):  ");
           break; 
  case 4 : printf("delete all except last meal?  (y/n):  ");
           break; 
  case 5 : printf("delete all meals?  (y/n):  ");
           break; 
  }
 junk = get_char();
 if (junk != 'Y' && junk != 'y') return;
 }
switch (menu_choice)
 {
 case 1 : delete_meals(30*options.mealsperday);
            break; 
 case 2 : delete_meals(7*options.mealsperday);
            break; 
 case 3 : delete_meals(options.mealsperday);
            break; 
 case 4 : delete_meals(1);
            break; 
 case 5 : delete_meals(0);
            break; 
 }
write_meal_db();
if (menu_choice == 5)
 {
 header("NUT:  Delete Meals and Set Meals Per Day");
 spacer(1);
 printf("\nMeals per day currently set at %d.",options.mealsperday);
 printf("\nEnter 1-19 or just <enter> to retain current value:  ");
 junk = get_int();
 if (junk > 0 && junk < 20) options.mealsperday = junk;
 write_OPTIONS();
 }
}

void remove_meals_menu()
{
printf("\n\n                  1  --  Delete All Except Last Month\'s Meals\n\n");
printf("                  2  --  Delete All Except Last Week\'s Meals\n\n");
printf("                  3  --  Delete All Except Last Day\'s Meals\n\n");
printf("                  4  --  Delete All Except Last Meal\n\n");
printf("                  5  --  Delete All Meals and Set Meals Per Day\n\n");
printf("                  6  --  Automatic Deletion of Meals\n\n");
}

/* nutrient.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

char *Nutrient[] = {"Protein","Total Fat","Total Carb","Ash","Calories","Starch","Sucrose","Glucose","Fructose","Lactose","Maltose","Ethyl Alcohol","Water","Adj. Protein","Caffeine","Theobromine","Energy","Sugars","Galactose","Fiber","Calcium","Iron","Magnesium","Phosphorus","Potassium","Sodium","Zinc","Copper","Fluoride","Manganese","Selenium","Vitamin A","Retinol","Vit. A, RAE","B-Carotene","A-Carotene","A-Tocopherol","Vitamin D","Vitamin D2","Vitamin D3","Vitamin D2+D3","B-Cryptoxanth.","Lycopene","Lutein+Zeaxan.","B-Tocopherol","G-Tocopherol","D-Tocopherol","A-Tocotrienol","B-Tocotrienol","G-Tocotrienol","D-Tocotrienol","Vitamin C","Thiamin","Riboflavin","Niacin","Panto. Acid","Vitamin B6","Folate","Vitamin B12","Choline","Menaquinone-4","Dihydro-K1","Vitamin K1","Folic Acid","Folate, food","Folate, DFE","Betaine","Tryptophan","Threonine","Isoleucine","Leucine","Lysine","Methionine","Cystine","Phenylalanine","Tyrosine","Valine","Arginine","Histidine","Alanine","Aspartic acid","Glutamic acid","Glycine","Proline","Serine","Hydroxyproline","Vit. E added","Vit. B12 added","Cholesterol","Trans Fat","Sat Fat","4:0","6:0","8:0","10:0","12:0","14:0","16:0","18:0","20:0","18:1","18:2","18:3","20:4","22:6n-3","22:0","14:1","16:1","18:4","20:1","20:5n-3","22:1","22:5n-3","Phytosterols","Stigmasterol","Campesterol","BetaSitosterol","Mono Fat","Poly Fat","15:0","17:0","24:0","16:1t","18:1t","22:1t","18:2t","18:2i","18:2t,t","18:2CLA","24:1c","20:2n-6c,c","16:1c","18:1c","18:2n-6c,c","22:1c","18:3n-6c,c,c","17:1","20:3","TransMonoenoic","TransPolyenoic","13:0","15:1","18:3n-3c,c,c","20:3n-3","20:3n-6","20:4n-6","18:3i","21:5","22:4","18:1-11 t (18:1t n-7)","Protein Calories","Fat Calories","Carb Calories","Non-Fiber Carb","LA","AA","ALA","EPA","DHA","Omega-6","Short-chain Omega-6","Long-chain Omega-6","Omega-3","Short-chain Omega-3","Long-chain Omega-3","Vitamin E"};

char *Unit[] = {"g","g","g","g","kc","g","g","g","g","g","g","g","g","g","mg","mg","kJ","g","g","g","mg","mg","mg","mg","mg","mg","mg","mg","mcg","mg","mcg","IU","mcg","mcg","mcg","mcg","mg","IU","mcg","mcg","mcg","mcg","mcg","mcg","mg","mg","mg","mg","mg","mg","mg","mg","mg","mg","mg","mg","mg","mcg","mcg","mg","mcg","mcg","mcg","mcg","mcg","mcg","mg","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","mg","mcg","mg","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","mg","mg","mg","mg","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","g","kc","kc","kc","g","g","g","g","g","g","g","g","g","g","g","g","IU"};


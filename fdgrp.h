/* fdgrp.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

char *FdGrp[] = {"Added Recipes and Labeled Foods","Dairy and Egg Products","Spices and Herbs","Baby Foods","Fats and Oils","Poultry Products","Soups, Sauces, and Gravies","Sausages and Luncheon Meats","Breakfast Cereals","Fruits and Fruit Juices","Pork Products","Vegetables and Vegetable Products","Nut and Seed Products","Beef Products","Beverages","Finfish and Shellfish Products","Legumes and Legume Products","Lamb, Veal, and Game Products","Baked Products","Sweets","Cereal Grains and Pasta","Fast Foods","Meals, Entrees, and Side Dishes","","","Snacks","","","","","","","","","","Am. Indian/Alaska Native Foods","Restaurant Foods"};
#define FD_GRP_COUNT 26
int FdGrpMap[FD_GRP_COUNT] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25,35,36};


prefix = /usr/local
libdir = $(prefix)/lib/nut
bindir = $(prefix)/bin
mandir = $(prefix)/man/man1

NUTDIR := \".nutdb\"
FOODDIR := \"$(libdir)\"
EXECUTABLE := nut
LIBS := -lm

FLTK_CONFIG := $(shell fltk-config --version 2>/dev/null)
ifeq ($(FLTK_CONFIG),)
HAVE_FLTK = 0
FLTK_NUT =
else
HAVE_FLTK = 1
FLTK_NUT = fltk/Nut
endif

OS := $(shell uname -s)
ifeq ($(OS),HP-UX)
CC = cc
OPT = -O
else
OPT = -O3
endif

ifeq ($(OS),HP-UX)
CFLAGS := $(OPT) -DNUTDIR=$(NUTDIR) -DFOODDIR=$(FOODDIR)
else
CFLAGS := $(OPT) -DNUTDIR=$(NUTDIR) -DFOODDIR=$(FOODDIR)
endif

CXXFLAGS := $(CFLAGS) `fltk-config --cxxflags`

SOURCE := $(wildcard *.c)
OBJS := $(patsubst %.c,%.o,$(SOURCE))

all: nut $(FLTK_NUT)

nut:	$(OBJS)
	$(CC) $(OPT) $(LDFLAGS) -o $(EXECUTABLE) $(OBJS) $(LIBS)

ifeq ($(HAVE_FLTK),1)
$(FLTK_NUT):
	-cd fltk; $(MAKE)
endif

deps:
	makedepend -Y. *.c >/dev/null 2>&1

clean:
	rm -f *.o $(EXECUTABLE) preprocess/*.o preprocess/dbabbrev preprocess/dbjw core
ifeq ($(HAVE_FLTK),1)
	-cd fltk; $(MAKE) clean
endif

install: all
	-mkdir -p -m 755 $(bindir) $(libdir) $(mandir)
	-cp nut $(FLTK_NUT) $(bindir)
	-cd $(bindir); strip nut Nut
	-rm -f $(libdir)/sr*.nut 2>/dev/null
	-cp raw.data/* $(libdir)
	-cp nut.1 $(mandir)
	-chmod a+r $(libdir)/* $(mandir)/nut.1

# DO NOT DELETE

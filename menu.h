/* menu.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MENU_H
#define MENU_H

#ifdef __cplusplus
extern "C" {
#endif

void menu(void);
void view_nuts_menu(void);
void add_foods_menu(void);
void personal_menu(void);
void screen_subvert_rank(int);
void screen_subvert_trendy(void);

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif

/* food.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FOOD_H
#define FOOD_H

#include <stdio.h>
#include "nut.h"

#define MAX_FOOD 12000
#define DV_COUNT 39
#define CARBAMINO_COUNT 31
#define MISC_COUNT 39
#define SATMONO_COUNT 27
#define POLYTRANS_COUNT 30
#define GRAMS_IN_OUNCE 28.349523
#define ALC_CAL_FACTOR 6.93
#define MAX_SCREEN 6

struct food
 {
 int   ndb_no;
 int   fdgrp;
 int   food_no;
 char  name[61];
 float qty;
 char  unit[81];
 int   refuse;
 float prot_cal_factor;
 float fat_cal_factor;
 float cho_cal_factor;
 float grams;
 float nutrient[NUTRIENT_COUNT];
 char pcf;
 struct food *next;
 };

extern struct food food_root, recipe_root, food_work, *new_food, *new_recipe, *FoodIndex[];
extern char *Nutrient[];
extern char *Unit[];
extern char *FdGrp[];
extern float DV[];
extern float DVBase[];
extern float NoData;
extern int DVMap[];
extern int DV1Map[];
extern int DVnotOK;
extern int CarbAminoMap[];
extern int MiscMap[];
extern int SatMonoMap[];
extern int PolyTransMap[];
extern int *ScreenMap[];
extern char *ScreenTitle[];
extern int FdGrpMap[];
extern int MaxFdGrp;
extern int LookupNutrNo[];
extern int LookupNut[];
extern int FoodCount;
extern int Abbrev_Count;
extern const char *Abbreviations[];

#ifdef __cplusplus
extern "C" {
#endif

void initializations(void);
int order_new_food(void);
int namestrcmp(char *, char *);
int substring_finder(char *, char *);
struct food *food_choice(char *, int);
void food_subcat(char *, char *, struct food **, int);
void food_show(struct food *, float *);
void make_food_index(void);
int modify_food_index(int, struct food *);
struct food *food_number(int);
int find_ndbno(int);
struct food *find_ndbno_ptr(int);
void clear_work(void);
void food_display(FILE *);
void food_display_line(FILE *, int, int, int, int, int);
void order_new_recipe(void);
void modify_recipe_food(int, char *);
void modify_label_food(int, char *);
void compute_derived_fields(struct food *);
char *format_serving(char *, float *, struct food *);
struct meal *load_foodwork(int, struct meal *);
float n6hufa(float, float, float, float, float, float);

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif

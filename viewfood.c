/* viewfood.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "options.h"
#include "util.h"

void view_foods()
{
struct food *food_ptr = NULL;
float ratio = -1, one = 1;
int savescreen;

if (DVnotOK)
 {
 savescreen = options.screen;
 options.screen = 0;          
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = savescreen;
 }

key_clean();
for ( ; ; )
 {
 if (ratio <= 0)
  {
  food_ptr = food_choice("NUT:  View Foods", 0);
  if (food_ptr == (struct food *) -1) return;
  if (food_ptr == (struct food *) 0) key_clean();
  if (food_ptr == (struct food *) 0) continue;
  }
 header("NUT:  View Foods");
 if (ratio > 0) food_show(food_ptr,&ratio);
 else food_show(food_ptr,&one);
 get_qty(&ratio, &(food_ptr->grams), &(food_ptr->nutrient[ENERC_KCAL]));
 if (ratio == -383838) key_take();
/* else if (ratio > 0 && ratio > 100) ratio = 100; */
 else if (ratio == 0) key_clean();
 }
}

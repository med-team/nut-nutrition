/* theusual.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "options.h"
#include "util.h"
#include "db.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void theusual()
{
struct food *food_ptr;
char meal_date[9];
char new_meal_date[9];
char key[121];
char buff[128];
char *token;
int c, d, modfood, junk;
float ratio = 0, one = 1;

if (DVnotOK)
 {
 junk = options.screen;
 options.screen = 0;          
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = junk;
 }

key[0] = '\0';
header("NUT:  Record \"The Usual\"--Customary Meals");
#ifndef DOS
printf("When you record Customary Meals, you can save time on data entry.  For instance,\n");
#else 
printf("When you record Customary Meals, you can save time on data entry.  For instance\n");
#endif
printf("if you record a customary meal named \"LUNCH\", you can then type \"theusuallunch\"\n");
printf("on the \"Record Meals\" screen and all the foods will added as specified.\n\n");
spacer(theusual_list()+3);
printf("\nType \"The Usual\" Identifier (max. 8 characters):  ");
get_string(new_meal_date,8);
if (strlen(new_meal_date) == 0) return;
for (c = 0 ; c < 9 ; c++) meal_date[c] = toupper(new_meal_date[c]);
for (c = 0 ; c < 9 ; c++) if (meal_date[c] == ' ') break;
for (d = c ; d < 9 ; d++) meal_date[d] = '\0';
for ( ; ; )
 {
 for ( ; ; )
  {
  if ((ratio != -383838) && (theusual_find(meal_date) != 0))
   {
   header("NUT:  Record \"The Usual\"--Customary Meals");
   if (! theusual_show(meal_date))
    {
    printf("\nPress <enter> to continue...");
    junk = get_int();
    }
   else
    {
    printf("\nEnter food name, # to delete, <enter> to quit:  ");
    get_string(key,60);
    if (strcmp(key,"") == 0) 
     {
     meal_date[0] = '\0';
     theusual();
     }
    }
   }
   if (meal_date[0] == '\0') return;
   modfood = atoi(key);
   strncpy(buff,key,60);
   token = strtok(buff, ", ");
   token = strtok(NULL,", "); 
   if (strcmp(key,"") == 0 || modfood == 0) break;
   modify_theusual(meal_date,modfood,token);
   write_theusual_db();
   strcpy(key,"");
  }
 key_put(key);
 food_ptr = food_choice("NUT:  Record \"The Usual\"--Customary Meals", 0);
 if (food_ptr == (struct food *) -1) 
  {
  theusual();
  return;
  }
 if (food_ptr == (struct food *)  0) key_clean();
 if (food_ptr == (struct food *)  0) continue;
 header("NUT:  Record \"The Usual\"--Customary Meals");
 food_show(food_ptr, &one);
 get_qty(&ratio, &(food_ptr->grams), &(food_ptr->nutrient[ENERC_KCAL]));
 if (ratio == -383838)
  {
  key_take();
  strcpy(key,key_take());
  }
 if (ratio != 0 && ratio != -383838)
  {
  if ((new_theusual = malloc(sizeof(struct meal))) == NULL)
   {
   printf("We are out of memory.  Bummer.\n");
   abort();
   } 
  new_theusual->ndb_no = food_ptr->ndb_no;
  new_theusual->food_no = food_ptr->food_no;
  strcpy(new_theusual->meal_date,meal_date);
  new_theusual->meal = 38;
  new_theusual->grams = food_ptr->grams * ratio;
  order_new_theusual();
  write_theusual_db();
  }
 }
}

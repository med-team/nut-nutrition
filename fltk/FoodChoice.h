/* FoodChoice.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FOODCHOICE_H
#define FOODCHOICE_H

#include "Nut.h"
#include "CancelWidget.h"

class FoodButton;

class FoodChoice : public Fl_Group
{
public:
FoodChoice(int x, int y, int w, int h, Fl_Color widgetcolor, bool specialheader);
void food_present(int foodno);
void search_focus(void);
void search(void);
void modify_food_button(int foodno);
void modify_food_button_non_recursive(int foodno);
void reindex_foodbuttons(int foodnum);
Nut_Input *search_box;
CancelWidget *cancel_button;

protected:
void resize(int x, int y, int w, int h);

private:
Nut_Scroll *ancient_scroll;
Nut_Pack *foodpack;
FoodButton *FoodButtonArray[MAX_FOOD];
int foodsieve[MAX_FOOD];
bool specialheader;
};

#endif

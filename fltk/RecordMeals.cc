/* Class RecordMeals */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "GOLightButton.h"
#include "ViewFoods.h"
#include "Nut_MenuButton.h"
#include "db.h"
#include <FL/Fl.H>
#include <stdlib.h>
#ifndef __hpux
#include <stdint.h>
#endif
#include <string.h>
#include <ctype.h>

static bool user_has_spoken;

static void meals_per_day_cb(Fl_Widget *which, void *choice)
{
intptr_t meals_per_day = (intptr_t) choice;
delete_meals(0);
write_meal_db();
options.mealsperday = meals_per_day;
write_OPTIONS();
request_mvo_initial_set();
}

static Fl_Menu_Item meals_per_day_pulldown[] = {
  {"Set 1 meal per day",   0, meals_per_day_cb, (void*)1},
  {"Set 2 meals per day",   0, meals_per_day_cb, (void*)2},
  {"Set 3 meals per day",   0, meals_per_day_cb, (void*)3},
  {"Set 4 meals per day",   0, meals_per_day_cb, (void*)4},
  {"Set 5 meals per day",   0, meals_per_day_cb, (void*)5},
  {"Set 6 meals per day",   0, meals_per_day_cb, (void*)6},
  {"Set 7 meals per day",   0, meals_per_day_cb, (void*)7},
  {"Set 8 meals per day",   0, meals_per_day_cb, (void*)8},
  {"Set 9 meals per day",   0, meals_per_day_cb, (void*)9},
  {"Set 10 meals per day",   0, meals_per_day_cb, (void*)10},
  {"Set 11 meals per day",   0, meals_per_day_cb, (void*)11},
  {"Set 12 meals per day",   0, meals_per_day_cb, (void*)12},
  {"Set 13 meals per day",   0, meals_per_day_cb, (void*)13},
  {"Set 14 meals per day",   0, meals_per_day_cb, (void*)14},
  {"Set 15 meals per day",   0, meals_per_day_cb, (void*)15},
  {"Set 16 meals per day",   0, meals_per_day_cb, (void*)16},
  {"Set 17 meals per day",   0, meals_per_day_cb, (void*)17},
  {"Set 18 meals per day",   0, meals_per_day_cb, (void*)18},
  {"Set 19 meals per day",   0, meals_per_day_cb, (void*)19},
{0}};

void show_analysis_cb(Fl_Widget *which, void *who)
{
RecordMeals *us_right_now = (RecordMeals *)who;
us_right_now->analyze(0);
redraw_whole_schmeer();
}

void search_rm_cb(Fl_Widget *which, void *who)
{
RecordMeals *who_we_are = (RecordMeals *) who;
who_we_are->value(who_we_are->fc);
who_we_are->fc->search_focus();
}

RecordMeals::RecordMeals (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
int i;
this->label("Record Meals");
this->labeltype(FL_ENGRAVED_LABEL);
for (i = 0; i < NUTRIENT_COUNT; i++) zero.nutrient[i] = 0.00000000000001;
user_has_spoken = 0;
potentialmeal = new NutButtonWidget(x, y, w, h, widgetcolor, &zero);
this->end();

Fl_Group *wizgroup;
for (i = 0; i < MAX_SCREEN; i++)
 {
 wizgroup = new Fl_Group(x, y, w, 6*h/22);
  {
  Nut_Box *o = new Nut_Box(w/44, 2*h/245+4*h/77, w/16-w/80, h/22, "Meal");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 mvo = new MealValueOutput(w/44+w/16-w/80, 2*h/245+4*h/77, w/8, h/22, widgetcolor, this);
  {
  Nut_MenuButton *o = new Nut_MenuButton(2*w/3, 2*h/245+4*h/77, 20*w/64, h/22,"Delete All Meals and Set Meals Per Day");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  o->color(fl_lighter(widgetcolor));
  o->clear_visible_focus();
  o->menu(meals_per_day_pulldown);
  }
 mmw = new MissMealWidget(w/44, 2*h/245+8*h/77,  3*w/16, h/22, widgetcolor, mvo);
  {
  Nut_Box *o = new Nut_Box(8*w/27, 2*h/245+8*h/77, 5*w/9, h/22, "Food Search:");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 tumb = new TheUsualMenuButton(w/44, 2*h/245+12*h/77, 3*w/16, h/22, widgetcolor, this);
  {
  Nut_Button *o = new Nut_Button(8*w/27, 2*h/245+12*h/77, 5*w/9, h/22);
  o->box(FL_DOWN_BOX);
  o->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))), fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
  o->clear_visible_focus();
  o->callback(search_rm_cb,this);
  o->tooltip("");
  }
 wizgroup->end();
 potentialmeal->wizard[i][0]->add(wizgroup);
 }

actualmeal = new Fl_Group(x, y, w, h);
actualmeal->color(widgetcolor);
 {
 Nut_Box *o = new Nut_Box(w/44, 2*h/245+4*h/77, w/16-w/80, h/22, "Meal");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
mvo = new MealValueOutput(w/44+w/16-w/80, 2*h/245+4*h/77, w/8, h/22, widgetcolor, this);
new GOLightButton(53*w/60,2*h/245+4*h/77,w/10,h/22,widgetcolor,1);
mmw = new MissMealWidget(w/44, 2*h/245+8*h/77,  3*w/16, h/22, widgetcolor, mvo);
 {
 Nut_Box *o = new Nut_Box(8*w/27, 2*h/245+8*h/77, 5*w/9, h/22, "Food Search:");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
new GOLightButton(53*w/60,2*h/245+8*h/77,w/10,h/22,widgetcolor,0);
tumb = new TheUsualMenuButton(w/44, 2*h/245+12*h/77, 3*w/16, h/22, widgetcolor, this);
 {
 Nut_Button *o = new Nut_Button(8*w/27, 2*h/245+12*h/77, 5*w/9, h/22);
 o->box(FL_DOWN_BOX);
 o->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))), fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 o->clear_visible_focus();
 o->callback(search_rm_cb,this);
 o->tooltip("");
 }
tunw = new TheUsualNameWidget(w/44,2*h/245+16*h/77,3*w/8 + 2*w/80, h/22, widgetcolor, this);
tunw->off();
 {
 Nut_Button *o = new Nut_Button(13*w/15, 2*h/245+16*h/77, w/8, h/22, "Analysis");
 o->color(fl_lighter(MEALCOLOR));
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->labelfont(FL_BOLD);
 o->clear_visible_focus();
 o->callback(show_analysis_cb,this);
 }
ancient_scroll = new Nut_Scroll(FL_NORMAL_SIZE/3,2*h/245+20*h/77,w-FL_NORMAL_SIZE/3,18*h/23);
ancient_scroll->type(Fl_Scroll::VERTICAL);    
ancient_scroll->color(scheme_color(widgetcolor));
foodpack = new Nut_Pack(FL_NORMAL_SIZE/3,2*h/245+20*h/77,w-2*FL_NORMAL_SIZE/3,18*h/23);
foodpack->end();
ancient_scroll->end();
actualmeal->end();
this->add(actualmeal);

analysis = new AnalyzeMeals (x, y, w, h, widgetcolor);
this->add(analysis);
analysis->set_specialheader(this, &current_temp_meal_root);

fc  = new FoodChoice(x, y, w, h, widgetcolor, 1);
fc->cancel_button->on();
this->add(fc);

this->value(potentialmeal);
potentialmeal->update();
potentialmeal->redraw();
Fl::flush();
}

void RecordMeals::tab_change(void)
{
if (this->value() != potentialmeal || user_has_spoken) return;
mvo->initial_set();
mmw->update(mvo->current_meal_date);
}

void RecordMeals::add_to_meal(ViewFoods *vf)
{
bool first_meal_in_the_middle_of_nowhere = 0;
this->value(actualmeal);
tunw->off();
struct food *foodptr = vf->what_food();
float grams = vf->how_much();

struct meal *meal_ptr = meal_find(mvo->current_meal_date,mvo->current_meal);
if (meal_ptr == NULL) first_meal_in_the_middle_of_nowhere = 1;

if ((new_meal = (struct meal *) malloc(sizeof(struct meal))) == NULL)
 {
 printf("We are out of memory.  Bummer.\n");
 abort();
 }
new_meal->ndb_no = foodptr->ndb_no;
new_meal->food_no = foodptr->food_no;
strcpy(new_meal->meal_date,mvo->current_meal_date);
new_meal->meal = mvo->current_meal;
new_meal->grams = grams;
order_new_meal();

if (first_meal_in_the_middle_of_nowhere) current_temp_meal_root = prev_meal(new_meal);

MealFood *mf = new MealFood(x(), y(), 59*w()/60, h()/23, foodptr, new_meal, grams, scheme_color(MEALCOLOR));
foodpack->add(mf);
mmw->update(mvo->current_meal_date);
defanal_change(this);
mf->pcf->PCF_computation();
}

void RecordMeals::direct_add_to_meal(struct food *foodptr, float grams)
{
bool first_meal_in_the_middle_of_nowhere = 0;
this->value(actualmeal);
tunw->off();

struct meal *meal_ptr = meal_find(mvo->current_meal_date,mvo->current_meal);
if (meal_ptr == NULL) first_meal_in_the_middle_of_nowhere = 1;

if ((new_meal = (struct meal *) malloc(sizeof(struct meal))) == NULL)
 {
 printf("We are out of memory.  Bummer.\n");
 abort();
 }
new_meal->ndb_no = foodptr->ndb_no;
new_meal->food_no = foodptr->food_no;
strcpy(new_meal->meal_date,mvo->current_meal_date);
new_meal->meal = mvo->current_meal;
new_meal->grams = grams;
order_new_meal();

if (first_meal_in_the_middle_of_nowhere) current_temp_meal_root = prev_meal(new_meal); 

MealFood *mf = new MealFood(x(), y(), 59*w()/60, h()/23, foodptr, new_meal, grams, scheme_color(MEALCOLOR));
foodpack->add(mf);
mmw->update(mvo->current_meal_date);
defanal_change(this);
mf->pcf->PCF_computation();
}

void RecordMeals::analyze(bool dv_change_only)
{                                  
analysis->update_am(-1);
this->value(analysis);
}

void RecordMeals::back_to_menu(void)
{
this->value(actualmeal);
tunw->off();
}

void RecordMeals::food_choice_cancel(void)
{                                   
this->value(actualmeal);
tunw->off();
}

void RecordMeals::delete_meal_food(MealFood *mf)
{
MealFood *mealfood;
mf->hide();
ancient_scroll->redraw();
pcfclear(mf->mfmealptr);
delete_meal_with_ptr(mf->mfmealptr);
foodpack->remove(mf);
ancient_scroll->redraw();
Fl::delete_widget(mf);
Fl::flush();
mmw->update(mvo->current_meal_date);
defanal_change(this);
if (foodpack->children() > 0)
 {
 mealfood = (MealFood *) foodpack->child(0);
 mealfood->pcf->PCF_computation();          
 }
}

void RecordMeals::delete_meal_food_with_replace(MealFood *mf)
{
MealFood *mealfood;
show_food_with_replace(mf->mffoodptr, mf->mffoodbutton->grams());
pcfclear(mf->mfmealptr);
delete_meal_with_ptr(mf->mfmealptr);
foodpack->remove(mf);
ancient_scroll->redraw();
Fl::delete_widget(mf);
Fl::flush();
mmw->update(mvo->current_meal_date);
defanal_change(this);
if (foodpack->children() > 0)
 {
 mealfood = (MealFood *) foodpack->child(0);
 mealfood->pcf->PCF_computation();
 }
}

void RecordMeals::update(bool dv_change_only)
{
if (value() != potentialmeal && dv_change_only) analysis->update_am_dv_change_only();
else if (value() != potentialmeal && !dv_change_only) analysis->update_am(-1);
}

void RecordMeals::meal_change(void)
{
MealFood *mf = NULL;
user_has_spoken = 1;
foodpack->clear();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
clear_out_pcfs();
pcfclear(NULL);
ancient_scroll->redraw();
delete_meals(options.delopt);
struct meal *meal_ptr = meal_find(mvo->current_meal_date,mvo->current_meal);
if (meal_ptr == NULL)
 {
 current_temp_meal_root = &meal_root;
 mmw->update(mvo->current_meal_date);
 return;
 }
this->value(actualmeal);
tunw->off();
current_temp_meal_root = prev_meal(meal_ptr);
while (strcmp(mvo->current_meal_date,meal_ptr->meal_date) == 0 && mvo->current_meal == meal_ptr->meal)
 {
 mf = new MealFood(x(), y(), 59*w()/60, h()/23, FoodIndex[meal_ptr->food_no], meal_ptr, meal_ptr->grams, scheme_color(MEALCOLOR));
 foodpack->add(mf);
 if (meal_ptr-> next == NULL) break;
 meal_ptr = meal_ptr->next;
 }
mmw->update(mvo->current_meal_date);
defanal_change(this);
if (mf != NULL) mf->pcf->PCF_computation();
}

void RecordMeals::add_theusual(struct meal *theusualptr)
{
bool first_meal_in_the_middle_of_nowhere = 0;
this->value(actualmeal);
tunw->off();

struct meal *meal_ptr = meal_find(mvo->current_meal_date,mvo->current_meal);
if (meal_ptr == NULL) first_meal_in_the_middle_of_nowhere = 1;

meal_ptr = theusualptr;
MealFood *mf = NULL;

while (meal_ptr != NULL && strcmp(meal_ptr->meal_date,theusualptr->meal_date) == 0)
 {
 if ((new_meal = (struct meal *) malloc(sizeof(struct meal))) == NULL)
  {
  printf("We are out of memory.  Bummer.\n");
  abort();
  }
 *new_meal = *meal_ptr;
 strcpy(new_meal->meal_date,mvo->current_meal_date);
 new_meal->meal = mvo->current_meal;
 order_new_meal();

 if (first_meal_in_the_middle_of_nowhere)
  {
  current_temp_meal_root = prev_meal(new_meal);
  first_meal_in_the_middle_of_nowhere = 0;
  }

 mf = new MealFood(x(), y(), 59*w()/60, h()/23, FoodIndex[meal_ptr->food_no], new_meal, new_meal->grams, scheme_color(MEALCOLOR));
 foodpack->add(mf);
 meal_ptr = meal_ptr->next;
 }
mmw->update(mvo->current_meal_date);
defanal_change(this);
mf->pcf->PCF_computation();
}

void RecordMeals::new_customary_meal_phase_one(void)
{                                                       
if (value() == potentialmeal || foodpack->children() == 0) return;
tunw->on();
}

void RecordMeals::new_customary_meal_phase_two(const char *theusualid)
{
int c, d;
for (c = 0 ; c < 8 ; c++) theusual_id[c] = toupper(theusualid[c]);
for (c = 0 ; c < 8 ; c++) if (theusual_id[c] == ' ') break;
for (d = c ; d < 9 ; d++) theusual_id[d] = '\0';
if (theusual_id[0] == '\0')
 {
 tunw->off();
 return;
 }
struct meal *meal_ptr = prev_meal(meal_find(mvo->current_meal_date,mvo->current_meal));
char *mdate = meal_ptr->next->meal_date;
int mnum = meal_ptr->next->meal;
while (meal_ptr->next != NULL && strcmp(mdate,meal_ptr->next->meal_date) == 0 &&meal_ptr->next->meal == mnum)
 {
 meal_ptr = meal_ptr->next;
 if ((new_theusual = (struct meal *) malloc(sizeof(struct meal))) == NULL)
  {
  printf("We are out of memory.  Bummer.\n");
  abort();
  }
 *new_theusual = *meal_ptr;
 strcpy(new_theusual->meal_date,theusual_id);
 new_theusual->meal = 38;
 order_new_theusual();
 }
write_theusual_db();
Fl::flush();
tumb->initialize_theusual();
tunw->success(theusual_id);
}

struct meal *RecordMeals::tell_me_meal_root(void)
{
return current_temp_meal_root;
}

void RecordMeals::reindex_foodbuttons(int foodnum)
{
fc->reindex_foodbuttons(foodnum);
}

void RecordMeals::resize(int x, int y, int w, int h)
{
Fl_Wizard::resize(x, y, w, h);
for (int i = 0; i < 19; i++) meals_per_day_pulldown[i].labelsize(FL_NORMAL_SIZE);
}

void RecordMeals::update_pcf(void)
{
MealFood *mealfood;
if (foodpack->children() > 0)
 {
 mealfood = (MealFood *) foodpack->child(0);        
 mealfood->pcf->PCF_computation();
 }
}

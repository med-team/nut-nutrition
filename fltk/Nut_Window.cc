/* Class Nut_Window */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <FL/Fl_Tooltip.H>
#include "Nut_Window.h"
#include "db.h"

Nut_Window::Nut_Window (int w, int h, const char *title) : Fl_Double_Window (w, h, title)
{
color(FL_WHITE);
resizable(this);
#if defined NOT_XFT
size_range(558, 344, 1674, 1032, 0, 0, 1);
#else
size_range(594, 367, 1782, 1101, 0, 0, 1);
#endif
}

void Nut_Window::resize(int x, int y, int w, int h)
{
int pixels;
#if defined NOT_XFT
pixels = w/50- 4;
#else
pixels = w/64 - 1;
#endif
FL_NORMAL_SIZE = pixels;
Fl_Tooltip::size(FL_NORMAL_SIZE);
Fl::scrollbar_size(FL_NORMAL_SIZE);
write_fontsize(pixels);
Fl_Double_Window::resize(x, y, w, h);
}

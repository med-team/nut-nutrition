/* Class NutCharts */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "NutCharts.h"
#include <FL/Fl.H>
#include <string.h>

NutCharts::NutCharts (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Group (x, y, w, h)
{
this->color(widgetcolor);

n = new Nut_LineChart(x,y+h/25,w,h/5);
f = new Fl_Chart(x,y+2*h/25+h/5,w,h/5,"Fat Calories");
c = new Fl_Chart(x,y+3*h/25+2*h/5,w,h/5,"Carb Calories");
p = new Fl_Chart(x,y+4*h/25+3*h/5,w,h/5,"Protein Calories");

n->type(FL_LINE_CHART);
n->box(FL_UP_BOX);
f->box(FL_UP_BOX);
c->box(FL_UP_BOX);
p->box(FL_UP_BOX);
n->color(widgetcolor);
f->color(widgetcolor);
c->color(widgetcolor);
p->color(widgetcolor);
n->autosize(1);
f->autosize(1);
c->autosize(1);
p->autosize(1);

this->end();
}

void NutCharts::populate(int nutnum, int screen)
{
int day = -1, i;
struct meal *meal_ptr = options.temp_meal_root;

n->clear();
f->clear();
c->clear();
p->clear();

for (i = 0; i < 1001; i++)
 {
 protein[i] = carb[i] = fat[i] = nut[i] = 0;
 mealdate[i][0] = '\0';
 }
if (screen > 0 || DV[nutnum] == 0) n->label(Nutrient[nutnum]);
else
 {
 sprintf(labelbuf,"%s Daily Value %1.1f %s",Nutrient[nutnum],DV[nutnum],Unit[nutnum]);
 n->label(labelbuf);
 }

while (meal_ptr->next != NULL)
 {
 meal_ptr = meal_ptr->next;
 if (strcmp(mealdate[day],meal_ptr->meal_date) != 0) strcpy(mealdate[++day],meal_ptr->meal_date);
  protein[day] += FoodIndex[meal_ptr->food_no]->nutrient[PROT_KCAL] * meal_ptr->grams / 100;
  carb[day] += FoodIndex[meal_ptr->food_no]->nutrient[CHO_KCAL] * meal_ptr->grams / 100;
  fat[day] += FoodIndex[meal_ptr->food_no]->nutrient[FAT_KCAL] * meal_ptr->grams / 100;
  nut[day] += FoodIndex[meal_ptr->food_no]->nutrient[nutnum] * meal_ptr->grams / 100;
 if (day == 1000) break;
 }
float maxn = 0, minn = 1e38, maxf = 0, maxc = 0, maxp = 0, max = 0;

if (options.defanal != 0) day = options.defanal / options.mealsperday;

if (day < 32)
 {
 f->type(FL_BAR_CHART);
 c->type(FL_BAR_CHART);
 p->type(FL_BAR_CHART);
 }
else
 {
 f->type(FL_FILL_CHART);
 c->type(FL_FILL_CHART);
 p->type(FL_FILL_CHART);
 }

for (i = day; i >= 0; i--)
 {
 if (nut[i] > maxn) maxn = nut[i];
 if (nut[i] < minn) minn = nut[i];
 if (fat[i] > maxf) maxf = fat[i];
 if (carb[i] > maxc) maxc = carb[i];
 if (protein[i] > maxp) maxp = protein[i];
 if (fat[i] + carb[i] + protein[i] > max) max = fat[i] + carb[i] + protein[i];
 }
if (screen == 0 && DV[nutnum] > 0)
 {
 maxn -= DV[nutnum];
 n->bounds(-DV[nutnum], DV[nutnum]);
 if (maxn > DV[nutnum]) n->bounds(-maxn, maxn);
 }
else
 {
 n->bounds(0, maxn);
 }

f->bounds(0, max);
c->bounds(0, max);
p->bounds(0, max);

if (screen == 0)
 {
 if (options.defanal < options.mealsperday)
  {
  if (Fl::scheme() && !strcmp(Fl::scheme(), "plastic")) n->add(nut[day] - DV[nutnum], 0, scheme_color(FL_BLACK));
  else n->add(nut[day] - DV[nutnum], 0, scheme_color(MEALCOLOR));
  f->add(fat[day], 0, scheme_color(MEALCOLOR));
  c->add(carb[day], 0, scheme_color(MEALCOLOR));
  p->add(protein[day], 0, scheme_color(MEALCOLOR));
  }

 for (i = day-1; i >= 0; i--)
  {
  if (Fl::scheme() && !strcmp(Fl::scheme(), "plastic")) n->add(nut[i] - DV[nutnum], 0, scheme_color(FL_BLACK));
  else n->add(nut[i] - DV[nutnum], 0, scheme_color(MEALCOLOR));
  f->add(fat[i], 0, scheme_color(MEALCOLOR));
  c->add(carb[i], 0, scheme_color(MEALCOLOR));
  p->add(protein[i], 0, scheme_color(MEALCOLOR));
  }
 }

else
 {
 if (options.defanal < options.mealsperday)
  {
  if (Fl::scheme() && !strcmp(Fl::scheme(), "plastic")) n->add(nut[day], 0, scheme_color(FL_BLACK));
  else n->add(nut[day], 0, scheme_color(MEALCOLOR));
  f->add(fat[day], 0, scheme_color(MEALCOLOR));
  c->add(carb[day], 0, scheme_color(MEALCOLOR));
  p->add(protein[day], 0, scheme_color(MEALCOLOR));
  }

 for (i = day-1; i >= 0; i--)
  {
  if (Fl::scheme() && !strcmp(Fl::scheme(), "plastic")) n->add(nut[i], 0, scheme_color(FL_BLACK));
  else n->add(nut[i], 0, scheme_color(MEALCOLOR));
  f->add(fat[i], 0, scheme_color(MEALCOLOR));
  c->add(carb[i], 0, scheme_color(MEALCOLOR));
  p->add(protein[i], 0, scheme_color(MEALCOLOR));
  }
 }

}

void NutCharts::resize(int x, int y, int w, int h)
{
n->labelsize(FL_NORMAL_SIZE);
f->labelsize(FL_NORMAL_SIZE);
c->labelsize(FL_NORMAL_SIZE);
p->labelsize(FL_NORMAL_SIZE);
Fl_Group::resize(x, y, w, h);
}

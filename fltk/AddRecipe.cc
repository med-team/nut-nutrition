/* Class AddRecipe */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "GOLightButton.h"
#include "AddRecipe.h"
#include "db.h"
#include <FL/Fl.H>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void search_ar_cb(Fl_Widget *which, void *who)
{
AddRecipe *who_we_are = (AddRecipe *) who;
who_we_are->value(who_we_are->fc);
who_we_are->fc->search_focus();
}

void recipe_done(Fl_Widget *which, void *who)
{
AddRecipe *who_we_are = (AddRecipe *) who;
who_we_are->recipe_done();
}

void cancel_recipe_cb(Fl_Widget *which, void *who)
{
AddRecipe *who_we_are = (AddRecipe *) who;
who_we_are->new_serving_unit_box->value("");
who_we_are->new_serving_qty_box->value("");
who_we_are->cancel_recipe();
}

void save_recipe_cb(Fl_Widget *which, void *who)
{
AddRecipe *who_we_are = (AddRecipe *) who;
who_we_are->save_recipe();
}


AddRecipe::AddRecipe (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
this->label("Add Recipe");
this->labeltype(FL_ENGRAVED_LABEL);
savex = x;
savey = y;
savew = w;
saveh = h;
recipelist = new Fl_Group(x, y, w, h);
recipelist->color(widgetcolor);
intro = new Nut_Box(w/44, 2*h/245+4*h/77, 4*w/16+35*w/96+29*w/1280, h/22, "Find the recipe ingredients in the database and show their quantities in the same manner as recording a meal.");
intro->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
new GOLightButton(53*w/60,2*h/245+4*h/77,w/10,h/22,widgetcolor,1);
 {
 Nut_Box *o = new Nut_Box(8*w/27, 2*h/245+8*h/77, 5*w/9, h/22, "Food Search:");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
new GOLightButton(53*w/60,2*h/245+8*h/77,w/10,h/22,widgetcolor,0);
 {
 Nut_Button *o = new Nut_Button(8*w/27, 2*h/245+12*h/77, 5*w/9, h/22);
 o->box(FL_DOWN_BOX);
 o->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))), fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 o->clear_visible_focus();
 o->callback(search_ar_cb,this);
 o->tooltip("");
 }
 {
 Nut_Button *o = new Nut_Button(13*w/15, 2*h/245+16*h/77, w/8, h/22, "Recipe Done");
 o->color(fl_lighter(widgetcolor));
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->labelfont(FL_BOLD);
 o->clear_visible_focus();
 o->callback(save_recipe_cb,this);
 }
ancient_scroll = new Nut_Scroll(FL_NORMAL_SIZE/3,2*h/245+20*h/77,w-FL_NORMAL_SIZE/3,18*h/23);
ancient_scroll->type(Fl_Scroll::VERTICAL);    
ancient_scroll->color(scheme_color(widgetcolor));
foodpack = new Nut_Pack(FL_NORMAL_SIZE/3,2*h/245+20*h/77,w-2*FL_NORMAL_SIZE/3,18*h/23);
foodpack->end();
ancient_scroll->end();
recipelist->end();

fc  = new FoodChoice(x, y, w, h, widgetcolor, 1);
fc->cancel_button->on();

setservingunit = new Fl_Group(x, y, w, h);
setservingunit->color(widgetcolor);
 {
 Nut_Box *o = new Nut_Box(x,2*h/18,w,h/20,"Please enter recipe name (max. 60 characters)");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
recipe_name_box = new Nut_Input(w/4, 3*h/18, w/2, h/22);
recipe_name_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Box *o = new Nut_Box(x,4*h/18,w,h/20,"Please enter serving unit (cup, tbsp, piece...)");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
new_serving_unit_box = new Nut_Input(w/4, 5*h/18, w/2, h/22);
new_serving_unit_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Box *o = new Nut_Box(x,6*h/18,w,h/20,"Please enter number of the above serving units in one serving");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
new_serving_qty_box = new Nut_Input(9*w/20, 7*h/18, w/10, h/22);
new_serving_qty_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Box *o = new Nut_Box(x,8*h/18,w,h/20,"Please enter number of servings the whole recipe makes");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
new_serving_count_box = new Nut_Input(9*w/20, 9*h/18, w/10, h/22);
new_serving_count_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Box *o = new Nut_Box(x,10*h/18,w,h/20,"Please enter weight of a single serving (optional)");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
new_serving_weight_box = new Nut_Input(9*w/20, 11*h/18, w/10, h/22);
new_serving_weight_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Button *o = new Nut_Button(x+5*w/6,35*h/56,3*w/24,h/15,"Save");
 o->color(fl_lighter(widgetcolor));
 o->labelfont(FL_BOLD);
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->clear_visible_focus();
 o->callback(recipe_done_cb);
 }
 {
 Nut_Button *o = new Nut_Button(x+5*w/6,y+2*h/3,3*w/24,h/15,"Cancel");
 o->color(fl_lighter(widgetcolor));
 o->labelfont(FL_BOLD);
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->clear_visible_focus();
 o->callback(cancel_recipe_cb,this);
 }
setservingunit->end();
this->add(setservingunit);

this->end();
this->value(recipelist);
for (int i = 0; i < NUTRIENT_COUNT; i++) blank.nutrient[i] = NoData;
}

void AddRecipe::add_to_meal(ViewFoods *vf)
{
intro->label("");
this->value(recipelist);
cancel_this_recipe = 0;
struct food *foodptr = vf->what_food();
float grams = vf->how_much();

if ((new_meal = (struct meal *) malloc(sizeof(struct meal))) == NULL)
 {
 printf("We are out of memory.  Bummer.\n");
 abort();
 }

MealFood *mf = new MealFood(savex, savey, 59*savew/60, saveh/23, foodptr, new_meal, grams, RECIPECOLOR);
foodpack->add(mf);
}

void AddRecipe::direct_add_to_meal(struct food *foodptr, float grams)
{
intro->label("");
this->value(recipelist);
cancel_this_recipe = 0;

if ((new_meal = (struct meal *) malloc(sizeof(struct meal))) == NULL)
 {
 printf("We are out of memory.  Bummer.\n");
 abort();
 }

MealFood *mf = new MealFood(savex, savey, 59*savew/60, saveh/23, foodptr, new_meal, grams, RECIPECOLOR);
foodpack->add(mf);
}

void AddRecipe::back_to_menu(void)
{
this->value(recipelist);
}

void AddRecipe::food_choice_cancel(void)
{                                   
this->value(recipelist);
}

void AddRecipe::delete_meal_food(MealFood *mf)
{
free(mf->mfmealptr);
foodpack->remove(mf);
ancient_scroll->redraw();
Fl::delete_widget(mf);
Fl::flush();
}

void AddRecipe::delete_meal_food_with_replace(MealFood *mf)
{
show_food_with_replace(mf->mffoodptr, mf->mffoodbutton->grams());
free(mf->mfmealptr);
foodpack->remove(mf);
ancient_scroll->redraw();
Fl::delete_widget(mf);
Fl::flush();
}

bool AddRecipe::recipe_done(void)
{
int i;
if (!cancel_this_recipe && strcmp(recipe_name_box->value(),"") == 0) return 0;
int children = foodpack->children();
MealFood *mf;
if (cancel_this_recipe)
 {
 cancel_this_recipe = 0;
 for (i = 0; i < children; i++)
  {
  mf = (MealFood *) foodpack->child(i);
  free(mf->mfmealptr);
  }
 }
else
 {
 if ((new_food = (struct food *) malloc(sizeof(struct food))) == NULL)
  {
  printf("We are out of memory.  Bummer.\n");
  abort();
  }
 *new_food = blank;
 float servingcount = atof(new_serving_count_box->value());
 strncpy(new_food->name,recipe_name_box->value(),60);
 new_food->name[60] = '\0';
 for (i = 0; i < 60; i++) new_food->name[i] = toupper(new_food->name[i]);
 new_food->qty = atof(new_serving_qty_box->value());
 if (new_food->qty == 0) new_food->qty = 1;
 strncpy(new_food->unit,new_serving_unit_box->value(),80);
 new_food->unit[80] = '\0';
 if (strcmp(new_food->unit,"") == 0)
  {
  strcpy(new_food->unit,"recipe");
  new_food->qty = 1;
  if (servingcount <= 0) servingcount = 1;
  new_food->qty = 1 / servingcount;
  }
 for (i = 0; i < children; i++)
  {
  mf = (MealFood *) foodpack->child(i);
  free(mf->mfmealptr);
  for (int j = 0; j < NUTRIENT_COUNT; j++) new_food->nutrient[j] += mf->mffoodbutton->grams() / 100 * mf->mffoodptr->nutrient[j] / servingcount;
  new_food->grams += mf->mffoodbutton->grams() / servingcount;
  }
 float servingweight = atof(new_serving_weight_box->value());
 if (servingweight != 0)
  {
  if (!options.grams) servingweight *= GRAMS_IN_OUNCE;
  float weightdiff = new_food->grams - servingweight;
  new_food->grams -= weightdiff;
  new_food->nutrient[WATER] -= weightdiff;
  }
 for (int j = 0; j < NUTRIENT_COUNT; j++) new_food->nutrient[j] /= new_food->grams / 100;
 new_food->ndb_no = options.next_recipe++;
 write_OPTIONS();
 int foodnum = modify_food_index(order_new_food(),new_food);
 reindex_meals(foodnum);
 reindex_foodbuttonarrays(foodnum);
 write_meal_db();                                            
 write_theusual_db();
 write_food_db();    
 write_recipe(new_food);
 show_new_food(new_food, new_food->grams);
 }
foodpack->clear();
recipe_name_box->value("");
new_serving_unit_box->value("");
new_serving_qty_box->value("");
new_serving_count_box->value("");
new_serving_weight_box->value("");
intro->label("Find the recipe ingredients in the database and show their quantities in the same manner as recording a meal.");
value(recipelist);
return 1;
}

void AddRecipe::gram_ounce_change(void)
{
this->redraw();
}

void AddRecipe::cancel_recipe(void)
{                                      
cancel_this_recipe = 1;
recipe_done_cb(this);
}

void AddRecipe::save_recipe(void)
{                                      
if (foodpack->children() == 0)
 {
 cancel_recipe();
 value(recipelist);
 }
else value(setservingunit);
}

void AddRecipe::reindex_foodbuttons(int foodnum)
{
fc->reindex_foodbuttons(foodnum);
}

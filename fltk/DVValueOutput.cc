/* Class DVValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "DVValueOutput.h"
#include "Nut.h"
#include "db.h"
#include <string.h>
#include <FL/Fl.H>

struct food *fooodwork;

static void cal_cb(Fl_Widget *who)
{
DVValueOutput *mr_cal = (DVValueOutput *) who;
int i;
if (options.abnuts[ENERC_KCAL] != -1) options.abnuts[ENERC_KCAL] = mr_cal->value();
else mr_cal->value(-1);
if (mr_cal->value() == 0) mr_cal->value(2000);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void fat_cb(Fl_Widget *who)
{
DVValueOutput *mr_fat = (DVValueOutput *) who;
int i;
if (options.abnuts[FAT] > 0) options.abnuts[FAT] = mr_fat->value();
else mr_fat->value(-1);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void pro_cb(Fl_Widget *who)
{
DVValueOutput *mr_pro = (DVValueOutput *) who;
int i;
if (options.abnuts[PROCNT] > 0) options.abnuts[PROCNT] = mr_pro->value();
else mr_pro->value(-1);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void nfc_cb(Fl_Widget *who)
{
DVValueOutput *mr_nfc = (DVValueOutput *) who;
int i;
if (options.abnuts[CHO_NONFIB] > 0) options.abnuts[CHO_NONFIB] = mr_nfc->value();
else mr_nfc->value(-1);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void fiber_cb(Fl_Widget *who)
{
DVValueOutput *mr_fiber = (DVValueOutput *) who;
int i;
if (options.abnuts[FIBTG] > 0) options.abnuts[FIBTG] = mr_fiber->value();
else mr_fiber->value(-1);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void satfat_cb(Fl_Widget *who)
{
DVValueOutput *mr_satfat = (DVValueOutput *) who;
int i;
if (options.abnuts[FASAT] > 0) options.abnuts[FASAT] = mr_satfat->value();
else mr_satfat->value(-1);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void fapu_cb(Fl_Widget *who)
{
DVValueOutput *mr_fapu = (DVValueOutput *) who;
int i;
if (options.abnuts[FAPU] > 0) options.abnuts[FAPU] = mr_fapu->value();
else mr_fapu->value(-1);
i = options.screen;
options.screen = 0;
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = i;
dv_change_minus_po();
write_OPTIONS();
}

static void fish_cb(Fl_Widget *who)
{
DVValueOutput *mr_fish = (DVValueOutput *) who;
options.n6hufa = mr_fish->value();
 {
 options.abnuts[FAMS] = 0;
 }
}

DVValueOutput::DVValueOutput (int x, int y, int w, int h, Fl_Color widgetcolor, int nutnum) : Nut_ValueOutput (x, y, w, h)
{
mynut = nutnum;
this->tooltip("Click and drag left and right to adjust,\nmiddle button 10x, right button 100x");
this->textcolor(FL_YELLOW);
this->textfont(FL_BOLD);
this->color(widgetcolor);
if (Fl::scheme() && !strcmp(Fl::scheme(), "plastic")) this->color(fl_darker(fl_darker(fl_darker(fl_darker(fl_darker(fl_darker(widgetcolor)))))));
this->step(0.1);
this->bounds(0, 9999);
switch (mynut)
 {
 case ENERC_KCAL:
  this->bounds(0, 9999);
  this->callback(cal_cb);
  break;
 case FAT:
  this->bounds(0, 9999);
  this->callback(fat_cb);
  break;
 case FIBTG:
  this->bounds(0, 9999);
  this->callback(fiber_cb);
  break;  
 case CHO_NONFIB:
  this->bounds(0, 9999);
  this->callback(nfc_cb);
  break;
 case PROCNT:
  this->bounds(0, 9999);
  this->callback(pro_cb);
  break;
 case FASAT:
  this->bounds(0, 9999);
  this->callback(satfat_cb);
  break;
 case -38:
  this->bounds(15, 90);
  this->step(1);
  this->callback(fish_cb);
  break;
 case FAPU:
  this->bounds(0, 9999);;
  this->callback(fapu_cb);
  break;
 }
}

int DVValueOutput::format(char *buffer)
{
switch (mynut)
 {
 case ENERC_KCAL:
  if (value() > 0 && options.abnuts[ENERC_KCAL] > 0) return snprintf(buffer, 128, "%0.1f kc", value());
  else snprintf(buffer, 128, "%0.1f kc", DV[ENERC_KCAL]);
  break;
 case FAT:
  if (value() > 0 && options.abnuts[FAT] > 0) return snprintf(buffer, 128, "%0.1f g", value());
  else snprintf(buffer, 128, "%0.1f g", DV[FAT]);
  break;
 case PROCNT:
  if (value() > 0 && options.abnuts[PROCNT] > 0) return snprintf(buffer, 128, "%0.1f g", value());
  else snprintf(buffer, 128, "%0.1f g", DV[PROCNT]);
  break;
 case FIBTG:
  if (value() > 0 && options.abnuts[FIBTG] > 0) return snprintf(buffer, 128, "%0.1f g", value());
  else snprintf(buffer, 128, "%0.1f g", DV[FIBTG]);
  break;
 case CHO_NONFIB:
  if (value() > 0 && options.abnuts[CHO_NONFIB] > 0) return snprintf(buffer, 128, "%0.1f g", value());
  else snprintf(buffer, 128, "%0.1f g", DV[CHO_NONFIB]);
  break;
 case FASAT:
  if (value() > 0 && options.abnuts[FASAT] > 0) return snprintf(buffer, 128, "%0.1f g", value());
  else snprintf(buffer, 128, "%0.1f g", DV[FASAT]);
  break;
 case -38:
  return snprintf(buffer, 128, "%0.0f / %0.0f", value(), 100 - value());
  break;
 case FAPU:
  if (value() > 0 && options.abnuts[FAPU] > 0) return snprintf(buffer, 128, "%0.1f g", value());
  else snprintf(buffer, 128, "%0.1f g", DV[FAPU]);
  break;
 }
return 0;
}

void DVValueOutput::update(void)
{
switch (mynut)
 {
 case ENERC_KCAL:
 case FAT:
 case FIBTG:
 case PROCNT:
 case CHO_NONFIB:
 case FASAT:
 case FAPU:
  value(DV[mynut]);
  break;
 case -38:
  value(options.n6hufa == 0 ? DefaultTarget : options.n6hufa);
  break;
 }
}

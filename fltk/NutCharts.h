/* NutCharts.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NUTCHARTS_H
#define NUTCHARTS_H

#include <FL/Fl_Chart.H>
#include "Nut_LineChart.h"
#include "Nut.h"

class NutCharts : public Fl_Group
{
public:
NutCharts(int x, int y, int w, int h, Fl_Color widgetcolor);
void populate(int nutnum, int screen);
float protein[1001];
float carb[1001];
float fat[1001];
float nut[1001];
char mealdate[1001][9];

protected:
void resize(int x, int y, int w, int h);

private:
Nut_LineChart *n;
Fl_Chart *f, *c, *p;
char labelbuf[60];
};

#endif

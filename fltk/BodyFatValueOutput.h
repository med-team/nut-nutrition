/* BodyFatValueOutput.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BODYFATVALUEOUTPUT_H
#define BODYFATVALUEOUTPUT_H

#include "Nut_ValueOutput.h"

class BodyFatValueOutput : public Nut_ValueOutput
{
public:
BodyFatValueOutput(int x, int y, int w, int h, Fl_Color widgetcolor);

private:
int format(char *buffer);
};

#endif

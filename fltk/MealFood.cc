/* Class MealFood */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"

MealFood::MealFood (int x, int y, int w, int h, struct food *foodptr, struct meal *mealptr, float grams, Fl_Color widgetcolor) : Fl_Group (x, y, w, h)
{
if (widgetcolor == RECIPECOLOR) this_is_a_recipe = 1;
else this_is_a_recipe = 0;
mffoodptr = foodptr;
mfmealptr = mealptr;
int spacing = w/225;
mffoodbutton = new FoodButton(x, y, 15*w/24, h, foodptr);
mffoodbutton->grams(grams);
mffoodbutton->callback(delete_meal_food_with_replace_cb,this);
mfvo = new MealFoodValueOutput(x+15*w/24+spacing, y, w/15, h, widgetcolor, this);
mfvo->value(grams);
if (!this_is_a_recipe) pcf = new PCF(x+15*w/24+w/15+2*spacing, y, 9*w/38, h, widgetcolor, this);
Nut_Button *db = new Nut_Button(x+15*w/24+w/15+9*w/38+3*spacing, y, w/20, h, "DEL");
db->color(fl_lighter(widgetcolor));
db->box(FL_ROUND_UP_BOX);
if (!this_is_a_recipe) db->color(fl_lighter(MEALCOLOR));
db->callback(delete_meal_food_cb, this);
this->end();
}

void MealFood::gram_change(double nv)
{
mffoodbutton->grams(nv);
mfmealptr->grams = nv;
if (!this_is_a_recipe) pcf->gram_change(PCF_index, nv);
}

void MealFood::non_recursive_gram_change(void)
{
mffoodbutton->grams(mfmealptr->grams);
mfvo->value(mfmealptr->grams);
this->redraw();
}

void MealFood::fill_PCF_matrix(int pcfindex, float *PCF_matrix, int *PCF_lock, MealFood **PCF_lock_holder)
{
PCF_index = pcfindex;
*PCF_lock_holder = this;
*PCF_matrix = mffoodbutton->grams();
if (mffoodptr->nutrient[PROCNT] != 0) *(PCF_matrix+1) = mffoodptr->nutrient[PROCNT] / 100;
else *(PCF_matrix+1) = 0.000001;
if (mffoodptr->nutrient[CHO_NONFIB] != 0) *(PCF_matrix+2) = mffoodptr->nutrient[CHO_NONFIB] / 100;
else *(PCF_matrix+2) = 0.000001;
if (mffoodptr->nutrient[FAT] != 0) *(PCF_matrix+3) = mffoodptr->nutrient[FAT] / 100;
else *(PCF_matrix+3) = 0.000001;
if (pcf->p->value() == 1) *PCF_lock = 1;
else if (pcf->c->value() == 1) *PCF_lock = 2;
else if (pcf->f->value() == 1) *PCF_lock = 3;
else *PCF_lock = 0;
}

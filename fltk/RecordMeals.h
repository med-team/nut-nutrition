/* RecordMeals.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RECORDMEALS_H
#define RECORDMEALS_H

#include "ViewFoods.h"
#include "NutButtonWidget.h"
#include "MealValueOutput.h"
#include "MealFood.h"
#include "MissMealWidget.h"
#include "TheUsualMenuButton.h"
#include "TheUsualNameWidget.h"

class FoodChoice;
class ViewFoods;
class MealFood;

class RecordMeals : public Fl_Wizard
{
public:
RecordMeals(int x, int y, int w, int h, Fl_Color widgetcolor);
void tab_change(void);
void add_to_meal(ViewFoods *vf);
void direct_add_to_meal(struct food *foodptr, float grams);
void analyze(bool dv_change_only);
void back_to_menu(void);
void food_choice_cancel(void);
void delete_meal_food(MealFood *mf);
void delete_meal_food_with_replace(MealFood *mf);
void update(bool dv_change_only);
void meal_change(void);
void add_theusual(struct meal *theusualptr);
void new_customary_meal_phase_one(void);
void new_customary_meal_phase_two(const char *);
void update_pcf(void);
struct meal *tell_me_meal_root(void);
void reindex_foodbuttons(int foodnum);
struct food zero;
FoodChoice *fc;
MealValueOutput *mvo;
NutButtonWidget *potentialmeal;

protected:
void resize(int x, int y, int w, int h);

private:
Fl_Group *actualmeal;
Nut_Pack *foodpack;
Nut_Scroll *ancient_scroll;
AnalyzeMeals *analysis;
struct meal *current_temp_meal_root;
MissMealWidget *mmw;
TheUsualNameWidget *tunw;
TheUsualMenuButton *tumb;
char theusual_id[9];
};

#endif

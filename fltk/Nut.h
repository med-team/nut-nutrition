/* Nut.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NUT_H
#define NUT_H

#define MEALCOLOR fl_rgb_color(255,127,0)
#define RECIPECOLOR fl_rgb_color(127,191,0)

#include "AnalyzeMeals.h"


bool get_recipe_mode(void);
Fl_Color scheme_color(Fl_Color whatever);
void update_pcf(void);
void gram_ounce_change(void);
void new_recipe_cb(Fl_Widget *whatever);
void recipe_done_cb(Fl_Widget *whatever);
void new_story(int nut, int screen);
void show_food(struct food *fptr, float grams);
void show_new_food(struct food *fptr, float grams);
void skip_show_food(struct food *fptr, float grams);
void show_food_with_replace(struct food *fptr, float grams);
void back_cb(Fl_Widget *whatever);
void dv_change(void);
void dv_change_minus_po(void);
void defanal_change(RecordMeals *rm);
void defanal_change(AnalyzeMeals *am);
void screen_change(void);
void redraw_whole_schmeer(void);
void reindex_foodbuttonarrays(int foodnum);
void add_to_meal_cb(Fl_Widget *whatever);
void food_choice_cancel_cb(Fl_Widget *whatever);
void delete_meal_food_cb(Fl_Widget *whatever, void *whichone);
void delete_meal_food_with_replace_cb(Fl_Widget *whatever, void *whichone);
void request_mvo_initial_set(void);
void update_analysis(void);

class Nut : public Fl_Tabs
{
public:
Nut(int x, int y, int w, int h);

protected:
void resize(int x, int y, int w, int h);
};

#endif

/* Class NewRecipeWidget */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"

NewRecipeWidget::NewRecipeWidget (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
this->box(FL_NO_BOX);
n = new Nut_Button(x, y, w, h, "New Recipe");
n->color(fl_lighter(RECIPECOLOR));
n->clear_visible_focus();
n->callback(new_recipe_cb);
f = new Fl_Box(x, y, w, h);
this->end();
}

void NewRecipeWidget::on(void)
{
this->value(n);
}

void NewRecipeWidget::off(void)
{
this->value(f);
}

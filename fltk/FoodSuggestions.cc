/* Class FoodSuggestions */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "FoodButton.h"
#include "anameal.h"
#include "Nut_ReturnButton.h"
#include <FL/Fl.H>

static void all_done_cb(Fl_Widget *whatever, void *amptr)
{
AnalyzeMeals *am = (AnalyzeMeals *) amptr;
am->back_to_nbw();
}

void food_recommend_cb(Fl_Widget *which, void *who)
{
FoodSuggestions *us_right_now = (FoodSuggestions *)who;
us_right_now->value(us_right_now->here_are_foods);
}

FoodSuggestions::FoodSuggestions (int x, int y, int w, int h, Fl_Color widgetcolor, AnalyzeMeals *amparent, struct food *fw, const char *label) : Fl_Wizard (x, y, w, h, label)
{
am = amparent;
foodwork = fw;
you_have_achieved = new Fl_Group(x,y,w,h);
you_have_achieved->color(widgetcolor);
 {
 new Nut_Box(x,y,w,2*h/3,"You have achieved the \"Daily Value\" for all nutrients.");
 }
 {
 Nut_ReturnButton *o = new Nut_ReturnButton(x+3*w/4,y+2*h/3,w/8,h/15,"OK");
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->color(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->clear_visible_focus();
 o->callback(all_done_cb,am);
 }
you_have_achieved->end();

here_are_nuts = new Fl_Group(x,y,w,h);
new Nut_Box(x,y+h/23,w,h/23,"Here are nutrients with additional daily percentages in the \"Daily Values\":");
ancient_scroll1 = new Nut_Scroll(x+w/3,y+3*h/23,w/3,20*h/23);
ancient_scroll1->type(Fl_Scroll::VERTICAL);
ancient_scroll1->color(scheme_color(widgetcolor));
nutpack = new Nut_Pack(x+w/3,y+3*h/23,8*w/27,20*h/23);
for (int i = 1; i <= DV_COUNT; i++) DeficNuts[DVMap[i]] = new DeficNut(x,y,8*w/27,h/23, DVMap[i], 0, widgetcolor, Nutrient[DVMap[i]]);
nutpack->end();
ancient_scroll1->end();
Nut_ReturnButton *rb = new Nut_ReturnButton(x+4*w/5,y+2*h/3,3*w/24,h/15,"continue");
rb->clear_visible_focus();
rb->callback(food_recommend_cb,this);
rb->box(FL_ROUND_UP_BOX);
rb->color(FL_GREEN);
here_are_nuts->end();
here_are_nuts->color(widgetcolor);

here_are_foods = new Fl_Group(x,y,w,h);
new Nut_Box(x,y+h/23,2*w/3,h/23,"Here are randomly selected foods that provide the additional nutrients noted:");
ancient_scroll2 = new Nut_Scroll(x,y+3*h/23,2*w/3,20*h/23);
ancient_scroll2->type(Fl_Scroll::VERTICAL);
ancient_scroll2->color(scheme_color(widgetcolor));
foodpack = new Nut_Pack(x,y+3*h/23,17*w/27,20*h/23);
foodpack->end();
ancient_scroll2->end();
rb = new Nut_ReturnButton(x+4*w/5,y+2*h/3,3*w/24,h/15,"Return");
rb->clear_visible_focus();
rb->callback(all_done_cb,am);
rb->box(FL_ROUND_UP_BOX);
rb->color(fl_lighter(widgetcolor));
here_are_foods->end();
here_are_foods->color(widgetcolor);
frcount = -1;
this->end();
this->value(here_are_nuts);
}

void FoodSuggestions::update(int max)
{
struct food *food_ptr, *food_ptr_abacus[MAX_FOOD];
struct food *food_recommended[MAX_FOOD];
float serving_ratio_abacus[MAX_FOOD], thispct, serving_ratio;
float totaldefic = 0, deficpct[DV_COUNT], prodefic;
float food_abacus[MAX_FOOD];
int deficnuts = -1, count, deficnut[DV_COUNT];
int abacuscount, high_grams;
FoodButton *fb;

if (max == 0) max = 1;

for (count = 0; count < DVMap[0]; count++) 
 {
 if (DVMap[count+1] == ENERC_KCAL || DVMap[count+1] == FAT || 
 DVMap[count+1] == NA || DVMap[count+1] == FASAT || 
 DVMap[count+1] == FAMS || DVMap[count+1] == FAPU || 
 DVMap[count+1] == CHOLE || DVMap[count+1] == OMEGA6 ||
 DVMap[count+1] == CHO_NONFIB || DVMap[count+1] == OMEGA3 ||
 DVMap[count+1] == CHOCDF ||
  DVMap[count+1] == AA || DVMap[count+1] == LA ||
 (DVMap[count+1] == EPA && DV[AA] - 0.01 < .00001 && DV[EPA] - 0.01 < .00001 && DV[DHA] - 0.01 < .00001 ) ||
 (DVMap[count+1] == DHA && DV[AA] - 0.01 < .00001 && DV[EPA] - 0.01 < .00001 && DV[DHA] - 0.01 < .00001 ) ||
 foodwork->nutrient[DVMap[count+1]] / DV[DVMap[count+1]] >= .9999) deficpct[count] = 0;
 else 
  {
  deficnuts++;
  deficnut[deficnuts] = count;
  deficpct[deficnuts] = (((float) max) / (float) options.mealsperday) * 100 * (1 - foodwork->nutrient[DVMap[count+1]]/DV[DVMap[count+1]]);
  totaldefic += deficpct[deficnuts];
  }
 }
if (deficnuts > 0) deficsort(deficpct, deficnut, deficnuts);
if (deficnuts == -1 || deficpct[0] == 0)
 {
 this->value(you_have_achieved);
 return;
 }
for (int i = 1; i <= DV_COUNT; i++) nutpack->remove(DeficNuts[DVMap[i]]);
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll1->position(0,0);
ancient_scroll2->position(0,0);
#else
ancient_scroll1->scroll_to(0,0);
ancient_scroll2->scroll_to(0,0);
#endif
this->value(here_are_nuts);
for (count = 0; count <= deficnuts; count++) 
 {
 DeficNuts[DVMap[deficnut[count]+1]]->set_value(deficpct[count]);
 nutpack->add(DeficNuts[DVMap[deficnut[count]+1]]);
 }
foodpack->clear();
frcount = -1;
while (totaldefic > 0)
 {
 food_ptr = &food_root;
 abacuscount = -1;
 while (food_ptr->next != NULL)
  {
  food_ptr = food_ptr->next;
  food_ptr_abacus[++abacuscount] = food_ptr;
  prodefic = 0;
  serving_ratio_abacus[abacuscount] = 10;
  high_grams = 114;
  for (count = 0; count <= deficnuts; count++)
   {
   thispct = 100 * food_ptr->nutrient[DVMap[deficnut[count]+1]] / DV[DVMap[deficnut[count]+1]]; 
   serving_ratio = deficpct[count] / thispct;
   if (serving_ratio < serving_ratio_abacus[abacuscount] && serving_ratio * food_ptr->grams > 14) serving_ratio_abacus[abacuscount] = serving_ratio;
   }
  if (food_ptr->fdgrp == 3 || food_ptr->fdgrp == 8 || food_ptr->fdgrp == 16 || food_ptr->fdgrp == 18 || food_ptr->fdgrp == 19 || food_ptr->fdgrp == 20 || food_ptr->fdgrp == 25 || food_ptr->fdgrp == 35) serving_ratio_abacus[abacuscount] = 0;
  if (serving_ratio_abacus[abacuscount] > 1 && high_grams == 114) serving_ratio_abacus[abacuscount] = 1;
  if ((serving_ratio_abacus[abacuscount] * food_ptr->grams) > high_grams) serving_ratio_abacus[abacuscount] = high_grams / food_ptr->grams;
  for (count = 0; count <= deficnuts; count++)
   {
   thispct = serving_ratio_abacus[abacuscount] * food_ptr->grams * food_ptr->nutrient[DVMap[deficnut[count]+1]] / DV[DVMap[deficnut[count]+1]]; 
   if (thispct > deficpct[count]) thispct = deficpct[count];
   prodefic += thispct;
   }
  if (serving_ratio_abacus[abacuscount] == 0) prodefic = 0;
  food_abacus[abacuscount] = prodefic;
  for (count = 0 ; count <= frcount; count++) if (food_ptr_abacus[abacuscount] == food_recommended[count]) food_abacus[abacuscount] = -1;
  }
 abacuscount = random_max_var_array(food_abacus, abacuscount);
 food_ptr = food_ptr_abacus[abacuscount];            
 food_recommended[++frcount] = food_ptr;
 fb = new FoodButton(x(), y()+3*h()/23, 17*w()/27, h()/23, food_ptr);
 fb->grams(serving_ratio_abacus[abacuscount] * food_ptr->grams);
 foodpack->add(fb);
 totaldefic = 0;
 for (count = 0; count <= deficnuts; count++) 
  {
  thispct = serving_ratio_abacus[abacuscount] * food_ptr->grams * food_ptr->nutrient[DVMap[deficnut[count]+1]] / DV[DVMap[deficnut[count]+1]]; 
  if (thispct > deficpct[count]) thispct = deficpct[count];
  deficpct[count] -= thispct;
  totaldefic += deficpct[count];
  }
 }
}

void FoodSuggestions::tab_change(void)
{
if (you_have_achieved->visible()) all_done_cb(this, am);
}

void FoodSuggestions::resize(int x, int y, int w, int h)
{
int i, children;
Fl_Wizard::resize(x, y, w, h);
for (i = 1; i <= DV_COUNT; i++) DeficNuts[DVMap[i]]->resize(x,y,8*w/27,h/23);
children = foodpack->children();
for (i = 1; i <= children; i++) foodpack->child(i-1)->resize(x,y,17*w/27,h/23);
}

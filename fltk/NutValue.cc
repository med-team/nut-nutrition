/* Class NutValue */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "util.h"

NutValue::NutValue (int x, int y, int w, int h, Fl_Color widgetcolor, NutButtonWidget *nbw, struct food *fw, int nutnum, int screennum) : Fl_Wizard (x, y, w, h)
{
foodwork = fw;
Nut_Box *boxptr;
nutindex = nutnum;
screen = screennum;
data = &(foodwork->nutrient[nutindex]);
Fl_Group::current(0);

display = new Fl_Group(x, y, w, h);
Fl_Group::current(0);
display->color(widgetcolor);
valbox = new Nut_Box(x, y, 2*w/3, h);
valbox->align(FL_ALIGN_INSIDE|FL_ALIGN_RIGHT);
valbox->labelfont(FL_BOLD);
display->add(valbox);
boxptr = new Nut_Box(x+2*w/3, y, w/3, h);
if (screen == 0 && nutindex != CHO_NONFIB) boxptr->label("%");
else boxptr->label(Unit[nutindex]);
boxptr->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
display->add(boxptr);

overflow = new Fl_Group(x, y, w, h);
Fl_Group::current(0);
overflow->color(widgetcolor);
boxptr = new Nut_Box(x, y, 2*w/3, h);
if (screen == 0) boxptr->label(">999999");
else if (screen < 4) boxptr->label(">999999.9");
else boxptr->label(">999999.99");
boxptr->align(FL_ALIGN_INSIDE|FL_ALIGN_RIGHT);
boxptr->labelfont(FL_BOLD);
overflow->add(boxptr);
boxptr = new Nut_Box(x+2*w/3, y, w/3, h);
if (screen == 0 && nutindex != CHO_NONFIB) boxptr->label("%");
else boxptr->label(Unit[nutindex]);
boxptr->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
overflow->add(boxptr);

underflow = new Fl_Group(x, y, w, h);
Fl_Group::current(0);
underflow->color(widgetcolor);
boxptr = new Nut_Box(x, y, 2*w/3, h);
if (screen == 0) boxptr->label("<-999999");
else if (screen < 4) boxptr->label("<-999999.9");
else boxptr->label("<-999999.99");
boxptr->align(FL_ALIGN_INSIDE|FL_ALIGN_RIGHT);
boxptr->labelfont(FL_BOLD);
underflow->add(boxptr);
boxptr = new Nut_Box(x+2*w/3, y, w/3, h);
if (screen == 0 && nutindex != CHO_NONFIB) boxptr->label("%");
else boxptr->label(Unit[nutindex]);
boxptr->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
underflow->add(boxptr);

nodata = new Nut_Box(x, y, 2*w/3, h, "(nd)");
nodata->tooltip("No Data");
nodata->color(widgetcolor);
nodata->align(FL_ALIGN_INSIDE|FL_ALIGN_RIGHT);

questionmark = new Nut_Box(x, y, w, h, "?");
questionmark->color(widgetcolor);
questionmark->labelfont(FL_BOLD);
questionmark->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);

this->add(display);
this->add(overflow);
this->add(underflow);
this->add(nodata);
this->add(questionmark);
this->box(FL_NO_BOX);

nbw->add_me_to_the_update_list(this);
special = 0;
}

NutValue::NutValue (int x, int y, int w, int h, Fl_Color widgetcolor, NutButtonWidget *nbw, struct food *fw, int nutnum, int screennum, int cpf) : Fl_Wizard (x, y, w, h)
{
foodwork = fw;
nutindex = nutnum;
screen = screennum;
data = &(foodwork->nutrient[nutindex]);
valbox = new Nut_Box(x, y, w, h);
valbox->color(widgetcolor);
valbox->labelfont(FL_BOLD);
valbox->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);
questionmark = new Nut_Box(x, y, w, h, "?");
questionmark->color(widgetcolor);
questionmark->labelfont(FL_BOLD);
questionmark->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);
this->end();
this->box(FL_NO_BOX);

nbw->add_me_to_the_update_list(this);
special = 1;
}

NutValue::NutValue (int x, int y, int w, int h, Fl_Color widgetcolor, NutButtonWidget *nbw, struct food *fw, int nutnum, int screennum, int cpf, int omega) : Fl_Wizard (x, y, w, h)
{
foodwork = fw;
nutindex = nutnum;
screen = screennum;
data = &(foodwork->nutrient[nutindex]);
valbox = new Nut_Box(x, y, w, h);
valbox->color(widgetcolor);
valbox->labelfont(FL_BOLD);
valbox->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);
questionmark = new Nut_Box(x, y, w, h, "?");
questionmark->color(widgetcolor);
questionmark->labelfont(FL_BOLD);
questionmark->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);
this->end();
this->box(FL_NO_BOX);

nbw->add_me_to_the_update_list(this);
special = 2;
}

void NutValue::update (void)
{
float printvalue;
switch (special)
 {
 case 0 : 
         if (!(*data < -383837.9 && *data > -383838.1))
          {
          if (screen == 0 && nutindex != CHO_NONFIB) printvalue = 100*(*data)/DV[nutindex];
          else printvalue = *data;
          if (printvalue > 999999) this->value(overflow);
          else if (printvalue < -999999) this->value(underflow);
          else if (test_for_negative_zero(data)) this->value(nodata);
          else 
           {
           if (screen > 3) snprintf(valbuf,20,"%1.2f",*data);
           else if (screen > 0) snprintf(valbuf,20,"%1.1f",*data);
           else if (nutindex == CHO_NONFIB) snprintf(valbuf,20,"%1.0f",*data);
           else snprintf(valbuf,20,"%1.0f",printvalue);
           valbox->label(valbuf);
           this->value(display);
           }
          }
         else this->value(questionmark);
          break;
 case 1 : 
         if (!(*data < -383837.9 && *data > -383838.1))
          { 
          if (foodwork->nutrient[ENERC_KCAL] > 0.05)
           {
           pctcarb = 100 * foodwork->nutrient[CHO_KCAL] / foodwork->nutrient[ENERC_KCAL];
           pctprot = 100 * foodwork->nutrient[PROT_KCAL] / foodwork->nutrient[ENERC_KCAL];
          
           pctfat = 100 * foodwork->nutrient[FAT_KCAL] / foodwork->nutrient[ENERC_KCAL];
           if (test_for_negative_zero(&pctcarb)) pctcarb = 0;
           if (test_for_negative_zero(&pctprot)) pctprot = 0;
           if (test_for_negative_zero(&pctfat)) pctfat = 0;
           }
          else
           {
           pctcarb = 0; pctprot = 0; pctfat = 0;
           }
          sprintf(valbuf,"%1.0f / %1.0f / %1.0f",pctprot,pctcarb,pctfat);
          valbox->label(valbuf);
          }
         else this->value(questionmark);
          break;
 case 2 : 
         if (!(*data < -383837.9 && *data > -383838.1))
          {
          p3 = 900 * foodwork->nutrient[SHORT3] / foodwork->nutrient[ENERC_KCAL];
          p6 = 900 * foodwork->nutrient[SHORT6] / foodwork->nutrient[ENERC_KCAL];
          h3 = 900 * foodwork->nutrient[LONG3] / foodwork->nutrient[ENERC_KCAL]; 
          h6 = 900 * foodwork->nutrient[LONG6] / foodwork->nutrient[ENERC_KCAL];
          o  = 900 * (foodwork->nutrient[FASAT] + foodwork->nutrient[FAMS] + foodwork->nutrient[FAPU] - foodwork->nutrient[SHORT6] - foodwork->nutrient[LONG6] - foodwork->nutrient[SHORT3] - foodwork->nutrient[LONG3]) / foodwork->nutrient[ENERC_KCAL];
          n6 = (int) n6hufa(p3,p6,h3,h6,o,foodwork->nutrient[ENERC_KCAL]);
          sprintf(valbuf,"%2d / %1d",n6 == 0 ? 0 : n6,n6 == 0 ? 0 : 100-n6);
          valbox->label(valbuf);
          }
         else this->value(questionmark);
          break;
 }
}

/* ServingMenuButton.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SERVINGMENUBUTTON_H
#define SERVINGMENUBUTTON_H

#include <FL/Fl_Menu_Button.H>

class ViewFoods;

void fill_serving_menu(struct food *foodptr);

class ServingMenuButton : public Fl_Menu_Button
{
public:
ServingMenuButton(int x, int y, int w, int h, ViewFoods *vf, Fl_Color widgetcolor);
void set_label(char *label);
void set_label(void);
void blank_out(void);

protected:
void resize(int x, int y, int w, int h);
};

#endif

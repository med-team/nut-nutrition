/* Class FoodButton */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "FoodButton.h"
#include <FL/Fl.H>

static void click_me_fb_cb(Fl_Widget *w, void *data)
{
FoodButton *who_we_are = (FoodButton *) w;
struct food *foodptr = (struct food *) data;
show_food(foodptr, who_we_are->grams());
}

FoodButton::FoodButton (int x, int y, int w, int h, struct food *foodptr) : Nut_Button (x, y, w, h)
{
if (!Fl::scheme()) this->box(FL_RSHADOW_BOX);
this->color(FL_GREEN);
char *bufoodptr = foodname;
this->mygrams = foodptr->grams;
for (int i = 0; i < 80; i++)
 {
 *bufoodptr++ = foodptr->name[i];
 if (foodptr->name[i] == '&') *bufoodptr++ = '&';
 if (foodptr->name[i] == '\0') break;
 }
this->label(foodname);
this->labelfont(FL_BOLD);
this->clear_visible_focus();
this->callback(click_me_fb_cb, foodptr);
}

float FoodButton::grams(void)
{
return mygrams;
}

void FoodButton::grams(float newval)
{
mygrams = newval;
}

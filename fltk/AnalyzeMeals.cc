/* Class AnalyzeMeals */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "PrintMenus.h"
#include "anameal.h"
#include <FL/Fl.H>
#include <string.h>

void food_suggestions_cb(Fl_Widget *which, void *who)
{
AnalyzeMeals *us_right_now = (AnalyzeMeals *)who;
us_right_now->food_suggestions();
}

AnalyzeMeals::AnalyzeMeals (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
int i;
for (i = 0; i < NUTRIENT_COUNT; i++) blank.nutrient[i] = NoData;
for (i = 0; i < NUTRIENT_COUNT; i++) zero.nutrient[i] = 0.00000000000001;
specialheader = 0;
mealroot = &meal_root;
meal_ptr_origin = &mealroot;
defanal = &(options.defanalanal);
this->label("Analyze Meals");
this->labeltype(FL_ENGRAVED_LABEL);
nbw = new NutButtonWidget(x, y, w, h, widgetcolor, &foodwork);
this->end();
fs  = new FoodSuggestions(x, y, w, h, widgetcolor, this, &foodwork, "");
this->add(fs);

Fl_Group *wizgroup;
for (i = 0; i < MAX_SCREEN; i++)
 {
 wizgroup = new Fl_Group(x, y, w, 6*h/22);
  {
  if (i == 0)
   {
   Nut_Button *fsb = new Nut_Button(w/30+16*w/21, h/11, w/6+w/96, h/22, "Food Suggestions");
   fsb->clear_visible_focus();
   fsb->color(FL_GREEN);
   fsb->callback(food_suggestions_cb,this);
   }
  here_are[i] = new Nut_Box(x, 4*h/22, 2*w/3, h/44);
  here_are[i]->align(FL_ALIGN_INSIDE|FL_ALIGN_RIGHT);
  
  meal_value_output[i] = new MealCountValueOutput(2*w/3+w/395, 15*h/88, w/18, h/22, this, widgetcolor);
  meal_meals[i] = new Nut_Box(2*w/3+w/395+w/18, 4*h/22, 3*w/12, h/44);
  meal_meals[i]->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  ptm[i] = new PrintMenus(w/12, 21*h/88, 2*w/18, h/22, widgetcolor);
  ptm[i]->off();
  meal_enum[i] = new Nut_Box(2*w/6, 21*h/88, w/3, h/22);
  btm[i] = new BackToMenu(13*w/15, 2*h/245+16*h/77, w/8, h/22, widgetcolor);
  btm[i]->off();
  }
 wizgroup->end();
 nbw->wizard[i][0]->add(wizgroup);
 }
update_am(-1);
}

int AnalyzeMeals::update_am(int newval)
{
//Fl::check();
//if (newval != -1 && newval != (int) meal_value_output[0]->value()) return 0;
int meals, count, meal = 0;
char meal_date[9];
mealcount = meal_count(*meal_ptr_origin);
for (int i = 0; i < MAX_SCREEN; i++) meal_value_output[i]->maximum(mealcount);
if (mealcount > 0) for (int i = 0; i < MAX_SCREEN; i++) meal_value_output[i]->minimum(1);
else if (mealcount == 0) for (int i = 0; i < MAX_SCREEN; i++) meal_value_output[i]->minimum(0);
max = mealcount;
if (*defanal > 0 && *defanal < mealcount) max = *defanal;
if (mealcount > 0) foodwork = blank;
else foodwork = zero;
meal_ptr = *meal_ptr_origin;
strcpy(meal_date,"");
if (max > mealcount) max = mealcount;
meals = 0;
//Fl::check();
//if (newval != -1 && newval != (int) meal_value_output[0]->value()) return 0;
while (meal_ptr->next != NULL && meals <= max)
 {
 meal_ptr = meal_ptr->next;
 if (strcmp(meal_date,meal_ptr->meal_date) != 0 || meal != meal_ptr->meal)
  {
  strcpy(meal_date,meal_ptr->meal_date);
  meal = meal_ptr->meal;
  meals++;
  }
 if (meals > max) break;
 for (count = 0; count < NUTRIENT_COUNT; count++) foodwork.nutrient[count] += options.mealsperday * meal_ptr->grams / 100 * FoodIndex[meal_ptr->food_no]->nutrient[count] / (float) max;
 }
//Fl::check();
//if (newval != -1 && newval != (int) meal_value_output[0]->value()) return 0;
update_var_elements();
count = options.screen;
options.screen = 0;
//Fl::check();
//if (newval != -1 && newval != (int) meal_value_output[0]->value()) return 0;
load_foodwork(*defanal,*meal_ptr_origin);
auto_cal();
efa_dynamics();
Fl::check();
if (newval != -1 && newval != (int) meal_value_output[0]->value()) return 0;
options.screen = count;
DVnotOK = 0;
nbw->update();
if (nbw->visible()) nbw->redraw();
Fl::flush();
return 1;
}

void AnalyzeMeals::update_am_dv_change_only(void)
{
update_var_elements();
nbw->update();
if (nbw->visible()) nbw->redraw();
Fl::flush();
}

void AnalyzeMeals::update_var_elements(void)
{
for (int i = 0; i < MAX_SCREEN; i++)
 {
 meal_value_output[i]->value(max);
 if (i == 0 && !options.custom) here_are[i]->label("Here are \"Daily Value\" average percentages for your previous");
 else if (i == 0 && options.custom) here_are[i]->label("Here are customized DV average percentages for your previous");
 else if (i != 0) here_are[i]->label("Here are average daily nutrient levels for your previous");
 if (max > 1 && max == mealcount) sprintf(mebuf," -> Meals  %s / %d  through  %s / %d\n\n",meal_ptr->meal_date,meal_ptr->meal,(*meal_ptr_origin)->next->meal_date,(*meal_ptr_origin)->next->meal);
 if (max > 1 && max == mealcount) sprintf(mebuf," -> Meals  %s / %d  through  %s / %d\n\n",meal_ptr->meal_date,meal_ptr->meal,(*meal_ptr_origin)->next->meal_date,(*meal_ptr_origin)->next->meal);
else if (max > 1 && max != mealcount) sprintf(mebuf," -> Meals  %s / %d  through  %s / %d\n\n",prev_meal(meal_ptr)->meal_date,prev_meal(meal_ptr)->meal,(*meal_ptr_origin)->next->meal_date,(*meal_ptr_origin)->next->meal);
 else if (max == 1) sprintf(mebuf," -> Meal  %s / %d\n\n",(*meal_ptr_origin)->next->meal_date,(*meal_ptr_origin)->next->meal);
 else if (max == 0) sprintf(mebuf," ");
 meal_enum[i]->label(mebuf);
 if (max != 1) meal_meals[i]->label("meals.");
 else meal_meals[i]->label("meal.");
 }
}

void AnalyzeMeals::restore_defaults_gui(void)
{
if (max == 0 || options.screen != 0) return;
restore_defaults();
dv_change();
}

void AnalyzeMeals::food_suggestions(void)
{
this->next();
fs->update(max);
}

void AnalyzeMeals::meal_value_output_change(int newval)
{
*defanal = newval;
for (int i = 0; i < MAX_SCREEN; i++) meal_value_output[i]->value((double) newval);
if (update_am(newval)) defanal_change(this);
}

void AnalyzeMeals::tab_change(void)
{
update_am(-1);
fs->tab_change();
}

void AnalyzeMeals::back_to_nbw(void)
{
this->value(nbw);
}

void AnalyzeMeals::set_specialheader(RecordMeals *rm, struct meal **rmroot)
{                                   
specialheader = 1;
for (int i = 0; i < MAX_SCREEN; i++)
 {
 btm[i]->on(rm);
 ptm[i]->on(rm);
 }
defanal = &(options.defanalrec);
meal_ptr_origin = rmroot;
}

struct meal *AnalyzeMeals::tell_me_meal_root(void)
{
return *meal_ptr_origin;
}

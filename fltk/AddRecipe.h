/* AddRecipe.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ADDRECIPE_H
#define ADDRECIPE_H

#include "MealFood.h"


class AddRecipe : public Fl_Wizard
{
public:
AddRecipe(int x, int y, int w, int h, Fl_Color widgetcolor);
void tab_change(void);
void add_to_meal(ViewFoods *vf);
void direct_add_to_meal(struct food *foodptr, float grams);
void back_to_menu(void);
void food_choice_cancel(void);
void delete_meal_food(MealFood *mf);
void delete_meal_food_with_replace(MealFood *mf);
bool recipe_done(void);
void gram_ounce_change(void);
void cancel_recipe(void);
void save_recipe(void);
void reindex_foodbuttons(int foodnum);
FoodChoice *fc;
Nut_Input *new_serving_unit_box, *new_serving_qty_box, *recipe_name_box;
Nut_Input *new_serving_weight_box, *new_serving_count_box;

private:
Fl_Group *recipelist, *setservingunit;
Nut_Pack *foodpack;
Nut_Scroll *ancient_scroll;
Nut_Box *intro;
int savex, savey, savew, saveh;
bool cancel_this_recipe;
struct food blank;
};

#endif

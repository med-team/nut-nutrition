/* Class FoodRanking */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <FL/Fl.H>
#include "FoodRanking.h"
#include "util.h"
#ifndef __hpux
#include <stdint.h>
#endif
#include <string.h>

static intptr_t the_ranking;
static int lastnut, lastscreen, food_group = -1, minimize = -1;

static FoodRanking *who_we_are;

static void the_cb(Fl_Widget *which, void *one)
{
the_ranking = (intptr_t) one;
food_group = -1;
minimize = -1;
who_we_are->initial_meal_add = 1;
who_we_are->populate(lastnut,lastscreen);
}

static Fl_Menu_Item pulldown[] = {
  {"per 100 Grams",   0, the_cb, (void*)0},
  {"per 100 Grams Dry Weight",    0, the_cb, (void*)1},
  {"per 100 Grams within Food Group ?",    0, the_cb, (void*)2},
  {"per 100 Calories",  0,   the_cb, (void*)3},
  {"per Serving",    0,   the_cb, (void*)4},
  {"per Serving, Minimize Nutrient ?, \"no data\" instances included",  0, the_cb, (void*)5},
  {"per Serving, Minimize Nutrient ?, \"no data\" instances excluded",  0, the_cb, (void*)6},
  {"per Daily Recorded Meals",    0,   the_cb, (void*)7},
{0}};

FoodRanking::FoodRanking (int x, int y, int w, int h, Fl_Color widgetcolor, TheStory *ts) : Fl_Group (x, y, w, h)
{
int i;
story = ts;
this->color(widgetcolor);
mb = new Fl_Menu_Button(x,y,w,h/23);
mb->color(fl_lighter(widgetcolor));
mb->clear_visible_focus();
mb->align(FL_ALIGN_INSIDE|FL_ALIGN_CENTER);
mb->menu(pulldown);
ancient_scroll = new Nut_Scroll(x,y+h/23,w,22*h/23);
ancient_scroll->type(Fl_Scroll::VERTICAL);
ancient_scroll->color(scheme_color(widgetcolor));
foodpack = new Nut_Pack(x,y+h/23,17*w/18,22*h/23);
foodpack->end();
ancient_scroll->end();
for (i = 0; i < FoodCount; i++)
 {
 FoodButtonArray[i] = new FoodButton(x,y,17*w/27,h/23, FoodIndex[i]);
 foodpack->add(FoodButtonArray[i]);                                  
 foodpack->remove(FoodButtonArray[i]);                                  
 }
this->end();
if (meal_count(options.temp_meal_root) > 0) 
 {
 the_ranking = 7;
 initial_meal_add = 1;
 }
else 
 {
 the_ranking = 0;
 initial_meal_add = 0;
 }
who_we_are = this;
}

void FoodRanking::populate(int num, int screen)
{
lastnut = num;
lastscreen = screen;
if (initial_meal_add == 0 && meal_count(options.temp_meal_root) > 0)
 {
 the_ranking = 7;
 initial_meal_add = 1;
 }
switch (the_ranking)
 {
 case 0: per_100_grams(num, screen);
         break;
 case 1: per_100_grams_dry_weight(num, screen);
         break;
 case 2: per_100_grams_within_food_group(num, screen);
         break;
 case 3: per_100_calories(num, screen);
         break;
 case 4: per_serving(num, screen);
         break;
 case 5: per_serving_minimize(num, screen, 1);
         break;
 case 6: per_serving_minimize(num, screen, 0);
         break;
 default:  per_daily_recorded_meals(num, screen);
           break;
 }
}

void FoodRanking::set_food_group(int fg)
{
food_group = fg;
per_100_grams_within_food_group(lastnut, lastscreen);
}

void FoodRanking::set_minimize(int min)
{
minimize = min;
per_serving_minimize(lastnut, lastscreen, -1);
}

void FoodRanking::food_present(int foodno)
{
static int count = 0;
static FoodButton *array[600];

if (foodno == -1)
 {
 for (int i = 0; i < count; i++) foodpack->remove(array[i]);
 count = 0;
 }

if (foodno > -1 && count < 600)
 {
 foodpack->add(FoodButtonArray[foodno]);
 array[count] = FoodButtonArray[foodno];
 count++;
 }
}

int FoodRanking::max_array(void)
{
int count, i = 0;
for ( count = 1 ; count < FoodCount ; count++ ) if (abacus[count] > abacus[i]) i = count;
return i;
}

void FoodRanking::per_daily_recorded_meals(int num, int screen)
{
int count, meals = 0, maxmeal, max = 0;
float days;
struct food *food_ptr = &food_root;
struct meal *meal_ptr = options.temp_meal_root;
char meal_date[9], meal = '\0';
mb->label("Foods Ranked per Daily Recorded Meals");
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
if (meal_count(meal_ptr) > 0)
 {
 if ( meal_count(options.temp_meal_root) < options.defanal || options.defanal == 0)
  {
  days = (float) meal_count(options.temp_meal_root) / (float) options.mealsperday;
  maxmeal = meal_count(options.temp_meal_root);
  }
 else
  {
  days = (float) options.defanal / (float) options.mealsperday;
  maxmeal = options.defanal;                                   
  }
 while ( meal_ptr->next != NULL && meals <= maxmeal )
  {
  meal_ptr = meal_ptr->next;
  if (strcmp(meal_date,meal_ptr->meal_date) != 0 || meal != meal_ptr->meal)
   {
   strcpy(meal_date,meal_ptr->meal_date);
   meal = meal_ptr->meal;
   meals++;
   }
  if ( meals <= maxmeal ) abacus[meal_ptr->food_no] += meal_ptr->grams;
  }
 for (count = 0 ; count < FoodCount ; count++)
  { 
  abacus[count] /= days;
  abacus[count] *= FoodIndex[count]->nutrient[num];
  }
 max = max_array(); 
 count = 0;
 while (abacus[max] != 0 && count < 600)
  {
  food_ptr = food_number(max);
  FoodButtonArray[max]->grams(abacus[max] / food_ptr->nutrient[num]);
  food_present(max);
  count++;
  abacus[max] = 0;
  max = max_array();
  }
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::per_100_grams(int num, int screen)
{
int count, max = 0;
struct food *food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
mb->label("Foods Ranked per 100 Grams");
for (count = 0 ; count < FoodCount ; count++) abacus[count] = FoodIndex[count]->nutrient[num];
max = max_array(); 
count = 0;
while (abacus[max] != 0 && count < 600)
 {
 food_ptr = food_number(max);
 FoodButtonArray[max]->grams(100);
 food_present(max);
 count++;
 abacus[max] = 0;
 max = max_array();
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::per_100_grams_dry_weight(int num, int screen)
{
int count, max = 0;
struct food *food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
mb->label("Foods Ranked per 100 Grams Dry Weight");
if (num != WATER)
 {
 for (count = 0 ; count < FoodCount ; count++)
  {
  abacus[count] = (100 * FoodIndex[count]->nutrient[num]) / (100 - FoodIndex[count]->nutrient[WATER]);
  if (!(FoodIndex[count]->nutrient[WATER] < 100)) abacus[count] = 0;
  }
 max = max_array(); 
 count = 0;
 while (abacus[max] != 0 && count < 600)
  {
  food_ptr = food_number(max);
  FoodButtonArray[max]->grams(100 * abacus[max] / food_ptr->nutrient[num]);
  food_present(max);
  count++;
  abacus[max] = 0;
  max = max_array();
  }
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::per_100_grams_within_food_group(int num, int screen)
{
if (food_group == -1)
 {
 story->choose_food_group();
 return;
 }
int count, max = 0;
struct food *food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
sprintf(mbbuf,"Foods Ranked per 100 Grams within \"%s\"",FdGrp[food_group]);
mb->label(mbbuf);
for (count = 0 ; count < FoodCount ; count++) if (FoodIndex[count]->fdgrp == food_group) abacus[count] = FoodIndex[count]->nutrient[num];
max = max_array();
count = 0;
while (abacus[max] != 0 && count < 600)
 {
 food_ptr = food_number(max);
 FoodButtonArray[max]->grams(100);
 food_present(max);
 count++;
 abacus[max] = 0;
 max = max_array();
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::per_serving_minimize(int num, int screen, int type)
{
static int savetype;
if (minimize == -1)
 {
 savetype = type;
 story->choose_minimize();
 return;
 }
if (type != -1) savetype = type;
int count, max = 0;
struct food *food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
if (savetype == 1) sprintf(mbbuf,"Foods Ranked per Serving, Minimize %s, \"no data\" instances included",Nutrient[minimize]);
else sprintf(mbbuf,"Foods Ranked per Serving, Minimize %s, \"no data\" instances excluded",Nutrient[minimize]);
mb->label(mbbuf);
float minavg = 0;
int mincount = 0;

for (count = 0 ; count < FoodCount ; count++) 
 {
 food_ptr = food_ptr->next;
 if (! test_for_negative_zero(&food_ptr->nutrient[minimize]))
  {
  minavg += food_ptr->nutrient[minimize];
  mincount++;
  }
 }
if (mincount > 0) minavg /= mincount;
food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) 
 {
 food_ptr = food_ptr->next;
 if (mincount == 0 ||
     test_for_negative_zero(&food_ptr->nutrient[num]) ||
     (savetype == 0 && test_for_negative_zero(&food_ptr->nutrient[minimize])) ||
     minimize == num ||
     (num == VITE && minimize == TOCPHA) || (num == TOCPHA && minimize == VITE) ||
     (num == LA && minimize == F18D2) || (num == F18D2 && minimize == LA) ||
     (num == LA && minimize == F18D2CN6) || (num == F18D2CN6 && minimize == LA) ||
     (num == AA && minimize == F20D4) || (num == F20D4 && minimize == AA) ||
     (num == ALA && minimize == F18D3) || (num == F18D3 && minimize == ALA) ||
     (num == ALA && minimize == F18D3CN3) || (num == F18D3CN3 && minimize == ALA) ||
     (num == EPA && minimize == F20D5) || (num == F20D5 && minimize == EPA) ||
     (num == DHA && minimize == F22D6) || (num == F22D6 && minimize == DHA))
     abacus[count] = 0;
 else abacus[count] = (food_ptr->grams / -100 * food_ptr->nutrient[num]) * ((food_ptr->grams / 100 * food_ptr->nutrient[minimize] / minavg) - 1);
 }

max = max_array();
count = 0;
while (abacus[max] != 0 && count < 600)
 {
 food_ptr = food_number(max);
 FoodButtonArray[max]->grams(food_ptr->grams);
 food_present(max);
 count++;
 abacus[max] = 0;
 max = max_array();
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::per_100_calories(int num, int screen)
{
int count, max = 0;
struct food *food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
mb->label("Foods Ranked per 100 Calories");
if (num != ENERC_KCAL && num != ENERC_KJ)
 {
 for (count = 0 ; count < FoodCount ; count++)
  {
  if (FoodIndex[count]->nutrient[ENERC_KCAL] > 0) abacus[count] = 100 * FoodIndex[count]->nutrient[num] / FoodIndex[count]->nutrient[ENERC_KCAL];
  else abacus[count] = 0;
  }
 max = max_array(); 
 count = 0;
 while (abacus[max] != 0 && count < 600)
  {
  food_ptr = food_number(max);
  FoodButtonArray[max]->grams(100 * abacus[max] / food_ptr->nutrient[num]);
  food_present(max);
  count++;
  abacus[max] = 0;
  max = max_array();
  }
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::per_serving(int num, int screen)
{
int count, max = 0;
struct food *food_ptr = &food_root;
for (count = 0 ; count < FoodCount ; count++) abacus[count] = 0;
food_present(-1);
mb->label("Foods Ranked per Serving");
for (count = 0 ; count < FoodCount ; count++) abacus[count] = FoodIndex[count]->grams / 100 * FoodIndex[count]->nutrient[num];
max = max_array(); 
count = 0;
while (abacus[max] != 0 && count < 600)
 {
 food_ptr = food_number(max);
 FoodButtonArray[max]->grams(food_ptr->grams);
 food_present(max);
 count++;
 abacus[max] = 0;
 max = max_array();
 }
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
Fl::flush();
}

void FoodRanking::reindex_foodbuttons(int foodnum)
{
for (int i = FoodCount; i > foodnum; i--) FoodButtonArray[i] = FoodButtonArray[i-1];
FoodButtonArray[foodnum] = new FoodButton(x(),y(),17*w()/27,h()/23, FoodIndex[foodnum]);
foodpack->add(FoodButtonArray[foodnum]);
foodpack->remove(FoodButtonArray[foodnum]);
}

void FoodRanking::resize(int x, int y, int w, int h)
{
int i;
Fl_Group::resize(x, y, w, h);
mb->labelsize(FL_NORMAL_SIZE);
for (i = 0; i < 8; i++) pulldown[i].labelsize(FL_NORMAL_SIZE);
for (i = 0; i < FoodCount; i++) FoodButtonArray[i]->resize(x,y,17*w/27,h/23);
}

/* Class TheStory */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "TheStory.h"
#include <FL/Fl.H>
#ifndef __hpux
#include <stdint.h>
#endif

static TheStory *who_we_are;

void food_group_selector_cb(Fl_Widget *which, void *what)
{
who_we_are->set_food_group( (intptr_t) what );
}

TheStory::TheStory (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
int i;
who_we_are = this;
lastnut = -1;
lastscreen = -1;
minimize = 0;
this->label("Options");
this->labeltype(FL_ENGRAVED_LABEL);
for (i = 0; i < NUTRIENT_COUNT; i++) foodwork.nutrient[i] = -383838;
mainscreen = new Fl_Group(x, y, w, h);
mainscreen->color(widgetcolor);
ranking = new FoodRanking(x, y, 2*w/3-FL_NORMAL_SIZE/2, h, widgetcolor, this);
charts = new NutCharts(x+2*w/3+FL_NORMAL_SIZE/2, y, w/3-FL_NORMAL_SIZE/2, h, widgetcolor);
mainscreen->end();
foodgroup = new Fl_Group(x, y, w, h);
foodgroup->color(widgetcolor);
 {
 new Nut_Box(x, y, w, 2*h/23, "Choose Food Group for Food Ranking:");
 }
 {
 int count;
 int column[2] = {w/12, 3*w/8+2*w/12};
 int row[14];
 int intdiv = MaxFdGrp / 2 + (MaxFdGrp % 2 == 0 ? 0 : 1);
 for ( count = 0 ; count < 14; count++) row[count] = 2*h/23 + h/20 + count*FL_NORMAL_SIZE/2 + count*h/20;
 for ( count = 0 ; count <= MaxFdGrp / 2 - (MaxFdGrp % 2 == 0 ? 1 : 0); count++)
  {
   {
   Nut_Button *o = new Nut_Button(column[0], row[count], 3*w/8, h/20, FdGrp[FdGrpMap[count]]);
   if (!Fl::scheme()) o->box(FL_OSHADOW_BOX);
   o->color(FL_DARK_GREEN);
   o->labelcolor(FL_WHITE);
   o->labelfont(FL_BOLD);
   o->clear_visible_focus();
   o->callback(food_group_selector_cb, (void *) FdGrpMap[count]);
   }
   {
   if (count+intdiv < MaxFdGrp)
    {
    Nut_Button *o = new Nut_Button(column[1], row[count], 3*w/8, h/20, FdGrp[FdGrpMap[count+intdiv]]);
    if (!Fl::scheme()) o->box(FL_OSHADOW_BOX);
    o->color(FL_DARK_GREEN);
    o->labelcolor(FL_WHITE);
    o->labelfont(FL_BOLD);
    o->clear_visible_focus();
    o->callback(food_group_selector_cb, (void *) (FdGrpMap[count+intdiv]));
    }
   }
  }
 }
foodgroup->end();
nbw = new NutButtonWidget(x, y, w, h, widgetcolor, &foodwork);
this->end();
for (i = 0; i < MAX_SCREEN; i++)
 {
 Nut_Box *o = new Nut_Box(x, y, w, 6*h/22, "Choose Nutrient to Minimize:");
 nbw->wizard[i][0]->add(o);
 }
nbw->update();                                
}

void TheStory::new_story(int nut, int screen)
{
if (minimize) 
 {
 minimize = 0;
 set_minimize(nut);
 return;
 } 
this->label(Nutrient[nut]);
lastnut = nut;
lastscreen = screen;
charts->populate(nut, screen);
ranking->populate(nut, screen);
}

void TheStory::update(void)
{
if (lastnut > -1)
 {
 charts->populate(lastnut, options.screen);
 ranking->populate(lastnut, options.screen);
 }
}

void TheStory::choose_food_group(void)
{
this->value(foodgroup);
}

void TheStory::choose_minimize(void)
{
minimize = 1;
this->value(nbw);
}

void TheStory::set_food_group(int foodgroup)
{
this->value(mainscreen);
ranking->set_food_group(foodgroup);
}

void TheStory::set_minimize(int nutnum)
{
this->value(mainscreen);
ranking->set_minimize(nutnum);
}

void TheStory::reindex_foodbuttons(int foodnum)
{
ranking->reindex_foodbuttons(foodnum);
}

/* ViewFoods.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef VIEWFOODS_H
#define VIEWFOODS_H

#include "Nut_Input.h"
#include "FoodChoice.h"
#include "ServingMenuButton.h"
#include "AddToMealWidget.h"
#include "NewRecipeWidget.h"
#include "options.h"

class FoodChoice;

class ViewFoods : public Fl_Wizard
{
public:
ViewFoods(int x, int y, int w, int h, Fl_Color widgetcolor);
void show_food(struct food *foodptr, float grams, bool replace);
void new_serving_unit(float grams, float servingunit);
void quantity_change(double newval);
void update(void);
void change_default_serving(char *label);
void change_default_serving(void);
void user_serving_unit(void);
void tab_change(void);
void update_var_elements(void);
struct food *what_food(void);
float how_much(void);
void new_recipe(void);
void recipe_done(void);
void reindex_foodbuttons(int foodnum);
struct food foodwork, zero;
FoodChoice *fc;
Nut_Input *new_serving_unit_box, *new_serving_qty_box;
NutButtonWidget *nbw;

private:
struct food *current_food;
AddToMealWidget *addtomeal[MAX_SCREEN];
NewRecipeWidget *nrw[MAX_SCREEN];
Nut_Button *addtomealbutton;
Nut_Box *noaddtomealbutton;
Nut_ValueOutput *value_output[4][MAX_SCREEN];
Fl_Group *setservingunit;
char current_food_name[80], waterbuf[30], refusebuf[30];
Nut_Box *nutintro[MAX_SCREEN], *water[MAX_SCREEN], *refuse[MAX_SCREEN];
ServingMenuButton *serving_unit[MAX_SCREEN];
float serving_unit_factor;
};

#endif

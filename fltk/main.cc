/* main.cc */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "db.h"
#include "Nut.h"
#include "Nut_Window.h"
#include <FL/Fl_Tooltip.H>

#if defined USING_X11
#include <FL/x.H>
#include <X11/xpm.h>
#include "../nuticon.xpm"
#endif

int main(int argc, char **argv)
{
food_root.next = NULL;
meal_root.next = NULL;
recipe_root.next = NULL;
theusual_root.next = NULL;
char title[60];
initializations();
int i = 0;
if (!Fl::args(argc,argv,i) || i < argc-1)
 {
 make_filenames(argv[1]);
 sprintf(title,"NUT %s for %s [DB] %s",VERSIONQ,USDAVERSION,argv[1]);
 }
else if (i < argc)
 {
 make_filenames(argv[i]);
 sprintf(title,"NUT %s for %s [DB] %s",VERSIONQ,USDAVERSION,argv[i]);
 }
else 
 {
 make_filenames("");
 sprintf(title,"NUT %s for %s",VERSIONQ,USDAVERSION);
 }
if ( version(0) == MAJVERSION && version(1) == MINVERSION && read_food_db() )
 {
 read_meal_db();
 read_theusual_db();
 read_OPTIONS(0);
 }
else read_food_files();
if (version(0) != MAJVERSION || version(1) != MINVERSION) write_version();

FL_NORMAL_SIZE = fontsize();

#if defined NOT_XFT
int w = 51*(FL_NORMAL_SIZE+4);
if (w > Fl::w())
 {
 FL_NORMAL_SIZE = Fl::w()/50 - 4;
 write_fontsize(FL_NORMAL_SIZE);
 w = 51*(FL_NORMAL_SIZE+4);
 }
#else
int w = 65*(FL_NORMAL_SIZE+1);
if (w > Fl::w() + 20)
 {
 FL_NORMAL_SIZE = Fl::w()/64 - 1;
 write_fontsize(FL_NORMAL_SIZE);
 w = 65*(FL_NORMAL_SIZE+1);
 }
#endif

int h = 1000 * w / 1618, margin = FL_NORMAL_SIZE/7;
Fl_Tooltip::size(FL_NORMAL_SIZE);
Fl::scrollbar_size(FL_NORMAL_SIZE);

Nut_Window *window = new Nut_Window(w,h,title);

#if defined USING_X11
fl_open_display();
Pixmap p, mask;
XpmCreatePixmapFromData(fl_display, DefaultRootWindow(fl_display), (char**) nuticon, &p, &mask, NULL);
window->icon((const void *)p);
#endif

new Nut(margin, margin, w-2*margin, h-2*margin);
window->end();
window->show(argc, argv);
return Fl::run();
}

/* FoodSuggestions.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FOODSUGGESTIONS_H
#define FOODSUGGESTIONS_H

#include "Nut_Scroll.h"
#include "Nut_Pack.h"
#include "DeficNut.h"

class AnalyzeMeals;

class FoodSuggestions : public Fl_Wizard
{
public:
FoodSuggestions(int x, int y, int w, int h, Fl_Color widgetcolor, AnalyzeMeals *amparent, struct food *foodwork, const char *label);
void update(int max);
void tab_change(void);
Fl_Group *here_are_foods;

protected:
void resize(int x, int y, int w, int h);

private:
AnalyzeMeals *am;
struct food *foodwork;
Fl_Group *you_have_achieved, *here_are_nuts;
Nut_Pack *nutpack, *foodpack;
Nut_Scroll *ancient_scroll1, *ancient_scroll2;
DeficNut *DeficNuts[NUTRIENT_COUNT];
int frcount;
};

#endif

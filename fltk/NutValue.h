/* NutValue.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NUTVALUE_H
#define NUTVALUE_H

#include <FL/Fl_Wizard.H>
#include "Nut_Box.h"
#include "food.h"

class NutButtonWidget;

class NutValue : public Fl_Wizard
{
public:
NutValue(int x, int y, int w, int h, Fl_Color widgetcolor, NutButtonWidget *nbw, struct food *foodwork, int nutindex, int screen);
NutValue(int x, int y, int w, int h, Fl_Color widgetcolor, NutButtonWidget *nbw, struct food *foodwork, int nutindex, int screen, int cpf);
NutValue(int x, int y, int w, int h, Fl_Color widgetcolor, NutButtonWidget *nbw, struct food *foodwork, int nutindex, int screen, int cpf, int omega);
void update(void);

private:
struct food *foodwork;
float *data;
char valbuf[30];
int nutindex;
int screen;
Nut_Box *valbox;
Nut_Box *nodata;
Nut_Box *questionmark;
Fl_Group *overflow;
Fl_Group *underflow;
Fl_Group *display;
int special;
float pctfat;
float pctcarb;
float pctprot;
float p3;
float p6;
float h3;
float h6;
float o;
int n6;
};

#endif

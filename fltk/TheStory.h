/* TheStory.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef THESTORY_H
#define THESTORY_H

#include "NutCharts.h"
#include "FoodRanking.h"

class FoodRanking;

class TheStory : public Fl_Wizard
{
public:
TheStory(int x, int y, int w, int h, Fl_Color widgetcolor);
void new_story(int nut, int screen);
void update(void);
void choose_food_group(void);
void set_food_group(int fg);
void choose_minimize(void);
void set_minimize(int min);
void reindex_foodbuttons(int foodnum);
NutCharts *charts;
FoodRanking *ranking;
NutButtonWidget *nbw;
struct food foodwork;

private:
int lastnut, lastscreen;
Fl_Group *mainscreen, *foodgroup;
bool minimize;
};

#endif

/* Class ServingValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ServingValueOutput.h"

static void gram_change_s_cb (Fl_Widget *which, void *who)
{
ViewFoods *vfptr = (ViewFoods *)who;
vfptr->quantity_change(((Fl_Valuator*)which)->value());
}

ServingValueOutput::ServingValueOutput (int x, int y, int w, int h, ViewFoods *vf, Fl_Color widgetcolor) : Nut_ValueOutput (x, y, w, h)
{
this->color(fl_lighter(widgetcolor));
this->step(0.1);
this->bounds(0, 99999);
this->soft(1);
this->callback((Fl_Callback*)gram_change_s_cb,vf);
}

int ServingValueOutput::format(char *buffer)
{
return snprintf(buffer, 128, "%0.3g", factor > 0 ? value() / factor : 0);
}

void ServingValueOutput::set_factor(float servingsize)
{
if (servingsize > 0)
 {
 factor = servingsize;
 this->bounds(0, 99999);
 }
else this->bounds(0, 0);
this->step(servingsize / 10);
}

float ServingValueOutput::get_factor(void)
{
return factor;
}

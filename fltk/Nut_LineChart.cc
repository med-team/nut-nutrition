/* Class Nut_LineChart */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "Nut_LineChart.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>

Nut_LineChart::Nut_LineChart (int x, int y, int w, int h) : Fl_Chart (x, y, w, h)
{
type(FL_LINE_CHART);
textsize(FL_NORMAL_SIZE);
labelsize(FL_NORMAL_SIZE);
}

void Nut_LineChart::draw() {
fl_line_style(0, FL_NORMAL_SIZE/6*1277/Fl::w());
Fl_Chart::draw();
fl_line_style(0);
}

void Nut_LineChart::resize(int x, int y, int w, int h)
{
textsize(FL_NORMAL_SIZE);
labelsize(FL_NORMAL_SIZE);
Fl_Chart::resize(x, y, w, h);
}

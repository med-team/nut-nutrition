/* Class Nut */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "AddRecipe.h"
#include "PersonalOptions.h"
#include "TheStory.h"
#include "db.h"
#include <FL/Fl.H>
#include <string.h>

static TheStory *story;
static AnalyzeMeals *am;
static RecordMeals *rm;
static AddRecipe *ar;
static ViewFoods *vf;
static PersonalOptions *po;
static Nut *who_we_are;
static Fl_Widget *TabList[2];

static bool need_to_update_story;
static bool need_dv_change;
static bool dv_change_in_progress;
static bool need_to_update_analysis;
static bool recipe_mode = 0;

void update_pcf()
{
rm->update_pcf();
}

bool get_recipe_mode(void)
{
return recipe_mode;
}

Fl_Color scheme_color(Fl_Color widgetcolor)
{
if (Fl::scheme() && !strcmp(Fl::scheme(), "plastic")) return (fl_color_average(fl_gray_ramp(16), widgetcolor, 0.75f));
return widgetcolor;
}

void gram_ounce_change(void)
{
ar->gram_ounce_change();
}

void new_recipe_cb(Fl_Widget *whatever)
{
int i, children = who_we_are->children();
Fl_Widget *childlist[10];
for (i = 0; i < children; i++) childlist[i] = who_we_are->child(i);
for (i = 1; i < children; i++) who_we_are->remove(childlist[i]);
who_we_are->add(ar);
for (i = 2; i < children; i++) who_we_are->add(childlist[i]);
who_we_are->value(ar);
vf->new_recipe();
redraw_whole_schmeer();
recipe_mode = 1;
TabList[0] = ar;
TabList[1] = vf;
}

void recipe_done_cb(Fl_Widget *whatever)
{
if (!ar->recipe_done()) return;
int i, children = who_we_are->children();
Fl_Widget *childlist[10];
for (i = 0; i < children; i++) childlist[i] = who_we_are->child(i);
for (i = 1; i < children; i++) who_we_are->remove(childlist[i]);
who_we_are->add(rm);
for (i = 2; i < children; i++) who_we_are->add(childlist[i]);
vf->recipe_done();
who_we_are->value(vf);
redraw_whole_schmeer();
recipe_mode = 0;
TabList[0] = vf;
TabList[1] = rm;
}

void new_story(int nut, int screen)
{
story->new_story(nut, screen);                        
who_we_are->add(story);
who_we_are->value(story);
TabList[1] = TabList[0];
TabList[0] = story;
}

void show_food(struct food *fptr, float grams)
{
vf->show_food(fptr, grams, 0);
who_we_are->value(vf);
redraw_whole_schmeer();
TabList[1] = TabList[0];
TabList[0] = vf;
}

void show_new_food(struct food *fptr, float grams)
{
vf->show_food(fptr, grams, 0);
who_we_are->value(vf);
TabList[1] = rm;
TabList[0] = vf;
}

void show_food_with_replace(struct food *fptr, float grams)
{
vf->show_food(fptr, grams, 1);
who_we_are->value(vf);
TabList[1] = TabList[0];
TabList[0] = vf;
}

void skip_show_food(struct food *fptr, float grams)
{
if (recipe_mode) ar->direct_add_to_meal(fptr, grams);
else rm->direct_add_to_meal(fptr, grams);
}

static void tab_change_cb (Fl_Widget *ptr)
{
po->prev();
am->tab_change();
TabList[1] = TabList[0];
TabList[0] = who_we_are->value();
rm->tab_change();
vf->tab_change();
if (TabList[0] == am) defanal_change(am);
if (TabList[0] == rm && rm->value() != rm->potentialmeal) defanal_change(rm);
redraw_whole_schmeer();
}

void back_cb (Fl_Widget *which)
{
Fl_Widget *savetab = TabList[0];
TabList[0] = TabList[1];
who_we_are->value(TabList[0]);
if (TabList[0] == vf && savetab == vf)
 {
 vf->value(vf->fc);
 vf->fc->search_box->take_focus();
 vf->label("View Foods");
 redraw_whole_schmeer();
 }
}

void dv_change(void)
{
need_dv_change = 1;
while (Fl::event_buttons() != 0) Fl::check();
if (need_dv_change && !dv_change_in_progress)
 {
 need_dv_change = 0;
 dv_change_in_progress = 1;
 am->update_am_dv_change_only();
 rm->update(1);
 vf->update();
 story->update();
 po->update();
 dv_change_in_progress = 0;
 }
}

void dv_change_minus_po(void)
{
need_dv_change = 1;
while (Fl::event_buttons() != 0) Fl::check();
if (need_dv_change)
 {
 need_dv_change = 0;
 am->update_am_dv_change_only();
 rm->update(1);
 vf->update();
 story->update();
 }
}

void screen_change(void)
{
story->update();
}

void defanal_change(AnalyzeMeals *amptr)
{
need_to_update_story = 1;
while (Fl::event_buttons() != 0) Fl::check();
if (need_to_update_story)
 {
 need_to_update_story = 0;
 if (TabList[0] == am)
  {
  options.defanal = options.defanalanal;
  options.temp_meal_root = am->tell_me_meal_root();
  }
 else
  {
  options.defanal = options.defanalrec;
  options.temp_meal_root = rm->tell_me_meal_root();
  }
 write_OPTIONS();
 story->update();
 }
}

void defanal_change(RecordMeals *rmptr)
{
//am->update_am();
need_to_update_story = 1;
while (Fl::event_buttons() != 0) Fl::check();
if (need_to_update_story)
 {
 need_to_update_story = 0;
 options.defanal = options.defanalrec;
 options.temp_meal_root = rm->tell_me_meal_root();
 write_OPTIONS();
 story->update();
 }
rm->update(0);
}

void redraw_whole_schmeer(void)
{
who_we_are->parent()->redraw();
}

void reindex_foodbuttonarrays(int foodnum)
{
ar->reindex_foodbuttons(foodnum);
rm->reindex_foodbuttons(foodnum);
vf->reindex_foodbuttons(foodnum);
story->reindex_foodbuttons(foodnum);
}

void add_to_meal_cb(Fl_Widget *which)
{
struct food *foodptr = NULL;
if (recipe_mode)
 {
ar->add_to_meal(vf);
who_we_are->value(ar);
vf->show_food(foodptr, 0, 0);
TabList[1] = TabList[0];
TabList[0] = ar;
 }
else
 {
who_we_are->value(rm);
rm->add_to_meal(vf);
vf->show_food(foodptr, 0, 0);
TabList[1] = TabList[0];
TabList[0] = rm;
 }
}

void food_choice_cancel_cb(Fl_Widget *whatever)
{
if (recipe_mode) ar->food_choice_cancel();
else rm->food_choice_cancel();
}

void no_op_cb(Fl_Widget *whatever) {}

void delete_meal_food_cb(Fl_Widget *whatever, void *whichone)
{
MealFood *mf = (MealFood *) whichone;
Fl_Widget *button = (Fl_Widget *)whatever;
button->callback(no_op_cb);
if (recipe_mode) ar->delete_meal_food(mf);
else rm->delete_meal_food(mf);
}

void delete_meal_food_with_replace_cb(Fl_Widget *whatever, void *whichone)
{
MealFood *mf = (MealFood *) whichone;
if (recipe_mode) ar->delete_meal_food_with_replace(mf);
else rm->delete_meal_food_with_replace(mf);
}

void request_mvo_initial_set(void)
{
rm->tab_change();
rm->redraw();
}

void update_analysis(void)
{
need_to_update_analysis = 1;
while (Fl::event_buttons() != 0) Fl::check();
if (need_to_update_analysis)
 {
 need_to_update_analysis = 0;
 am->update_am(-1);
 rm->update(0);
 vf->update();
 story->update();
 }
}

Nut::Nut (int x, int y, int w, int h) : Fl_Tabs (x, y, w, h)
{
int tabheight = h / 23;
am = new AnalyzeMeals(x, y+tabheight, w, h-tabheight, FL_CYAN);
rm = new RecordMeals(x, y+tabheight, w, h-tabheight, MEALCOLOR);
ar = new AddRecipe(x, y+tabheight, w, h-tabheight, RECIPECOLOR);
vf = new ViewFoods(x, y+tabheight, w, h-tabheight, FL_GREEN);
po = new PersonalOptions(x, y+tabheight, w, h-tabheight, fl_lighter(FL_BLUE));
story = new TheStory(x, y+tabheight, w, h-tabheight, FL_YELLOW);
this->labelcolor(FL_RED);
this->clear_visible_focus();
this->end();
this->remove(story);
this->remove(ar);
who_we_are = this;
this->callback(tab_change_cb);
TabList[0] = am;
options.defanal = options.defanalanal;
dv_change_in_progress = 0;
}

void Nut::resize(int x, int y, int w, int h)
{
if ( ar->parent() == NULL) ar->resize(x, y+h/23, w, h-h/23);
if ( story->parent() == NULL) story->resize(x, y+h/23, w, h-h/23);
am->labelsize(FL_NORMAL_SIZE);
rm->labelsize(FL_NORMAL_SIZE);
ar->labelsize(FL_NORMAL_SIZE);
vf->labelsize(FL_NORMAL_SIZE);
po->labelsize(FL_NORMAL_SIZE);
story->labelsize(FL_NORMAL_SIZE);
Fl_Tabs::resize(x, y, w, h);
}

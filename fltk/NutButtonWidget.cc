/* Class NutButtonWidget */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "db.h"
#include <stdlib.h>

static NutButtonWidget *nbwobj[10];
static int nbwobjcount = 0;

//static void cb_exit(Fl_Widget *which)
//{
//exit(0);
//}

static void sync_all_nbws_cb (Fl_Widget *which, void *who)
{
NutButtonWidget *us_right_now = (NutButtonWidget *)who;
Fl_Widget* const *aptr = us_right_now->array();
int save = 0;
for (int i = 0; i <= MAX_SCREEN; i++) if (aptr[i]->visible()) for (int j = 0; j < nbwobjcount; j++) if (nbwobj[j] != us_right_now)
 {
 Fl_Widget* const *aptrthem = nbwobj[j]->array();
 nbwobj[j]->value(aptrthem[i]);
 save = i;
 }
if (save == MAX_SCREEN) exit(0);
//if (save == MAX_SCREEN) Fl::add_timeout(1.13, (Fl_Timeout_Handler)cb_exit, (void *)which);
if (save != MAX_SCREEN)
 {
 options.screen = save;
 write_OPTIONS();
 screen_change();
 }
}

NutButtonWidget::NutButtonWidget(int x, int y, int w, int h, Fl_Color widgetcolor, struct food *foodwork) : Fl_Tabs(x, y, w, h)
{
int i, j, k, tabheight = h / 22, packspacing = FL_NORMAL_SIZE / 3;
int column[6], row[15];
Fl_Group *screen[MAX_SCREEN+1];
Fl_Group *nuttable[MAX_SCREEN];
update_count = 0;
Fl_Group::current(0);
this->color(FL_WHITE,widgetcolor);
this->clear_visible_focus();
nbwobj[nbwobjcount++] = this;
this->callback(sync_all_nbws_cb,this);
const char *screentitle[] =
 {
 "Daily Value %","DV Amounts",
 "Carbs && Amino Acids","Miscellaneous",
 "Sat && Mono Fatty Acids","Poly && Trans Fatty Acids"
 };
for (j = 0; j < 6; j++) column[j] = packspacing + j * w / 6;
for (j = 0; j < 15; j++) row[j] = 7*h/22 + j*tabheight;
for (j = 0; j < MAX_SCREEN; j++)
 {
 screen[j] = new Fl_Group(x, y, w, h-tabheight, screentitle[j]);
 Fl_Group::current(0);
 wizard[j][0] = new Fl_Wizard(x, y, w, 6*h/22);
 wizard[j][0]->box(FL_NO_BOX);
 Fl_Group::current(0);
 nuttable[j] = new Fl_Group(x, y+6*h/22, w, 15*h/22);
 Fl_Group::current(0);
 screen[j]->add(wizard[j][0]);
 screen[j]->add(nuttable[j]);
 screen[j]->labeltype(FL_EMBOSSED_LABEL);
 screen[j]->color(widgetcolor,widgetcolor);
 screen[j]->box(FL_NO_BOX);
 this->add(screen[j]);
 }
screen[MAX_SCREEN] = new Fl_Group(x, y, w, h-tabheight, "Quit NUT");
Fl_Group::current(0);
Fl_Box *has_ended = new Fl_Box(x, y, w, h-tabheight, "NUT has ended.");
has_ended->color(widgetcolor);
screen[MAX_SCREEN]->add(has_ended);
screen[MAX_SCREEN]->labeltype(FL_EMBOSSED_LABEL);
screen[MAX_SCREEN]->color(widgetcolor,widgetcolor);
this->add(screen[MAX_SCREEN]);
this->labelcolor(FL_YELLOW);

j = 0; k = 0;
calorie_button = new NutButton(column[k], row[0], w/6, tabheight-packspacing, ENERC_KCAL, j, "Calories (2000)");
nuttable[j]->add(calorie_button);
nuttable[j]->add(new NutButton(column[k], row[1], w/6, tabheight-packspacing, ENERC_KCAL, j, "Prot/Carb/Fat"));
for (i = 2; i < 14; i++) nuttable[j]->add(new NutButton(column[k], row[i+1], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 0; k = 1; i = 1;
nuttable[j]->add(new NutValue(column[k], row[0], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
nuttable[j]->add(new NutValue(column[k], row[1], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j, 0));
for (i = 2; i < 14; i++) nuttable[j]->add(new NutValue(column[k], row[i+1], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 0; k = 2;
for (i = 14; i < 16; i++) nuttable[j]->add(new NutButton(column[k], row[i-14], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));
for (i = 18; i < 30; i++) nuttable[j]->add(new NutButton(column[k], row[i-15], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 0; k = 3;
for (i = 14; i < 16; i++) nuttable[j]->add(new NutValue(column[k], row[i-14], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
for (i = 18; i < 30; i++) nuttable[j]->add(new NutValue(column[k], row[i-15], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 0; k = 4;
for (i = 17; i > 15; i--) nuttable[j]->add(new NutButton(column[k], row[17-i], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));
for (i = 30; i < 40; i++) nuttable[j]->add(new NutButton(column[k], row[i-27], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));
nuttable[j]->add(new NutButton(column[k], row[14], w/6, tabheight-packspacing, FAPU, j, "Omega-6/3 Balance"));

j = 0; k = 5;
for (i = 17; i > 15; i--) nuttable[j]->add(new NutValue(column[k], row[17-i], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
for (i = 30; i < 40; i++) nuttable[j]->add(new NutValue(column[k], row[i-27], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
nuttable[j]->add(new NutValue(column[k], row[14], w/6, tabheight-packspacing, widgetcolor, this, foodwork, FAPU, j, 0, 0));

j = 1; k = 0;
nuttable[j]->add(new NutButton(column[k], row[0], w/6, tabheight-packspacing, ENERC_KCAL, j, Nutrient[ENERC_KCAL]));
nuttable[j]->add(new NutButton(column[k], row[1], w/6, tabheight-packspacing, ENERC_KCAL, j, "Prot/Carb/Fat"));
for (i = 2; i < 14; i++) nuttable[j]->add(new NutButton(column[k], row[i+1], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 1; k = 1; i = 1;
nuttable[j]->add(new NutValue(column[k], row[0], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
nuttable[j]->add(new NutValue(column[k], row[1], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j, 0));
for (i = 2; i < 14; i++) nuttable[j]->add(new NutValue(column[k], row[i+1], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 1; k = 2;
for (i = 14; i < 16; i++) nuttable[j]->add(new NutButton(column[k], row[i-14], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));
for (i = 18; i < 30; i++) nuttable[j]->add(new NutButton(column[k], row[i-15], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 1; k = 3;
for (i = 14; i < 16; i++) nuttable[j]->add(new NutValue(column[k], row[i-14], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
for (i = 18; i < 30; i++) nuttable[j]->add(new NutValue(column[k], row[i-15], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 1; k = 4;
for (i = 17; i > 15; i--) nuttable[j]->add(new NutButton(column[k], row[17-i], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));
for (i = 30; i < 40; i++) nuttable[j]->add(new NutButton(column[k], row[i-27], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));
nuttable[j]->add(new NutButton(column[k], row[14], w/6, tabheight-packspacing, FAPU, j, "Omega-6/3 Balance"));

j = 1; k = 5;
for (i = 17; i > 15; i--) nuttable[j]->add(new NutValue(column[k], row[17-i], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
for (i = 30; i < 40; i++) nuttable[j]->add(new NutValue(column[k], row[i-27], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));
nuttable[j]->add(new NutValue(column[k], row[14], w/6, tabheight-packspacing, widgetcolor, this, foodwork, FAPU, j, 0, 0));

j = 2; k = 0;
for (i = 1; i < 11; i++) nuttable[j]->add(new NutButton(column[k], row[i+1], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 2; k = 1;
for (i = 1; i < 11; i++) nuttable[j]->add(new NutValue(column[k], row[i+1], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 2; k = 2;
for (i = 11; i < 22; i++) nuttable[j]->add(new NutButton(column[k], row[i-9], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 2; k = 3;
for (i = 11; i < 22; i++) nuttable[j]->add(new NutValue(column[k], row[i-9], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 2; k = 4;
for (i = 22; i < 32; i++) nuttable[j]->add(new NutButton(column[k], row[i-20], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 2; k = 5;
for (i = 22; i < 32; i++) nuttable[j]->add(new NutValue(column[k], row[i-20], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 3; k = 0;
for (i = 1; i < 14; i++) nuttable[j]->add(new NutButton(column[k], row[i], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 3; k = 1;
for (i = 1; i < 14; i++) nuttable[j]->add(new NutValue(column[k], row[i], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 3; k = 2;
for (i = 14; i < 27; i++) nuttable[j]->add(new NutButton(column[k], row[i-13], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 3; k = 3;
for (i = 14; i < 27; i++) nuttable[j]->add(new NutValue(column[k], row[i-13], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 3; k = 4;
for (i = 27; i < 40; i++) nuttable[j]->add(new NutButton(column[k], row[i-26], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 3; k = 5;
for (i = 27; i < 40; i++) nuttable[j]->add(new NutValue(column[k], row[i-26], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 4; k = 1;
for (i = 1; i < 16; i++) nuttable[j]->add(new NutButton(column[k], row[i-1], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 4; k = 2;
for (i = 1; i < 16; i++) nuttable[j]->add(new NutValue(column[k], row[i-1], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 4; k = 3;
for (i = 16; i < 28; i++) nuttable[j]->add(new NutButton(column[k], row[i-15], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 4; k = 4;
for (i = 16; i < 28; i++) nuttable[j]->add(new NutValue(column[k], row[i-15], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 5; k = 0;
for (i = 1; i < 9; i++) nuttable[j]->add(new NutButton(column[k], row[i+2], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 5; k = 1;
for (i = 1; i < 9; i++) nuttable[j]->add(new NutValue(column[k], row[i+2], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 5; k = 2;
for (i = 9; i < 19; i++) nuttable[j]->add(new NutButton(column[k], row[i-7], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 5; k = 3;
for (i = 9; i < 19; i++) nuttable[j]->add(new NutValue(column[k], row[i-7], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

j = 5; k = 4;
for (i = 19; i < 31; i++) nuttable[j]->add(new NutButton(column[k], row[i-18], w/6, tabheight-packspacing, ScreenMap[j][i], j, Nutrient[ScreenMap[j][i]]));

j = 5; k = 5;
for (i = 19; i < 31; i++) nuttable[j]->add(new NutValue(column[k], row[i-18], w/6, tabheight-packspacing, widgetcolor, this, foodwork, ScreenMap[j][i], j));

this->value(screen[options.screen]);
}

void NutButtonWidget::add_me_to_the_update_list(NutValue *nv)
{
update_list[update_count++] = nv;
}

void NutButtonWidget::update(void)
{
for (int i = 0; i < update_count; i++) update_list[i]->update();
snprintf(calbuf,20,"Calories (%1.0f)",DV[ENERC_KCAL]);
calorie_button->label(calbuf);
}

void NutButtonWidget::resize(int x, int y, int w, int h)
{
for (int j = 0; j <= MAX_SCREEN; j++) child(j)->labelsize(FL_NORMAL_SIZE);
Fl_Tabs::resize(x,y,w,h);
}

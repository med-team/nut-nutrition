/* Class TheUsualNameWidget */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include <FL/Fl.H>

static RecordMeals *rm;
static TheUsualNameWidget *who_we_are;

void tunw_cb(Fl_Widget *who, void *what)
{
Nut_Input *who_i_am = (Nut_Input *) what;
rm->new_customary_meal_phase_two(who_i_am->value());
}

void turn_off(Fl_Widget *who, void *what)
{
who_we_are->off();
}

TheUsualNameWidget::TheUsualNameWidget (int x, int y, int w, int h, Fl_Color widgetcolor, RecordMeals *rmeals) : Fl_Wizard (x, y, w, h)
{
rm = rmeals;
who_we_are = this;
this->box(FL_NO_BOX);
entry = new Fl_Group(x, y, w, h);
entry->color(widgetcolor);
 {
 Nut_Box *o = new Nut_Box(x, y, 14*w/64, h, "New Name");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
 {
 input = new Nut_Input(x+14*w/64, y, 16*w/64, h);
 input->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 input->when(FL_WHEN_ENTER_KEY);
 input->callback(tunw_cb,input);
 }
 {
 Nut_Box *o = new Nut_Box(x+31*w/64, y, 33*w/64, h, "max. 8 characters");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
entry->end();
blank = new Nut_Box(x, y, w, h);
blank->color(widgetcolor);
successful = new Nut_Box(x, y, w, h, "Copied to customary meal.");
successful->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
this->end();
}

void TheUsualNameWidget::on(void)
{
this->value(entry);
input->take_focus();
}

void TheUsualNameWidget::off(void)
{
this->value(blank);
input->value("");
}

void TheUsualNameWidget::success(const char *theusualid)
{
static char buf[60];
sprintf(buf,"Copied to customary meal %s",theusualid);
successful->label(buf);
this->value(successful);
Fl::add_timeout(3.0, (Fl_Timeout_Handler)turn_off);
}


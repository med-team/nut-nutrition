/* Class GOLightButton */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "GOLightButton.h"
#include "PCF.h"
#include "db.h"

static GOLightButton *GOLBlist[20];
static int GOLBcount;

void GOLB_cb(Fl_Widget *lb)
{
if (options.grams) options.grams = 0;
else options.grams = 1;
write_OPTIONS();
for (int i = 0; i < GOLBcount; i++) GOLBlist[i]->set_value();
if (get_recipe_mode()) gram_ounce_change();
else PCF::refresh_values();
lb->parent()->redraw();
}

GOLightButton::GOLightButton (int x, int y, int w, int h, Fl_Color widgetcolor, bool polarbear) : Nut_LightButton (x, y, w, h)
{
polarity = polarbear;
if (polarity) this->label("Grams");
else this->label("Ounces");
this->labelfont(FL_BOLD);
this->color(fl_lighter(widgetcolor));
this->selection_color(FL_RED);
this->clear_visible_focus();
this->callback(GOLB_cb);
GOLBlist[GOLBcount++] = this;
if (polarity) this->value(options.grams);
else this->value(!options.grams);
}

void GOLightButton::set_value(void)
{
if (polarity) this->value(options.grams);
else this->value(!options.grams);
}

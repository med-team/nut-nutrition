/* Class DeficNut */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "NutButton.h"

DeficNut::DeficNut (int x, int y, int w, int h, int nutindex, int screen, Fl_Color widgetcolor, const char *label) : Fl_Group (x, y, w, h)
{
new NutButton(x, y, w/2, h, nutindex, screen, label);
valbox = new Nut_Box(x+w/2, y, w/3, h);
valbox->align(FL_ALIGN_INSIDE|FL_ALIGN_RIGHT);
valbox->labelfont(FL_BOLD);
Nut_Box *o = new Nut_Box(x+5*w/6, y, w/6, h, "%");
o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
this->end();
}

void DeficNut::set_value(float value)
{
if (value >= 1) sprintf(valbuf,"%1.0f",value);
else sprintf(valbuf,"%g",value);
valbox->label(valbuf);
}

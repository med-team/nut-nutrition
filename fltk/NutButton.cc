/* Class NutButton */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "NutButton.h"
#include <FL/Fl.H>

static void click_me_nb_cb(Fl_Widget *w, void *data)
{
int nut, screen, code;
code = *(int *) data;
nut = code % 1000;
screen = code / 1000;
new_story(nut, screen);
}

NutButton::NutButton (int x, int y, int w, int h, int nutindex, int screen, const char *label) : Nut_Button (x, y, w, h, label)
{
nutcode = nutindex + screen * 1000;
if (!Fl::scheme()) this->box(FL_RSHADOW_BOX);
this->color(FL_YELLOW);
this->clear_visible_focus();
this->callback(click_me_nb_cb, &nutcode);
}

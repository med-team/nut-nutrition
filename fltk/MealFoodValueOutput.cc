/* Class MealFoodValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include <string.h>
#include <FL/Fl.H>

static void gram_change_mf_cb (Fl_Widget *which, void *who)
{
MealFood *mf = (MealFood *) who;
MealFoodValueOutput *mfvo = (MealFoodValueOutput *) which;
mf->gram_change(mfvo->value());
}

MealFoodValueOutput::MealFoodValueOutput (int x, int y, int w, int h, Fl_Color widgetcolor, MealFood *mf) : Nut_ValueOutput (x, y, w, h)
{
this->textfont(FL_BOLD);
if (!Fl::scheme() || strcmp(Fl::scheme(),"plastic")) this->color(fl_lighter(fl_lighter(widgetcolor)));
else this->color(widgetcolor);
this->step(1.0);
this->bounds(0, 99999);
this->soft(1);
this->callback((Fl_Callback*)gram_change_mf_cb,mf);
}

int MealFoodValueOutput::format(char *buffer)
{
if (options.grams)
 {
 this->step(1.0);
 return snprintf(buffer, 128, "%0.1f", value());
 }
else
 {
 this->step(GRAMS_IN_OUNCE / 8);
 return snprintf(buffer, 128, "%0.1f", value() / GRAMS_IN_OUNCE);
 }
}

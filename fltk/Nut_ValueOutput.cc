/* Class Nut_ValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include "Nut.h"

Nut_ValueOutput::Nut_ValueOutput (int x, int y, int w, int h) : Fl_Value_Output (x, y, w, h)
{
this->tooltip("Click and drag left and right to adjust,\nmiddle button 10x, right button 100x");
if (!Fl::scheme()) this->box(FL_SHADOW_BOX);
}

void Nut_ValueOutput::draw() {
  Fl_Boxtype b = box() ? box() : FL_DOWN_BOX;
  int X = x()+Fl::box_dx(b);
  int Y = y()+Fl::box_dy(b);
  int W = w()-Fl::box_dw(b);
  int H = h()-Fl::box_dh(b);
  if (damage()&~FL_DAMAGE_CHILD)
    draw_box(b, color());
  else {
    fl_color(color());
    fl_rectf(X, Y, W, H);
  }
  char buf[128];
  format(buf);
  fl_color(active_r() ? textcolor() : fl_inactive(textcolor()));
  fl_font(textfont(), textsize());
  fl_draw(buf,X,Y,W,H,FL_ALIGN_CENTER);
}

void Nut_ValueOutput::resize(int x, int y, int w, int h)
{
textsize(FL_NORMAL_SIZE);
Fl_Value_Output::resize(x, y, w, h);
}

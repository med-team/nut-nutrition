/* Class ServingMenuButton */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "db.h"
#include <string.h>
#include <stdlib.h>
#ifndef __hpux
#include <stdint.h>
#endif

static Fl_Menu_Item pulldown[] = { {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0} , {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
static char servdisp[20][81];
static char buf[1000];
static char *result[61];
static char *big_label;
static ViewFoods *vf;
static struct food *current_foodptr;

void menu_cb(Fl_Widget *which, void *n)
{
intptr_t num = (intptr_t) n;
if (num > 0)
 {
 big_label = result[3*num-2];
 vf->new_serving_unit(atof(result[3*num-1]), atof(result[3*num-3]));
 }
else if (num == 0) vf->change_default_serving(big_label);
else if (num == -1) vf->user_serving_unit();
}

void fill_serving_menu(struct food *foodptr)
{
int i, resultcount = 0;
int *resultparm = &resultcount;
current_foodptr = foodptr;
read_weightlib(foodptr->ndb_no,buf,sizeof(buf),result,resultparm);
for (i = 1; i <= resultcount / 3; i++)
 {
 strcpy(servdisp[i-1],result[3*i-3]);
 strcat(servdisp[i-1]," ");
 strcat(servdisp[i-1],result[3*i-2]);
 pulldown[i-1].label(servdisp[i-1]);
 pulldown[i-1].callback(menu_cb, (void *)i);
 pulldown[i-1].flags = 0;
 }
for (i = resultcount / 3 + 1; i < 20; i++) pulldown[i].flags = FL_MENU_INVISIBLE;
pulldown[resultcount / 3].flags = 0;
pulldown[resultcount / 3].label("Save the current serving as the default for this food");
pulldown[resultcount / 3].callback(menu_cb, (void *)0);
pulldown[resultcount / 3 + 1].flags = 0;
pulldown[resultcount / 3 + 1].label("Save the current serving as the default but I will enter a new serving unit");
pulldown[resultcount / 3 + 1].callback(menu_cb, (void *)-1);
}

ServingMenuButton::ServingMenuButton (int x, int y, int w, int h, ViewFoods *vfoods, Fl_Color widgetcolor) : Fl_Menu_Button (x, y, w, h)
{
vf = vfoods;
this->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
this->color(fl_lighter(widgetcolor));
this->clear_visible_focus();
this->menu(pulldown);
}

void ServingMenuButton::set_label(char *label)
{
this->menu(pulldown);
big_label = label;
this->label(big_label);
}

void ServingMenuButton::set_label(void)
{
this->label(big_label);
}

void ServingMenuButton::blank_out(void)
{
this->menu(NULL);
this->label("");
}

void ServingMenuButton::resize(int x, int y, int w, int h)
{
Fl_Menu_Button::resize(x, y, w, h);
this->labelsize(FL_NORMAL_SIZE);
for (int i = 0; i < 21; i++) if (pulldown[i].label() != NULL) pulldown[i].labelsize(FL_NORMAL_SIZE);
}

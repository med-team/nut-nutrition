/* Class PersonalOptions */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "PersonalOptions.h"
#include "DVValueOutput.h"
#include "WeightCharts.h"
#include "db.h"
#include "recmeal.h"
#include <FL/Fl.H>
#include <FL/filename.H>
#ifndef __hpux
#include <stdint.h>
#endif
#include <math.h>
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
#include <stdlib.h>
#include <string.h>
#endif

struct food *foodwork;
DVValueOutput *cvo, *fatvo, *pvo, *nfcvo, *fibvo, *satvo, *efavo, *obvo;
Nut_LightButton *balfat, *adjfat, *adjcal, *autoset, *adjfib, *balfib, *adjsat, *balsat, *adjefa, *balefa, *adjnfc, *balnfc, *adjpro, *balpro;
char buffer[128];

WeightCharts *our_charts;
char llabel[100], flabel[100];

static void longterm_idle_cb(void *whatever)
{
struct dirent **namelist;
int n, i, s, wdate[2000], realwdate[2000], measurementcount = 0, earliest = 1, lastdate;
int datapoints, unit;
char userfile[FL_PATH_MAX], userdir[FL_PATH_MAX], buffer[100], *xdate, cdate[9];
char unitstring[9], chartlabel[20];
float weight, bf, fat[2000], lean[2000], lastlean, lastfat, biggestval = 0.0;
float realfat[2000], realean[2000], leanavg, fatavg;
FILE *fp;
Fl::remove_idle(longterm_idle_cb, NULL);

datapoints = fatavg = leanavg = 0;
if (strlen(subuser) == 0) sprintf(userdir, "%s", nutdir);
else sprintf(userdir, "%s/%s/", nutdir, subuser);
s = n = fl_filename_list(userdir, &namelist, fl_numericsort);
if (n < 0)
 {
 perror("scandir");
 return;
 }
else
 {
 while (s--)
  {
  if (strncmp(namelist[s]->d_name, "WLOG.2", 6) == 0 || strncmp(namelist[s]->d_name, "WLOG.txt", 8) == 0)
   {
   if (strlen(subuser) != 0) sprintf(userfile,"%s%s",userdir,namelist[s]->d_name);
   else sprintf(userfile,"%s/%s%s",userdir,subuser,namelist[s]->d_name);
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
   if ((fp = fopen(userfile,"rb")) == NULL)
#else
   if ((fp = fl_fopen(userfile,"rb")) == NULL)
#endif
    {
    fclose(fp);
    continue;
    }
   while (fgets(buffer,100,fp) != NULL)
    {
    weight = atof(strtok(buffer," "));
    bf     = atof(strtok(NULL," "));
    if (weight > 0.0 && bf > 0.0)
     {
     xdate = strtok(NULL," ");
     strncpy(cdate,xdate,8);
     cdate[8] = '\0';
     wdate[measurementcount] = reverse_time_machine(cdate);
     if (wdate[measurementcount] < earliest) earliest = wdate[measurementcount];
     fat[measurementcount] = weight * bf / 100;
     lean[measurementcount] = weight - fat[measurementcount];
     measurementcount++;
     }
    if (measurementcount >= 2000) break;
    }
   fclose(fp);
   Fl::check();
   if (measurementcount >= 2000) break;
   }
  if (measurementcount >= 2000) break;
  }
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
 for (i = n; i > 0;) free((void*)(namelist[--i]));
 free((void*)namelist);
#else
 fl_filename_free_list(&namelist, n);
#endif
 } 
for (i = 0; i < 2000; i++) realwdate[i] = 1;
earliest *= -1;
datapoints = earliest + 1;
if (measurementcount < 1)
 {
 sprintf(llabel,"No Lean Mass Measurements");
 sprintf(flabel,"No Fat Mass Measurements");
 our_charts->l->label(llabel);
 our_charts->f->label(flabel);
 return;
 }
for (i = 0; i < measurementcount; i++)
 {
 realwdate[wdate[i] + earliest] = wdate[i];
 realfat[wdate[i] + earliest] = fat[i];
 realean[wdate[i] + earliest] = lean[i];
 }

lastdate = realwdate[0];
leanavg = lastlean = realean[0];
fatavg = lastfat = realfat[0];
for (i = 1; i < datapoints; i++)
 {
 if (realwdate[i] == 1)
  {
  realwdate[i] = lastdate + 1;
  realean[i] = lastlean;
  realfat[i] = lastfat;
  }
 lastdate = realwdate[i];
 lastlean = realean[i];
 lastfat = realfat[i];
 leanavg += realean[i];
 fatavg += realfat[i];
 }

leanavg /= datapoints;
fatavg /= datapoints;
for (i = 0; i < datapoints; i++)
 {
 realean[i] -= leanavg;
 realfat[i] -= fatavg; 
 if (fabs(realean[i]) > biggestval) biggestval = fabs(realean[i]); 
 if (fabs(realfat[i]) > biggestval) biggestval = fabs(realfat[i]); 
 }
our_charts->l->bounds(-biggestval, biggestval);
our_charts->f->bounds(-biggestval, biggestval);
sprintf(llabel,"Average Lean Mass Measurement was %0.1f (range shown is +/- %0.1f)",leanavg, biggestval);
sprintf(flabel,"Average Fat Mass Measurement was %0.1f (range shown is +/- %0.1f)",fatavg, biggestval);
our_charts->l->label(llabel);
our_charts->f->label(flabel);
our_charts->l->clear();
our_charts->f->clear();
if (datapoints / 365 > 0)
 {
 unit = 365;
 sprintf(unitstring,"year");
 }
else if (datapoints / 183 > 0)
 {
 unit = 183;
 sprintf(unitstring,"6 months");
 }
else if (datapoints / 30 > 0)
 {
 unit = 30;
 sprintf(unitstring,"month");
 }
else
 {
 unit = 7;
 sprintf(unitstring,"week");
 }

for (i = 0; i < datapoints; i++)
 {
 if (realwdate[i] % -unit != 0 || (realwdate[i] % -unit == 0 && realwdate[i] / -unit == 0))
  {
  our_charts->l->add(realean[i], 0, FL_RED);
  our_charts->f->add(realfat[i], 0, FL_MAGENTA);
  }
 else
  {
  if (unit != 183)
   {
   if (realwdate[i] / -unit == 1) sprintf(chartlabel,"1 %s ago",unitstring);
   else sprintf(chartlabel,"%d %ss ago",realwdate[i] / -unit,unitstring);
   }
  else sprintf(chartlabel,"6 months ago");
  our_charts->l->add(realean[i], chartlabel, FL_RED);
  our_charts->f->add(realfat[i], chartlabel, FL_MAGENTA);
  }
 }
}

static void adjcal_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (options.autocal != 2)
 {
 options.autocal = 0;
 options.wltweak = 0;
 options.wlpolarity = 0;
 }
else if (options.autocal == 2) lb->value(0);
if (lb->value() == 1)
 {
 options.abnuts[ENERC_KCAL] = -1;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 cvo->value(-1);
 fatvo->update();
 pvo->update();
 nfcvo->update();
 }
else
 {
 options.abnuts[ENERC_KCAL] = DV[ENERC_KCAL];
 cvo->value(options.abnuts[ENERC_KCAL]);
 if (!(options.abnuts[FAT] == 0 || options.abnuts[CHO_NONFIB] == 0))
  {
  options.abnuts[CHO_NONFIB] = 0;
  balnfc->value(1);
  adjnfc->value(0);
  nfcvo->update();
  }
 }
write_OPTIONS();
}

static void adjfat_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (lb->value() == 1)
 {
 balfat->value(0);
 balfat->label("DV 30% of Calories");
 adjnfc->value(0);
 balnfc->value(1);
 options.abnuts[FAT] = -1;
 options.abnuts[CHO_NONFIB] = 0;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 dv_change();
 }
else
 {
 options.abnuts[FAT] = DV[FAT];
 fatvo->value(options.abnuts[FAT]);
 fatvo->update();
 if (options.abnuts[CHO_NONFIB] != 0)
  {
  balnfc->value(1);
  adjnfc->value(0);
  options.abnuts[CHO_NONFIB] = 0;
  }
 }
write_OPTIONS();
}

static void adjpro_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (lb->value() == 1)
 {
 balpro->value(0);
 options.abnuts[PROCNT] = -1;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 pvo->value(-1);
 pvo->update();
 }
else
 {
 options.abnuts[PROCNT] = DV[PROCNT];
 pvo->value(options.abnuts[PROCNT]);
 }
write_OPTIONS();
}

static void adjnfc_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (lb->value() == 1)
 {
 balnfc->value(0);
 balfat->value(1);
 balfat->label("Balance of Calories");
 adjfat->value(0);
 options.abnuts[CHO_NONFIB] = -1;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 nfcvo->value(-1);
 nfcvo->update();
 fatvo->update();
 }
else
 {
 options.abnuts[CHO_NONFIB] = DV[CHO_NONFIB];
 nfcvo->value(options.abnuts[CHO_NONFIB]);
 }
write_OPTIONS();
}

static void adjfib_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (lb->value() == 1)
 {
 balfib->value(0);
 options.abnuts[FIBTG] = -1;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 fibvo->value(-1);
 fibvo->update();
 }
else
 {
 options.abnuts[FIBTG] = DV[FIBTG];
 fibvo->value(options.abnuts[FIBTG]);
 }
write_OPTIONS();
}

static void adjsat_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (lb->value() == 1)
 {
 balsat->value(0);
 options.abnuts[FASAT] = -1;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 satvo->value(-1);
 satvo->update();
 }
else
 {
 options.abnuts[FASAT] = DV[FASAT];
 satvo->value(options.abnuts[FASAT]);
 }
write_OPTIONS();
}

static void adjefa_cb(Fl_Widget *w, void *data)
{
int i;
Nut_LightButton *lb = (Nut_LightButton *) w;
if (lb->value() == 1)
 {
 balefa->value(0);
 options.abnuts[FAPU] = -1;
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 efavo->value(-1);
 efavo->update();
 }
else
 {
 options.abnuts[FAPU] = DV[FAPU];
 efavo->value(options.abnuts[FAPU]);
 }
write_OPTIONS();
}

static void bal_cb(Fl_Widget *w, void *data)
{
Nut_LightButton *lb = (Nut_LightButton *) w;
intptr_t nut = (intptr_t) data;
int i;
switch (nut)
 {
 case FAT :
  if (lb->value() == 1)
   {
   fatvo->bounds(0,9999);
   fatvo->value(0);
   adjfat->value(0);
   adjnfc->value(0);
   balnfc->value(0);
   options.abnuts[FAT] = 0;
   if (options.abnuts[CHO_NONFIB] == 0)
    {
    balfat->label("DV 30% of Calories");
    options.abnuts[CHO_NONFIB] = nfcvo->value();
    }
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  else
   {
   options.abnuts[FAT] = DV[FAT];
   fatvo->bounds(1,9999);
   fatvo->value(DV[FAT]);
   balnfc->value(1);
   options.abnuts[CHO_NONFIB] = 0;
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  break;
 case PROCNT :
  if (lb->value() == 1)
   {
   pvo->bounds(0,9999);
   pvo->value(0);
   adjpro->value(0);
   options.abnuts[PROCNT] = 0;
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  else
   {
   options.abnuts[PROCNT] = DV[PROCNT];
   pvo->bounds(1,9999);
   pvo->value(DV[PROCNT]);
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  break;
 case CHO_NONFIB :
  if (lb->value() == 1)
   {
   nfcvo->bounds(0,9999);
   nfcvo->value(0);
   adjnfc->value(0);
   options.abnuts[CHO_NONFIB] = 0;
   if (options.abnuts[FAT] == 0) balfat->label("DV 30% of Calories");
   if (options.abnuts[FAT] == -1 || options.abnuts[FAT] > 0) balfat->value(0);
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  else
   {
   options.abnuts[CHO_NONFIB] = DV[CHO_NONFIB];
   adjnfc->value(0);
   nfcvo->bounds(1,9999);
   nfcvo->value(DV[CHO_NONFIB]);
   balfat->label("Balance of Calories");
   if (options.abnuts[FAT] != 0) balfat->value(0);
   options.abnuts[FAT] = 0;
   fatvo->value(0);
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  break;
 case FIBTG :
  if (lb->value() == 1)
   {
   fibvo->bounds(0,9999);
   fibvo->value(0);
   adjfib->value(0);
   options.abnuts[FIBTG] = 0;
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  else
   {
   options.abnuts[FIBTG] = DV[FIBTG];
   fibvo->bounds(1,9999);
   fibvo->value(DV[FIBTG]);
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  break;
 case FASAT :
  if (lb->value() == 1)
   {
   satvo->bounds(0,9999);
   satvo->value(0);
   adjsat->value(0);
   options.abnuts[FASAT] = 0;
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  else
   {
   options.abnuts[FASAT] = DV[FASAT];
   satvo->bounds(1,9999);
   satvo->value(DV[FASAT]);
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  break;
 case FAPU :
  if (lb->value() == 1)
   {
   efavo->bounds(0,9999);
   efavo->value(0);
   adjefa->value(0);
   options.abnuts[FAPU] = 0;
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  else
   {
   options.abnuts[FAPU] = DV[FAPU];
   efavo->bounds(1,9999);
   efavo->value(DV[FAPU]);
   i = options.screen;
   options.screen = 0;
   load_foodwork(options.defanal,options.temp_meal_root);
   auto_cal();
   efa_dynamics();
   options.screen = i;
   dv_change();
   }
  break;
 }
write_OPTIONS();
}

static void autoset_cb(Fl_Widget *w, void *data)
{
Nut_LightButton *lb = (Nut_LightButton *) w;
Nut_LightButton *la = (Nut_LightButton *) data;
double value = lb->value();
if (value > 0)
 {
 options.autocal = 2;
 options.wltweak = 0;
 options.wlpolarity = 0;
 la->value(0);
 if (options.abnuts[ENERC_KCAL] <= 0) options.abnuts[ENERC_KCAL] = DV[ENERC_KCAL];
 cvo->value(options.abnuts[ENERC_KCAL]);
 }
else
 {
 options.autocal = 0;
 options.wltweak = 0;
 options.wlpolarity = 0;
 }
write_OPTIONS();
}

static void weightlog_cb(Fl_Widget *w, void *who)
{
PersonalOptions *who_we_are = (PersonalOptions *) who;
who_we_are->initializeWeightLog();
}

static void settings_cb(Fl_Widget *w, void *who)
{
PersonalOptions *who_we_are = (PersonalOptions *) who;
who_we_are->prev();
}

static void restore_global_defaults_cb(Fl_Widget *whatever, void *who)
{
restore_defaults();
PersonalOptions *us_now = (PersonalOptions *) who;
adjcal->value(0);
adjfat->value(0);
adjpro->value(0);
adjnfc->value(0);
adjfib->value(0);
adjsat->value(0);
adjefa->value(0);
balfat->value(1);
balpro->value(1);
balnfc->value(1);
balfib->value(1);
balsat->value(1);
balefa->value(1);
us_now->update();
dv_change();
}

PersonalOptions::PersonalOptions (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
int column[4] = {21*w/128, 21*w/128+w/50+w/6, 21*w/128+5*w/50+w/6+w/75+w/15, 36*w/128+w/50+w/6+w/75+w/5+3*w/50};
int row[9];
this->label("Options");
this->labeltype(FL_ENGRAVED_LABEL);
for (int i = 0; i < 9; i++) row[i] = 9*h/44+i*2*h/22;
Fl_Group *settings = new Fl_Group(x, y, w, h);
settings->color(widgetcolor);
 {
 Nut_Button *o = new Nut_Button(x+96*w/128, y+h/640+h/22, 9*w/40, h/22, "Weight Log Regression");
 o->color(fl_lighter(widgetcolor));
 o->labelfont(FL_BOLD);
 o->labelcolor(fl_darker(FL_RED));
 o->clear_visible_focus();
 o->box(FL_ROUND_UP_BOX);
 o->callback(weightlog_cb,this);
 }
 {
 Nut_Box *o = new Nut_Box(column[0], row[0], w/6, h/22, "Calories");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
cvo = new DVValueOutput(column[1], row[0], w/10, h/22, widgetcolor, ENERC_KCAL);
adjcal = new Nut_LightButton(column[2], row[0], 10*w/52, h/22, "Adjust to my meals");
adjcal->color(widgetcolor);
adjcal->labelfont(FL_BOLD);
adjcal->labelcolor(FL_YELLOW);
adjcal->selection_color(FL_RED);
adjcal->callback(adjcal_cb,(void *)ENERC_KCAL);
if (options.abnuts[ENERC_KCAL] == -1) adjcal->value(1);
autoset = new Nut_LightButton(column[3], row[0], w/10, h/22, "Auto-Set");
autoset->color(widgetcolor);
autoset->labelfont(FL_BOLD);
autoset->labelcolor(FL_YELLOW);
autoset->selection_color(FL_RED);
autoset->tooltip("Determine the optimal calorie level from my daily weight log entries that include body fat percentage and clear the weight log upon success to start new cycles of logging and calorie adjustments.  I will eat according to the calorie level shown.");
autoset->callback(autoset_cb,(void *)adjcal);                                 

 {
 Nut_Box *o = new Nut_Box(column[0], row[1], w/6, h/22, "Total Fat");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
fatvo = new DVValueOutput(column[1], row[1], w/10, h/22, widgetcolor, FAT);
adjfat = new Nut_LightButton(column[2], row[1], 10*w/52, h/22, "Adjust to my meals");
adjfat->color(widgetcolor);
adjfat->labelfont(FL_BOLD);
adjfat->labelcolor(FL_YELLOW);
adjfat->selection_color(FL_RED);
adjfat->callback(adjfat_cb,(void *)FAT);
balfat = new Nut_LightButton(column[3], row[1], 10*w/52, h/22, "DV 30% of Calories");
balfat->color(widgetcolor);
balfat->labelfont(FL_BOLD);
balfat->labelcolor(FL_YELLOW);
balfat->selection_color(FL_RED);
balfat->callback(bal_cb,(void *)FAT);
if (options.abnuts[FAT] == 0)
 {
 balfat->value(1);
 fatvo->bounds(0,9999);
 if (options.abnuts[CHO_NONFIB] > 0) balfat->label("Balance of Calories");
 }
else if (options.abnuts[FAT] == -1)
 {
 fatvo->bounds(-1,9999);
 adjfat->value(1);
 }
else fatvo->bounds(1,9999);

 {
 Nut_Box *o = new Nut_Box(column[0], row[2], w/6, h/22, "Protein");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
pvo = new DVValueOutput(column[1], row[2], w/10, h/22, widgetcolor, PROCNT);

adjpro = new Nut_LightButton(column[2], row[2], 10*w/52, h/22, "Adjust to my meals");
adjpro->color(widgetcolor);
adjpro->labelfont(FL_BOLD);
adjpro->labelcolor(FL_YELLOW);
adjpro->selection_color(FL_RED);
adjpro->callback(adjpro_cb,(void *)PROCNT);
balpro = new Nut_LightButton(column[3], row[2], 10*w/52, h/22, "DV 10% of Calories");
balpro->color(widgetcolor);
balpro->labelfont(FL_BOLD);
balpro->labelcolor(FL_YELLOW);
balpro->selection_color(FL_RED);
balpro->callback(bal_cb,(void *)PROCNT);
if (options.abnuts[PROCNT] == 0)
 {
 balpro->value(1);
 pvo->bounds(0,9999);
 }
else if (options.abnuts[PROCNT] == -1)
 {
 pvo->bounds(-1,9999);
 adjpro->value(1);
 }
else pvo->bounds(1,9999);
 {
 Nut_Box *o = new Nut_Box(column[0], row[3], w/6, h/22, "Non-Fiber Carb");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
nfcvo = new DVValueOutput(column[1], row[3], w/10, h/22, widgetcolor, CHO_NONFIB);

adjnfc = new Nut_LightButton(column[2], row[3], 10*w/52, h/22, "Adjust to my meals");
adjnfc->color(widgetcolor);
adjnfc->labelfont(FL_BOLD);
adjnfc->labelcolor(FL_YELLOW);
adjnfc->selection_color(FL_RED);
adjnfc->callback(adjnfc_cb,(void *)CHO_NONFIB);
balnfc = new Nut_LightButton(column[3], row[3], 10*w/52, h/22, "Balance of Calories");
balnfc->color(widgetcolor);
balnfc->labelfont(FL_BOLD);
balnfc->labelcolor(FL_YELLOW);
balnfc->selection_color(FL_RED);
balnfc->callback(bal_cb,(void *)CHO_NONFIB);
if (options.abnuts[CHO_NONFIB] == 0)
 {
 balnfc->value(1);
 nfcvo->bounds(0,9999);
 }
else if (options.abnuts[CHO_NONFIB] == -1)
 {
 nfcvo->bounds(-1,9999);
 adjnfc->value(1);
 }
else nfcvo->bounds(1,9999);
 {
 Nut_Box *o = new Nut_Box(column[0], row[4], w/6, h/22, "Fiber");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
fibvo = new DVValueOutput(column[1], row[4], w/10, h/22, widgetcolor, FIBTG);
adjfib = new Nut_LightButton(column[2], row[4], 10*w/52, h/22, "Adjust to my meals");
adjfib->color(widgetcolor);
adjfib->labelfont(FL_BOLD);
adjfib->labelcolor(FL_YELLOW);
adjfib->selection_color(FL_RED);
adjfib->callback(adjfib_cb,(void *)FIBTG);
balfib = new Nut_LightButton(column[3], row[4], 10*w/52, h/22, "Daily Value default");
balfib->color(widgetcolor);
balfib->labelfont(FL_BOLD);
balfib->labelcolor(FL_YELLOW);
balfib->selection_color(FL_RED);
balfib->callback(bal_cb,(void *)FIBTG);
if (options.abnuts[FIBTG] == 0)
 {
 balfib->value(1);
 fibvo->bounds(0,9999);
 }
else if (options.abnuts[FIBTG] == -1)
 {
 fibvo->bounds(-1,9999);
 adjfib->value(1);
 }
else fibvo->bounds(1,9999);

 {
 Nut_Box *o = new Nut_Box(column[0], row[5], w/6, h/22, "Saturated Fat");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
satvo = new DVValueOutput(column[1], row[5], w/10, h/22, widgetcolor, FASAT);
adjsat = new Nut_LightButton(column[2], row[5], 10*w/52, h/22, "Adjust to my meals");
adjsat->color(widgetcolor);
adjsat->labelfont(FL_BOLD);
adjsat->labelcolor(FL_YELLOW);
adjsat->selection_color(FL_RED);
adjsat->callback(adjsat_cb,(void *)FASAT);
balsat = new Nut_LightButton(column[3], row[5], 10*w/52, h/22, "DV 10% of Calories");
balsat->color(widgetcolor);
balsat->labelfont(FL_BOLD);
balsat->labelcolor(FL_YELLOW);
balsat->selection_color(FL_RED);
balsat->callback(bal_cb,(void *)FASAT);
if (options.abnuts[FASAT] == 0)
 {
 balsat->value(1);
 satvo->bounds(0,9999);
 }
else if (options.abnuts[FASAT] == -1)
 {
 satvo->bounds(-1,9999);
 adjsat->value(1);
 }
else satvo->bounds(1,9999);

 {
 Nut_Box *o = new Nut_Box(column[0], row[6], w/6, h/22, "Essential Fatty Acids");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
efavo = new DVValueOutput(column[1], row[6], w/10, h/22, widgetcolor, FAPU);
adjefa = new Nut_LightButton(column[2], row[6], 10*w/52, h/22, "Adjust to my meals");
adjefa->color(widgetcolor);
adjefa->labelfont(FL_BOLD);
adjefa->labelcolor(FL_YELLOW);
adjefa->selection_color(FL_RED);
adjefa->callback(adjefa_cb,(void *)FAPU);
balefa = new Nut_LightButton(column[3], row[6], 10*w/52, h/22, "4% of Calories");
balefa->color(widgetcolor);
balefa->labelfont(FL_BOLD);
balefa->labelcolor(FL_YELLOW);
balefa->selection_color(FL_RED);
balefa->callback(bal_cb,(void *)FAPU);
if (options.abnuts[FAPU] == 0)
 {
 balefa->value(1);
 efavo->bounds(0,9999);
 }
else if (options.abnuts[FAPU] == -1)
 {
 efavo->bounds(-1,9999);
 adjefa->value(1);
 }
else efavo->bounds(1,9999);
 {
 Nut_Box *o = new Nut_Box(column[0], row[7], w/6, h/22, "Omega-6/3 Balance");
 o->labelcolor(FL_YELLOW);
 o->labelfont(FL_BOLD);
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
obvo = new DVValueOutput(column[1], row[7], w/10, h/22, widgetcolor, -38);

 {
 Nut_Button *o = new Nut_Button(w/2-(w/6+w/48)/4-10, row[8]+h/33, w/6+w/48, h/22, "Restore All Defaults");
 o->labelfont(FL_BOLD);
 o->callback(restore_global_defaults_cb,(void *)this);
 }

settings->end(); 

weightlog = new Fl_Group(x, y, w, h);
weightlog->color(widgetcolor);
 {
 Nut_Button *o = new Nut_Button(x+96*w/128, y+h/640+h/22, 9*w/40, h/22, "Program Settings");
 o->color(fl_lighter(widgetcolor));
 o->labelfont(FL_BOLD);
 o->labelcolor(FL_YELLOW);
 o->clear_visible_focus();
 o->box(FL_ROUND_UP_BOX);
 o->callback(settings_cb,this);
 }
 weightlogwidget = new WeightLogWidget(x+w/24, y+h/640+3*h/22, w/4, 75*h/88, widgetcolor, this);
 our_charts = weightcharts = new WeightCharts(x+2*w/24+w/4, y+h/640+3*h/22, 2*w/3, 75*h/88, widgetcolor);
weightlog->end(); 

this->end();
value(settings);
Fl::add_idle(longterm_idle_cb);
update();
}

void PersonalOptions::update(void)
{
cvo->update();
fatvo->update();
pvo->update();
nfcvo->update();
fibvo->update();
satvo->update();
efavo->update();
obvo->update();
if (options.autocal == 2) autoset->value(1);
else autoset->value(0);
}

void PersonalOptions::initializeWeightLog(void)
{
read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
weightlogwidget->initialize(&n,&nfat,&weightyintercept,&fatyintercept);
weightcharts->populate(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
redraw_whole_schmeer();
next();
}

void PersonalOptions::initializeWeightLogAutoCal(void)
{
read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
float leanslope = weightslope - fatslope;
if (nfat > 1)
 {
 if (!options.wlpolarity)
  {
  if (fatslope > 0)
   {
   options.abnuts[ENERC_KCAL] -= 20;
   options.wltweak = 1;
   }
  else if (fatslope < 0 && leanslope < 0)
   {
   options.abnuts[ENERC_KCAL] += 20;
   options.wltweak = 1;
   }
  else if (fatslope < 0 && leanslope > 0 && options.wltweak)
   {
   options.wltweak = 0;
   options.wlpolarity = 1;
   float weight = 0, bf;
   char meal_date[9];
   if (options.autocal == 2)
    {
    options.lastwlw = weightyintercept;
    options.lastwlbfp = 100 * fatyintercept / weightyintercept;
    }
   if (n > 0) write_WLOG(&weight,&bf,meal_date);
   read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
   }
  }
 else
  {
  if (fatslope > 0 && leanslope > 0)
   {
   options.abnuts[ENERC_KCAL] -= 20;
   options.wltweak = 1;
   }
  else if (leanslope < 0)
   {
   options.abnuts[ENERC_KCAL] += 20;
   options.wltweak = 1;
   }
  else if (fatslope < 0 && leanslope > 0 && options.wltweak)
   {
   options.wltweak = 0;
   options.wlpolarity = 0;
   float weight = 0, bf;
   char meal_date[9];
   if (options.autocal == 2)
    {
    options.lastwlw = weightyintercept;
    options.lastwlbfp = 100 * fatyintercept / weightyintercept;
    }
   if (n > 0) write_WLOG(&weight,&bf,meal_date);
   read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
   }
  }
 auto_cal();
 write_OPTIONS();
 this->update();
 dv_change_minus_po();
 update_pcf();
 }

weightlogwidget->initialize(&n,&nfat,&weightyintercept,&fatyintercept);
weightcharts->populate(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
redraw_whole_schmeer();
next();
}

void PersonalOptions::clearWeightLog(void)
{
float weight = 0, bf;
char meal_date[9];
if (n > 0) write_WLOG(&weight,&bf,meal_date);
initializeWeightLog();
}

void PersonalOptions::accept(double w, double bodyfat)
{
float weight = (float) w, bf = (float) bodyfat;
char meal_date[9];
if (weight == 0) return;
today(meal_date);
write_WLOG(&weight,&bf,meal_date);
if (options.autocal != 2) initializeWeightLog(); 
else initializeWeightLogAutoCal();
weightcharts->wiz->value(weightcharts->chartgroup1);
weightcharts->longshort->clear();
Fl::add_idle(longterm_idle_cb);
}

/* FoodRanking.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FOODRANKING_H
#define FOODRANKING_H

#include "TheStory.h"

class TheStory;

class FoodRanking : public Fl_Group
{
public:
FoodRanking(int x, int y, int w, int h, Fl_Color widgetcolor, TheStory *story);
void populate(int nutnum, int screen);
void set_food_group(int fg);
void set_minimize(int min);
void per_100_grams(int nutnum, int screen);
void per_100_grams_dry_weight(int nutnum, int screen);
void per_100_grams_within_food_group(int nutnum, int screen);
void per_100_calories(int nutnum, int screen);
void per_serving(int nutnum, int screen);
void per_serving_minimize(int nutnum, int screen, int type);
void per_daily_recorded_meals(int nutnum, int screen);
int max_array(void);
void food_present(int foodno);
void reindex_foodbuttons(int foodnum);
bool initial_meal_add;

protected:
void resize(int x, int y, int w, int h);

private:
Nut_Scroll *ancient_scroll;
Nut_Pack *foodpack;
Fl_Menu_Button *mb;
float abacus[MAX_FOOD];
FoodButton *FoodButtonArray[MAX_FOOD];
TheStory *story;
char mbbuf[80];
};

#endif

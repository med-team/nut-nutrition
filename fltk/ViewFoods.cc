/* Class ViewFoods */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "CalorieValueOutput.h"
#include "GramValueOutput.h"
#include "OunceValueOutput.h"
#include "ServingValueOutput.h"
#include "db.h"
#include "Nut_ReturnButton.h"
#include <FL/Fl.H>
#include <string.h>
#include <stdlib.h>

void search_vf_cb(Fl_Widget *which, void *who)
{
ViewFoods *who_we_are = (ViewFoods *) who;
who_we_are->value(who_we_are->fc);
who_we_are->fc->search_focus();
who_we_are->label("View Foods");
redraw_whole_schmeer();
}

void cancel_set_unit_cb(Fl_Widget *which, void *who)
{
ViewFoods *who_we_are = (ViewFoods *) who;
who_we_are->new_serving_unit_box->value("");
who_we_are->new_serving_qty_box->value("");
who_we_are->value(who_we_are->nbw);
}

void set_unit_cb(Fl_Widget *which, void *who)
{
ViewFoods *who_we_are = (ViewFoods *) who;
who_we_are->change_default_serving();
}

ViewFoods::ViewFoods (int x, int y, int w, int h, Fl_Color widgetcolor)
: Fl_Wizard (x, y, w, h)
{
int i;
this->labeltype(FL_ENGRAVED_LABEL);
this->label("View Foods");
nbw = new NutButtonWidget(x, y, w, h, widgetcolor, &foodwork);
this->end();
fc  = new FoodChoice(x, y, w, h, widgetcolor, 0);
this->add(fc);
setservingunit = new Fl_Group(x, y, w, h);
setservingunit->color(widgetcolor);
 {
 Nut_Box *o = new Nut_Box(x,9*h/50,w,h/22,"Please enter serving unit (cup, tbsp, piece...):");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
new_serving_unit_box = new Nut_Input(2*w/9, 14*h/50, 5*w/9, h/22);
new_serving_unit_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Box *o = new Nut_Box(x,19*h/50,w,h/22,"Please enter number of serving units:");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM);
 }
new_serving_qty_box = new Nut_Input(5*w/11, 24*h/50, w/11, h/22);
new_serving_qty_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 {
 Nut_Button *o = new Nut_Button(2*w/3,33*h/56,3*w/24,h/15,"Save");
 o->color(fl_lighter(widgetcolor));
 o->labelfont(FL_BOLD);
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->callback(set_unit_cb,this);
 }
 {
 Nut_Button *o = new Nut_Button(2*w/3,38*h/56,3*w/24,h/15,"Cancel");
 o->color(fl_lighter(widgetcolor));
 o->labelfont(FL_BOLD);
 if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
 o->callback(cancel_set_unit_cb,this);
 }
setservingunit->end();
this->add(setservingunit);
for (i = 0; i < NUTRIENT_COUNT; i++) zero.nutrient[i] = 0.00000000000001;
foodwork = zero;
current_food = NULL;

Fl_Group *wizgroup;
for (i = 0; i < MAX_SCREEN; i++)
 {
 wizgroup = new Fl_Group(x, y, w, 6*h/22);
  {
  Nut_Box *o = new Nut_Box(w/44, 2*h/245+4*h/77, w/8, h/22, "Serving");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 value_output[0][i] = new ServingValueOutput(w/6-5, 2*h/245+4*h/77, w/8, h/22, this, widgetcolor);
 serving_unit[i] = new ServingMenuButton(8*w/27, 2*h/245+4*h/77, 5*w/9, h/22, this, widgetcolor);
  {
  Nut_ReturnButton *o = new Nut_ReturnButton(13*w/15, 2*h/245+4*h/77, w/8, h/22, "Back");
  o->color(fl_lighter(widgetcolor));
  if (!Fl::scheme()) o->box(FL_RSHADOW_BOX);
  o->labelfont(FL_BOLD);
  o->clear_visible_focus();
  o->callback(back_cb);
  }
  {
  Nut_Box *o = new Nut_Box(w/44, 2*h/245+8*h/77, w/8, h/22, "Grams");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 value_output[1][i] = new GramValueOutput(w/6-5, 2*h/245+8*h/77, w/8, h/22, this, widgetcolor);
  {
  Nut_Box *o = new Nut_Box(8*w/27, 2*h/245+8*h/77, 5*w/9, h/22, "Food Search:");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 nrw[i] = new NewRecipeWidget(13*w/15, 2*h/245+8*h/77, w/8, h/22, widgetcolor);
  {
  Nut_Box *o = new Nut_Box(w/44, 2*h/245+12*h/77, w/8, h/22, "Ounces");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 value_output[2][i] = new OunceValueOutput(w/6-5, 2*h/245+12*h/77, w/8, h/22, this, widgetcolor);
  {
  Nut_Button *o = new Nut_Button(8*w/27, 2*h/245+12*h/77, 5*w/9, h/22);
  o->box(FL_DOWN_BOX);
  o->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))), fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
  o->clear_visible_focus();
  o->callback(search_vf_cb,this);
  o->tooltip("");
  }
  {
  Nut_Box *o = new Nut_Box(w/44, 2*h/245+16*h/77, w/8, h/22, "Calories");
  o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
  }
 value_output[3][i] = new CalorieValueOutput(w/6-5, 2*h/245+16*h/77, w/8, h/22, this, widgetcolor);
 water[i] = new Nut_Box(w/3, 2*h/245+16*h/77, w/8, h/22, "0.0% Water");
 refuse[i] = new Nut_Box(3*w/6, 2*h/245+16*h/77, w/8, h/22, "0% Refuse");
 addtomeal[i] = new AddToMealWidget(13*w/15, 2*h/245+16*h/77, w/8, h/22, scheme_color(MEALCOLOR));
 nutintro[i] = new Nut_Box(w/44, 2*h/245+20*h/77, 95*w/96, h/22);
 nutintro[i]->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 wizgroup->end();
 nbw->wizard[i][0]->add(wizgroup);
 }
for (i = 0; i < MAX_SCREEN; i++) addtomeal[i]->off();
update_var_elements();
if (DVnotOK)
 {
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 }
nbw->update();
nbw->redraw();
Fl::flush();
}

void ViewFoods::show_food(struct food *foodptr, float thesegrams, bool replace)
{
int i , j;
this->value(nbw);
if (foodptr == NULL)
 {
 foodwork = zero;
 for (j = 0; j < MAX_SCREEN; j++)
  {
  water[j]->label("0.0% Water");
  refuse[j]->label("0% Refuse");
  addtomeal[j]->off();
  serving_unit[j]->blank_out();
  for (i = 0; i < 4; i++) value_output[i][j]->value(0);
  }
 this->label("View Foods");
 }
else 
 {
 current_food = foodptr;
 if (DVnotOK)
  {
  i = options.screen;
  options.screen = 0;
  load_foodwork(options.defanal,options.temp_meal_root);
  auto_cal();
  efa_dynamics();
  options.screen = i;
  }
 foodwork = *foodptr;
 Fl::flush();
 sprintf(waterbuf,"%1.1f%% Water",foodwork.nutrient[WATER]);
 sprintf(refusebuf,"%1d%% Refuse",foodwork.refuse);
 for (i = 0; i < 4; i++) for (int j = 0; j < MAX_SCREEN; j++) value_output[i][j]->value(thesegrams);
 for (j = 0; j < MAX_SCREEN; j++)
  {
  ((ServingValueOutput *) value_output[0][j])->set_factor(foodwork.grams / foodwork.qty);
  ((CalorieValueOutput *) value_output[3][j])->set_factor(foodwork.nutrient[ENERC_KCAL] / 100);
  serving_unit[j]->set_label(foodptr->unit);
  water[j]->label(waterbuf);
  refuse[j]->label(refusebuf);
  if (!replace) addtomeal[j]->on();
  else addtomeal[j]->replace();
  }
 for (i = 0; i < NUTRIENT_COUNT; i++) foodwork.nutrient[i] *= thesegrams / 100;
 char *bufptr = current_food_name;
 for (i = 0; i < 80; i++)
  {
  *bufptr++ = foodptr->name[i];
  if (foodptr->name[i] == '&') *bufptr++ = '&';
  if (foodptr->name[i] == '\0') break;
  }
 this->label(current_food_name);
 fill_serving_menu(foodptr);
 }
update_var_elements();
redraw_whole_schmeer();
nbw->update();
nbw->redraw();
Fl::flush();
}

void ViewFoods::new_serving_unit(float grams, float servingunit)
{
int j;
if (DVnotOK)
 {
 j = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = j;
 }
foodwork = *current_food;
serving_unit_factor = grams / servingunit;
for (j = 0; j < NUTRIENT_COUNT; j++) foodwork.nutrient[j] *= grams / 100;
for (j = 0; j < MAX_SCREEN; j++)
 {
 ((ServingValueOutput *) value_output[0][j])->set_factor(serving_unit_factor);
 serving_unit[j]->set_label();
 for (int i = 0; i < 4; i++) value_output[i][j]->value(grams);
 }
nbw->update();
nbw->redraw();
Fl::flush();
}

void ViewFoods::quantity_change(double newval)
{
int i, j;
if (current_food != NULL)
 {
 foodwork = *current_food;
 for (i = 0; i < NUTRIENT_COUNT; i++) foodwork.nutrient[i] *= newval / 100;
 for (i = 0; i < 4; i++) for (j = 0; j < MAX_SCREEN; j++) value_output[i][j]->value(newval);
 if (newval == 0) foodwork = zero;
 }
else for (i = 0; i < 4; i++) for (j = 0; j < MAX_SCREEN; j++) value_output[i][j]->value(0);
nbw->update();
nbw->redraw();
Fl::flush();
}

void ViewFoods::update(void)
{
update_var_elements();
nbw->update();
nbw->redraw();
Fl::flush();
}

void ViewFoods::change_default_serving(char *label)
{
float grams = value_output[0][0]->value();
serving_unit_factor = ((ServingValueOutput *) value_output[0][0])->get_factor();
if (grams == 0 || ( current_food->grams == grams && strcmp(label,current_food->unit) == 0 && current_food->qty == grams / serving_unit_factor )) return;
current_food->grams = grams;
current_food->qty = grams / serving_unit_factor;
strncpy(current_food->unit,label,80);
fc->modify_food_button(current_food->food_no);
write_food_db();
write_serving(current_food);
}

void ViewFoods::change_default_serving(void)
{
float grams = value_output[0][0]->value();
float qty = atof(new_serving_qty_box->value());
if (grams > 0 && qty > 0)
 {
 current_food->grams = grams;
 current_food->qty = qty;
 strncpy(current_food->unit,new_serving_unit_box->value(),80);
 write_food_db();
 write_serving(current_food);
 new_serving_unit_box->value("");
 new_serving_qty_box->value("");
 fc->modify_food_button(current_food->food_no);
 show_food(current_food, grams, 0);
 }
else
 {
 new_serving_unit_box->value("");
 new_serving_qty_box->value("");
 this->value(nbw);
 }
}

void ViewFoods::user_serving_unit(void)
{
new_serving_unit_box->take_focus();
this->value(setservingunit);
}

void ViewFoods::tab_change(void)
{
if (this->value() == fc) fc->search_box->take_focus();
if (this->value() == fc && strcmp(fc->search_box->value(),"") == 0) this->value(nbw);
}

void ViewFoods::update_var_elements(void)
{
for (int i = 0; i < MAX_SCREEN; i++)
 {
 if ( ! options.custom && i == 0 ) nutintro[i]->label("Percentages of \"Daily Values\" in this serving:");
 else if ( options.custom && i == 0 ) nutintro[i]->label("Percentages of customized DV in this serving:");
 else if ( i > 0 ) nutintro[i]->label("Nutrients in this serving:");
 }
}

struct food *ViewFoods::what_food(void)
{
return current_food;
}

float ViewFoods::how_much(void)
{
return value_output[0][0]->value();
}

void ViewFoods::new_recipe(void)
{                              
for (int i = 0; i < MAX_SCREEN; i++)
 {
 nrw[i]->off();
 addtomeal[i]->recipe();
 }
}

void ViewFoods::recipe_done(void)
{
for (int i = 0; i < MAX_SCREEN; i++)
 {
 nrw[i]->on();
 addtomeal[i]->recipe_done();
 }
}

void ViewFoods::reindex_foodbuttons(int foodnum)
{
fc->reindex_foodbuttons(foodnum);
}

/* AnalyzeMeals.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ANALYZEMEALS_H
#define ANALYZEMEALS_H

#include "NutButtonWidget.h"
#include "FoodSuggestions.h"
#include "MealCountValueOutput.h"
#include "BackToMenu.h"
#include "PrintMenus.h"

class PrintMenus;

class AnalyzeMeals : public Fl_Wizard
{
public:
AnalyzeMeals(int x, int y, int w, int h, Fl_Color widgetcolor);
int update_am(int newval);
void update_am_dv_change_only(void);
void update_var_elements(void);
void restore_defaults_gui(void);
void food_suggestions(void);
void tab_change(void);
void meal_value_output_change(int newval);
void back_to_nbw(void);
void set_specialheader(RecordMeals *rm, struct meal **rmroot);
struct meal *tell_me_meal_root(void);
struct food foodwork, blank, zero;

private:
NutButtonWidget *nbw;
FoodSuggestions *fs;
Nut_Box *here_are[MAX_SCREEN], *meal_meals[MAX_SCREEN], *meal_enum[MAX_SCREEN];
MealCountValueOutput *meal_value_output[MAX_SCREEN];
BackToMenu *btm[MAX_SCREEN];
PrintMenus *ptm[MAX_SCREEN];
struct meal **meal_ptr_origin, *meal_ptr, *mealroot;
int specialheader, max, mealcount, *defanal;
char maxbuf[128], inputbuf[128], mebuf[128];
};

#endif

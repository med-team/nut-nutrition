/* Class MissMealWidget */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include <string.h>
#ifndef __hpux
#include <stdint.h>
#endif

static MissMealWidget *mmwobj[10];
static int mmwobjcount = 0;
static char current_meal_date[9];
static char labelbuf[15][20];
static MealValueOutput *mvo;

static void missing_meal_cb(Fl_Widget *which, void *choice)
{
intptr_t meal = (intptr_t) choice;
mvo->set_value(current_meal_date,meal);
}

static Fl_Menu_Item pulldown[] = {
  {"1",   0, missing_meal_cb, (void*)1},
  {"2",   0, missing_meal_cb, (void*)2},
  {"3",   0, missing_meal_cb, (void*)3},
  {"4",   0, missing_meal_cb, (void*)4},
  {"5",   0, missing_meal_cb, (void*)5},
  {"6",   0, missing_meal_cb, (void*)6},
  {"7",   0, missing_meal_cb, (void*)7},
  {"8",   0, missing_meal_cb, (void*)8},
  {"9",   0, missing_meal_cb, (void*)9},
  {"10",  0, missing_meal_cb, (void*)10},
  {"11",  0, missing_meal_cb, (void*)11},
  {"12",  0, missing_meal_cb, (void*)12},
  {"13",  0, missing_meal_cb, (void*)13},
  {"14",  0, missing_meal_cb, (void*)14},
  {"15",  0, missing_meal_cb, (void*)15},
  {"16",  0, missing_meal_cb, (void*)16},
  {"17",  0, missing_meal_cb, (void*)17},
  {"18",  0, missing_meal_cb, (void*)18},
  {"19",  0, missing_meal_cb, (void*)19},
{0}};

MissMealWidget::MissMealWidget (int x, int y, int w, int h, Fl_Color widgetcolor, MealValueOutput *mvop) : Fl_Wizard (x, y, w, h)
{
mmwobj[mmwobjcount++] = this;
mvo = mvop;
this->box(FL_NO_BOX);
bt = new Fl_Menu_Button(x, y, w, h, "Day\'s Missing Meals");
bt->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
bt->color(fl_lighter(widgetcolor));
bt->clear_visible_focus();
bt->menu(pulldown);
bx = new Fl_Box(x, y, w, h);
this->end();
this->value(bx);
}

void MissMealWidget::update(char *curr_meal_date)
{ 
struct meal *meal_ptr = &meal_root;
int c;
int missing[20];
strcpy(current_meal_date,curr_meal_date);
for (c=0; c<20; c++) missing[c] = 0;
while (meal_ptr->next != NULL && strcmp(current_meal_date,meal_ptr->meal_date) != 0) meal_ptr = meal_ptr->next;
if (meal_ptr->next == NULL && strcmp(current_meal_date,meal_ptr->meal_date) != 0)
 {
 for (c = options.mealsperday; c >= 1; c--)
  {
  missing[0]++;
  missing[c] = 1;
  }
 }
else for (c = options.mealsperday; c >= 1; c--)
 {
 if (c != meal_ptr->meal)
  {
  missing[0]++;
  missing[c] = 1;
  }
 while (c == meal_ptr->meal && meal_ptr->next != NULL && strcmp(current_meal_date,meal_ptr->next->meal_date) == 0) meal_ptr = meal_ptr->next;
 }
if (missing[0] == 0) for (int i = 0; i < mmwobjcount; i++) mmwobj[i]->value(mmwobj[i]->bx);
else
 {
 for (c=1; c<20; c++)
  {
  if (missing[c] == 1)
   {
   sprintf(labelbuf[c-1],"%s / %d",current_meal_date,c);
   pulldown[c-1].label(labelbuf[c-1]);
   pulldown[c-1].flags = 0;
   }
  else pulldown[c-1].flags = FL_MENU_INVISIBLE;
  }
 for (int i = 0; i < mmwobjcount; i++) mmwobj[i]->value(mmwobj[i]->bt);
 }
}

void MissMealWidget::resize(int x, int y, int w, int h)
{
int i;
Fl_Wizard::resize(x, y, w, h);
bt->labelsize(FL_NORMAL_SIZE);
for (i = 0; i < 19; i++) pulldown[i].labelsize(FL_NORMAL_SIZE);
}

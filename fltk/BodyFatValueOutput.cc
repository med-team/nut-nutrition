/* Class BodyFatValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "BodyFatValueOutput.h"
#include <stdio.h>

BodyFatValueOutput::BodyFatValueOutput (int x, int y, int w, int h, Fl_Color widgetcolor) : Nut_ValueOutput (x, y, w, h)
{
this->color(fl_lighter(fl_lighter(widgetcolor)));
this->labelcolor(FL_RED);
this->align(FL_ALIGN_BOTTOM);
this->label("Body Fat Measurement");
this->tooltip("Click and drag left and right to adjust,\nmiddle button 10x, right button 100x\n0% means that you did not measure body fat");
this->step(0.1);
this->bounds(0, 50);
}

int BodyFatValueOutput::format(char *buffer)
{
return snprintf(buffer, 128, "%0.1f%%", value());
}

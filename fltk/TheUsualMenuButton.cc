/* Class TheUsualMenuButton */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "db.h"
#include <FL/Fl.H>
#include <string.h>
#ifndef __hpux
#include <stdint.h>
#endif

static TheUsualMenuButton *theusualobj[10];
static int theusualobjcount = 0;
static RecordMeals *rm;
static struct meal *theusual_ptrs[101];

void copy_meal_cb(Fl_Widget* whomever, void* whatever)
{
rm->new_customary_meal_phase_one();
}

static Fl_Menu_Item add_pulldown[100];
static Fl_Menu_Item delete_pulldown[100];
static Fl_Menu_Item main_pulldown[] = {
 {"Add to this meal", 0, 0, add_pulldown, FL_SUBMENU_POINTER},
 {"Copy this meal with new name", 0, copy_meal_cb, 0, 0},
 {"Delete", 0, 0, delete_pulldown, FL_SUBMENU_POINTER},
 {0}};

void tu_add_to_meal_cb(Fl_Widget* whomever, void* whatever)
{
intptr_t choice = (intptr_t) whatever;
rm->add_theusual(theusual_ptrs[choice]);
}

void delete_cb(Fl_Widget* whomever, void* whatever)
{
intptr_t choice = (intptr_t) whatever;
add_pulldown[choice].deactivate();
delete_pulldown[choice].deactivate();
theusualobj[theusualobjcount-1]->delete_it(theusual_ptrs[choice]);
}

TheUsualMenuButton::TheUsualMenuButton (int x, int y, int w, int h, Fl_Color widgetcolor, RecordMeals *rmeal) : Fl_Menu_Button (x, y, w, h)
{
rm = rmeal;
theusualobj[theusualobjcount++] = this;
this->label("Customary Meals");
this->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
this->color(fl_lighter(widgetcolor));
this->clear_visible_focus();
initialize_theusual();
}

void TheUsualMenuButton::initialize_theusual(void)
{
struct meal *theusual_ptr = &theusual_root;
char last_meal_date[9];
int c = 0;
last_meal_date[0] = '\0';
this->clear();
Fl::flush();
while (theusual_ptr->next != NULL && c < 100)             
 {                                
 theusual_ptr = theusual_ptr->next;
 if (strcmp(last_meal_date,theusual_ptr->meal_date) == 0) continue;
 strcpy(last_meal_date,theusual_ptr->meal_date);
 theusual_ptrs[c] = theusual_ptr;
 add_pulldown[c].add(theusual_ptr->meal_date, 0, tu_add_to_meal_cb, (void *) c, 0);
 delete_pulldown[c].add(theusual_ptr->meal_date, 0, delete_cb, (void *) c, 0);
 add_pulldown[c].label(theusual_ptr->meal_date);
 delete_pulldown[c].label(theusual_ptr->meal_date);
 add_pulldown[c].user_data((void *) c);
 delete_pulldown[c].user_data((void *) c);
 add_pulldown[c].activate();
 delete_pulldown[c++].activate();
 }
theusual_ptrs[c] = NULL;
add_pulldown[c].label(NULL);
delete_pulldown[c].label(NULL);
main_pulldown[0].flags = FL_MENU_INVISIBLE;
main_pulldown[2].flags = FL_MENU_INVISIBLE;
if (theusual_ptrs[0] != NULL)
 {
 main_pulldown[0].flags = FL_SUBMENU_POINTER;
 main_pulldown[0].user_data(add_pulldown);
 main_pulldown[2].flags = FL_SUBMENU_POINTER;
 main_pulldown[2].user_data(delete_pulldown);
 }
this->menu(main_pulldown);
}

void TheUsualMenuButton::delete_it(struct meal *theusualptr)
{
char meal_date[9];
strcpy(meal_date,theusualptr->meal_date);
struct meal *tuptr = &theusual_root;
while (tuptr->next != NULL && strcmp(tuptr->next->meal_date,meal_date) != 0) tuptr = tuptr->next;
while (tuptr->next != NULL && strcmp(tuptr->next->meal_date,meal_date) == 0) delete_theusual_with_ptr(tuptr->next);
write_theusual_db();
initialize_theusual();
}

void TheUsualMenuButton::resize(int x, int y, int w, int h)
{
int i;
Fl_Menu_Button::resize(x, y, w, h);
this->labelsize(FL_NORMAL_SIZE);   
for (i = 0; i < 100; i++) if (add_pulldown[i].label() != NULL) add_pulldown[i].labelsize(FL_NORMAL_SIZE);
for (i = 0; i < 100; i++) if (delete_pulldown[i].label() != NULL) delete_pulldown[i].labelsize(FL_NORMAL_SIZE);
for (i = 0; i < 3; i++) main_pulldown[i].labelsize(FL_NORMAL_SIZE);
}

/* Class PCF */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "db.h"
#include <FL/Fl_Tooltip.H>

static int PCF_count;
static bool need_to_save_meal_db;

void clear_out_pcfs(void)
{                               
PCF_count = 0;
}

void save_meal_db(void)
{
Fl::remove_idle((Fl_Timeout_Handler)save_meal_db);
if (!need_to_save_meal_db) return;
if (Fl::event_buttons() == 0)
 {
 write_meal_db();
 need_to_save_meal_db = 0;
 redraw_whole_schmeer();
 }
}

void pcf_cb(Fl_Widget *which, void *who)
{
PCF *who_we_are = (PCF *) who;
Nut_ToggleButton *button = (Nut_ToggleButton *) which;
if (button->value() == 1) who_we_are->turnon_tunein(button);
else who_we_are->dropout();
}

PCF::PCF (int x, int y, int w, int h, Fl_Color widgetcolor, MealFood *mealfood) : Fl_Group (x, y, w, h)
{
mf = mealfood;
p = new Nut_ToggleButton(x, y, w/10, h, "P");
p->labelfont(FL_BOLD);
p->color(fl_lighter(MEALCOLOR), FL_YELLOW);
p->callback(pcf_cb,this);
p->clear_visible_focus();
p->tooltip("Protein automatic portion control");
c = new Nut_ToggleButton(x+w/10, y, w/10, h, "C");
c->labelfont(FL_BOLD);
c->color(fl_lighter(MEALCOLOR), FL_YELLOW);
c->callback(pcf_cb,this);
c->clear_visible_focus();
c->tooltip("Non-Fiber Carb automatic portion control");
f = new Nut_ToggleButton(x+2*w/10, y, w/10, h, "F");
f->labelfont(FL_BOLD);
f->color(fl_lighter(MEALCOLOR), FL_YELLOW);
f->callback(pcf_cb,this);
f->clear_visible_focus();
f->tooltip("Total Fat automatic portion control");
t = new Nut_ToggleButton(x+3*w/10, y, w/10, h, "T");
t->labelfont(FL_BOLD);
t->color(fl_lighter(MEALCOLOR), FL_YELLOW);
t->callback(pcf_cb,this);
t->clear_visible_focus();
t->tooltip("Thiamin automatic portion control");
n = new Nut_ToggleButton(x+4*w/10, y, w/10, h, "N");
n->labelfont(FL_BOLD);
n->color(fl_lighter(MEALCOLOR), FL_YELLOW);
n->callback(pcf_cb,this);
n->clear_visible_focus();
n->tooltip("Panto. Acid automatic portion control");
e = new Nut_ToggleButton(x+5*w/10, y, w/10, h, "E");
e->labelfont(FL_BOLD);
e->color(fl_lighter(MEALCOLOR), FL_YELLOW);
e->callback(pcf_cb,this);
e->clear_visible_focus();
e->tooltip("Vitamin E automatic portion control");
l = new Nut_ToggleButton(x+6*w/10, y, w/10, h, "L");
l->labelfont(FL_BOLD);
l->color(fl_lighter(MEALCOLOR), FL_YELLOW);
l->callback(pcf_cb,this);
l->clear_visible_focus();
l->tooltip("Calcium automatic portion control");
i = new Nut_ToggleButton(x+7*w/10, y, w/10, h, "I");
i->labelfont(FL_BOLD);
i->color(fl_lighter(MEALCOLOR), FL_YELLOW);
i->callback(pcf_cb,this);
i->clear_visible_focus();
i->tooltip("Iron automatic portion control");
k = new Nut_ToggleButton(x+8*w/10, y, w/10, h, "K");
k->labelfont(FL_BOLD);
k->color(fl_lighter(MEALCOLOR), FL_YELLOW);
k->callback(pcf_cb,this);
k->clear_visible_focus();
k->tooltip("Potassium automatic portion control");
z = new Nut_ToggleButton(x+9*w/10, y, w/10, h, "Z");
z->labelfont(FL_BOLD);
z->color(fl_lighter(MEALCOLOR), FL_YELLOW);
z->callback(pcf_cb,this);
z->clear_visible_focus();
z->tooltip("Zinc automatic portion control");
this->end();
}

void PCF::turnon_tunein(Nut_ToggleButton *button)
{
MealFood *mealfood;
int nut = -1;
if (button != p) p->value(0); 
else nut = PROCNT;
if (button != c) c->value(0); 
else nut = CHO_NONFIB;
if (button != f) f->value(0); 
else nut = FAT;
if (button != t) t->value(0); 
else nut = THIA;
if (button != n) n->value(0); 
else nut = PANTAC;
if (button != e) e->value(0); 
else nut = VITE;
if (button != l) l->value(0);
else nut = CA;
if (button != i) i->value(0);
else nut = FE;
if (button != k) k->value(0);
else nut = K;
if (button != z) z->value(0);
else nut = ZN;
if (options.abnuts[nut] != -1) pcftweak(nut,mf->mfmealptr);
else 
 {
 button->value(0);
 return;
 }
PCF_count = parent()->parent()->children();
for (int j = 0; j < PCF_count; j++)
 {
 mealfood = (MealFood *) (parent()->parent()->child(j));
 if (mealfood != mf)
  {
  if (button == p) mealfood->pcf->p->value(0);
  else if (button == c) mealfood->pcf->c->value(0);
  else if (button == f) mealfood->pcf->f->value(0);
  else if (button == t) mealfood->pcf->t->value(0);
  else if (button == n) mealfood->pcf->n->value(0);
  else if (button == e) mealfood->pcf->e->value(0);
  else if (button == l) mealfood->pcf->l->value(0);
  else if (button == i) mealfood->pcf->i->value(0);
  else if (button == k) mealfood->pcf->k->value(0);
  else mealfood->pcf->z->value(0);
  if (mf->mffoodptr == mealfood->mffoodptr)
   {
   mealfood->pcf->p->value(0);
   mealfood->pcf->c->value(0);
   mealfood->pcf->f->value(0);
   mealfood->pcf->t->value(0);
   mealfood->pcf->n->value(0);
   mealfood->pcf->e->value(0);
   mealfood->pcf->l->value(0);
   mealfood->pcf->i->value(0);
   mealfood->pcf->k->value(0);
   mealfood->pcf->z->value(0);
   }
  }
 }
PCF::PCF_computation();
}

void PCF::dropout(void)
{
pcfclear(mf->mfmealptr);
PCF_count = parent()->parent()->children();
PCF_computation();
}

void PCF::gram_change(int index, double newval)
{
PCF_computation();
}

void PCF::refresh_values(void)
{
}

void PCF::PCF_computation()
{
MealFood *mealfood;
if (PCF_count == 0)
 {
 need_to_save_meal_db = 1;
 Fl::add_idle((Fl_Timeout_Handler)save_meal_db);
 return;
 }
pcfrun();
PCF_count = parent()->parent()->children();
for (int i = 0; i < PCF_count; i++)
 {
 mealfood = (MealFood *) parent()->parent()->child(i);
 mealfood->non_recursive_gram_change();
 }
need_to_save_meal_db = 1;
Fl::add_idle((Fl_Timeout_Handler)save_meal_db);
update_analysis();
}

/* WeightCharts.h */

/*
    WEIGHT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WEIGHTCHARTS_H
#define WEIGHTCHARTS_H

#include "Nut_Box.h"
#include "Nut_Button.h"
#include "Nut_LineChart.h"
#include <FL/Fl_Group.H>
#include <FL/Fl_Chart.H>
#include <FL/Fl_Wizard.H>

class WeightCharts : public Fl_Group
{
public:
WeightCharts(int x, int y, int w, int h, Fl_Color widgetcolor);
void populate(int *n, int *nfat, float *weightslope, float *fatslope, float *weightyintercept, float *fatyintercept, float *earliestrdate);

Nut_Button *longshort;

Fl_Wizard *wiz;
Fl_Group *chartgroup1, *chartgroup2;
Nut_LineChart *l, *f;

protected:
void resize(int x, int y, int w, int h);

private:
Fl_Chart *n;
Nut_Box *bw;
char bwlabel[400], nlabel[80];
char ttip[160];
};

#endif

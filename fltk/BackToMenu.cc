/* Class BackToMenu */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include <FL/Fl.H>

void back_to_menu_cb(Fl_Widget *which, void *who)
{
RecordMeals *rm = (RecordMeals *) who;
rm->back_to_menu();
}

BackToMenu::BackToMenu (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
this->box(FL_NO_BOX);
n = new Nut_Button(x, y, w, h, "Menu");
n->color(fl_lighter(MEALCOLOR));
if (!Fl::scheme()) n->box(FL_RSHADOW_BOX);
n->labelfont(FL_BOLD);
n->clear_visible_focus();
f = new Fl_Box(x, y, w, h);
this->end();
}

void BackToMenu::on(RecordMeals *rmptr)
{
this->value(n);
n->callback(back_to_menu_cb,rmptr);
}

void BackToMenu::off(void)
{
this->value(f);
}

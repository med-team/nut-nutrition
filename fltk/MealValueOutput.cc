/* Class MealValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include "recmeal.h"
#include <string.h>
#include <FL/Fl.H>

static MealValueOutput *mvoobj[10];
static int mvoobjcount = 0;
static bool need_to_notify_of_meal_change;
static bool allow_real_time_value_meaning;

void meal_change_cb(Fl_Widget *which, void *who)
{
RecordMeals *rm = (RecordMeals *) who;
MealValueOutput *who_we_are = (MealValueOutput *) which;
need_to_notify_of_meal_change = 1;
allow_real_time_value_meaning = 1;
while (Fl::event_buttons() != 0) Fl::check();
if (need_to_notify_of_meal_change)
 {
 need_to_notify_of_meal_change = 0;
 for (int j = 0; j < mvoobjcount; j++) if (mvoobj[j] != who_we_are) mvoobj[j]->set_value(who_we_are->value());
 rm->meal_change();
 }
}

MealValueOutput::MealValueOutput (int x, int y, int w, int h, Fl_Color widgetcolor, RecordMeals *rm) : Nut_ValueOutput (x, y, w, h)
{
recmeal = rm;
this->color(fl_lighter(widgetcolor));
this->textfont(FL_BOLD);
this->step(0.5);
this->bounds(-100000,100000);
mvoobj[mvoobjcount++] = this;
this->callback(meal_change_cb,rm);
allow_real_time_value_meaning = 1;
initial_set();
}

void MealValueOutput::initial_set(void)
{
char buffer[128];
int minutes, meal;
today_plus(meal_date, &minutes);
meal = 1 + (minutes - 360) / (960 / options.mealsperday);
if (meal < 1) meal = 1;
if (meal > options.mealsperday) meal = options.mealsperday;
if (meal_root.next != NULL)
 {
 if (strcmp(meal_root.next->meal_date,meal_date) > 0) meal = (options.mealsperday * reverse_time_machine(meal_root.next->meal_date) + meal_root.next->meal + 1);
 else if (strcmp(meal_root.next->meal_date,meal_date) == 0 && meal_root.next->meal >= meal) meal = meal_root.next->meal + 1;
 }
this->value(meal-1);
format(buffer);
}

int MealValueOutput::format(char *buffer)
{
if (allow_real_time_value_meaning)
 {
 int v = (int) value();
 sprintf(current_meal_date,"%+d",v < 0 ? (v-options.mealsperday+1) / options.mealsperday : v / options.mealsperday);
 time_machine(current_meal_date);
 current_meal = v < 0 ? 1 + (options.mealsperday + v % options.mealsperday) % options.mealsperday : 1 + v % options.mealsperday;
 allow_real_time_value_meaning = 0;
 }
sprintf(buffer,"%s / %d",current_meal_date, current_meal);
return 0;
}

void MealValueOutput::set_value(double newvald)
{
int newval = (int) newvald;
value(newvald);
sprintf(current_meal_date,"%+d",newval < 0 ? (newval-options.mealsperday+1) / options.mealsperday : newval / options.mealsperday);
time_machine(current_meal_date);
current_meal = newval < 0 ? 1 + (options.mealsperday + newval % options.mealsperday) % options.mealsperday : 1 + newval % options.mealsperday;
allow_real_time_value_meaning = 0;
}

void MealValueOutput::set_value(char *curr_meal_date, int curr_meal)
{
this->set_value(-1 + reverse_time_machine(curr_meal_date) * options.mealsperday + curr_meal);
meal_change_cb(this, recmeal);
}

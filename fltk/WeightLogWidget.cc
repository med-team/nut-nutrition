/* Class WeightLogWidget */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "PersonalOptions.h"
#include "recmeal.h"
#include "options.h"
#include <string.h>

static void accept_cb(Fl_Widget *w, void *who)
{
w->deactivate();
WeightLogWidget *who_we_are = (WeightLogWidget *) who;
who_we_are->accept();
}

static void clear_cb(Fl_Widget *w, void *who)
{
WeightLogWidget *who_we_are = (WeightLogWidget *) who;
who_we_are->clear();
}

WeightLogWidget::WeightLogWidget (int x, int y, int w, int h, Fl_Color widgetcolor, PersonalOptions *pothis) : Fl_Group (x, y, w, h)
{
po = pothis;
this->color(widgetcolor);
wvo = new Nut_ValueOutput(x,y,w,h/20);
wvo->color(fl_lighter(fl_lighter(widgetcolor)));
wvo->labelcolor(FL_RED);
wvo->align(FL_ALIGN_BOTTOM);
wvo->label("Weight Measurement");
wvo->step(0.1);
wvo->bounds(0, 999.9);
bfvo = new BodyFatValueOutput(x,y+(4*h/5-y)/3+h/18,w,h/20,widgetcolor);
mb = new Nut_Button(x,y+2*(4*h/5-y)/3+2*h/18,w,h/20,"Accept New Measurements");
mb->clear_visible_focus();
mb->callback(accept_cb,this);
cwb = new Nut_Button(x,4*h/5+3*h/18,w,h/20,"Clear Weight Log");
cwb->clear_visible_focus();
cwb->callback(clear_cb,this);
cwb->tooltip("When a nutrition strategy changes, clear the weight log to start a new cycle of logging.");

this->end();
}

void WeightLogWidget::initialize(int *n, int *nfat, float *weightyintercept, float *fatyintercept)
{
char meal_date[9];
today(meal_date);
if (*n > 1) wvo->value(*weightyintercept);
else if (*n == 1) wvo->value(options.lastwlw);
else wvo->value(0);
if (*nfat > 1) bfvo->value(100 * *fatyintercept / *weightyintercept);
else if (*nfat == 1) bfvo->value(options.lastwlbfp);
else bfvo->value(0);
if (*n != 0 && strcmp(meal_date,options.lastwldate) == 0)
 {
 wvo->deactivate();
 bfvo->deactivate();
 mb->deactivate();
 wvo->tooltip("Today's measurements have been entered.");
 bfvo->tooltip("Today's measurements have been entered.");
 mb->tooltip("Today's measurements have been entered.");
 wvo->labelcolor(FL_RED);
 bfvo->labelcolor(FL_RED);
 }
else
 {
 wvo->activate();
 bfvo->activate();
 mb->activate();
 wvo->tooltip("Click and drag left and right to adjust,\nmiddle button 10x, right button 100x");
 bfvo->tooltip("Click and drag left and right to adjust,\nmiddle button 10x, right button 100x\n0% means that you did not measure body fat");
 mb->tooltip("");
 wvo->labelcolor(FL_BLACK);
 bfvo->labelcolor(FL_BLACK);
 }

if (*n < 2)
 {
 cwb->deactivate();
 cwb->tooltip("Weight log has been cleared.");
 }

else if (options.autocal == 2)
 {
 cwb->deactivate();
 cwb->tooltip("Weight log will be cleared automatically by Calorie Auto-Set.");
 }


else
 {
 cwb->activate();
 cwb->tooltip("When a nutrition strategy changes, clear the weight log to start a new cycle of logging with the changed strategy.");
 }
 
}

void WeightLogWidget::accept(void)
{
po->accept(wvo->value(),bfvo->value());
}

void WeightLogWidget::clear(void)
{
po->clearWeightLog();
}

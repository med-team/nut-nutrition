/* Class MealCountValueOutput */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"

static void meal_count_change_cb (Fl_Widget *which, void *who)
{
AnalyzeMeals *amptr = (AnalyzeMeals *)who;
amptr->meal_value_output_change(((Fl_Valuator*)which)->value());
}

MealCountValueOutput::MealCountValueOutput (int x, int y, int w, int h, AnalyzeMeals *am, Fl_Color widgetcolor) : Nut_ValueOutput (x, y, w, h)
{
this->color(fl_lighter(widgetcolor));
this->step(1.0);
this->callback((Fl_Callback*)meal_count_change_cb,am);
}

/* NutButtonWidget.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NUTBUTTONWIDGET_H
#define NUTBUTTONWIDGET_H

#include <FL/Fl_Tabs.H>
#include "NutValue.h"
#include "NutButton.h"

class NutButton;

class NutButtonWidget : public Fl_Tabs
{
public:
NutButtonWidget(int x, int y, int w, int h, Fl_Color widgetcolor, struct food *foodwork);
void add_me_to_the_update_list(NutValue *nv);
void update(void);
Fl_Wizard *wizard[MAX_SCREEN][2];

protected:
void resize(int x, int y, int w, int h);

private:
NutValue *update_list[250];
int update_count;
NutButton *calorie_button;
char calbuf[20];
};

#endif

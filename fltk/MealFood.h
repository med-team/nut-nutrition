/* MealFood.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MEALFOOD_H
#define MEALFOOD_H

#include "FoodButton.h"
#include "MealFoodValueOutput.h"
#include "PCF.h"

class PCF;

class MealFood : public Fl_Group
{
public:
MealFood(int x, int y, int w, int h, struct food *foodptr, struct meal *mealptr, float grams, Fl_Color widgetcolor);
void gram_change(double);
void non_recursive_gram_change(void);
void fill_PCF_matrix(int, float *, int *, MealFood **);
struct food *mffoodptr;
struct meal *mfmealptr;
FoodButton *mffoodbutton;
MealFoodValueOutput *mfvo;
PCF *pcf;
int PCF_index;

private:
bool this_is_a_recipe;
};

#endif

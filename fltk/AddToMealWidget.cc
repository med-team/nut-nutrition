/* Class AddToMealWidget */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Nut.h"
#include <FL/Fl.H>

AddToMealWidget::AddToMealWidget (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Wizard (x, y, w, h)
{
this->box(FL_NO_BOX);
n = new Nut_Button(x, y, w, h, "Add to Meal");
n->color(fl_lighter(MEALCOLOR));
if (!Fl::scheme()) n->box(FL_RSHADOW_BOX);
n->labelfont(FL_BOLD);
n->clear_visible_focus();
n->callback(add_to_meal_cb);
r = new Nut_Button(x, y, w, h, "Replace");
r->color(fl_lighter(MEALCOLOR));
if (!Fl::scheme()) r->box(FL_RSHADOW_BOX);
r->labelfont(FL_BOLD);
r->clear_visible_focus();
r->callback(add_to_meal_cb);
p = new Nut_Button(x, y, w, h, "Add to Recipe");
p->color(fl_lighter(RECIPECOLOR));
if (!Fl::scheme()) p->box(FL_RSHADOW_BOX);
p->labelfont(FL_BOLD);
p->clear_visible_focus();
p->callback(add_to_meal_cb);
f = new Fl_Box(x, y, w, h);
this->end();
buttonvalue = &n;
}

void AddToMealWidget::on(void)
{
if (buttonvalue == &r) buttonvalue = &n;
this->value(*buttonvalue);
}

void AddToMealWidget::off(void)
{
this->value(f);
}

void AddToMealWidget::replace(void)
{
if (get_recipe_mode()) r->color(fl_lighter(RECIPECOLOR));
else r->color(fl_lighter(MEALCOLOR));
buttonvalue = &r;
this->value(*buttonvalue);
}

void AddToMealWidget::recipe(void)
{
buttonvalue = &p;
if (this->value() != f) this->value(*buttonvalue);
}

void AddToMealWidget::recipe_done(void)
{
buttonvalue = &n;
if (this->value() != f) this->value(*buttonvalue);
}

/* Class WeightCharts */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "WeightCharts.h"
#include "Nut.h"

void longshort_cb(Fl_Widget *which, void *who)
{
Nut_Button *me = (Nut_Button *) which;
WeightCharts * us = (WeightCharts *) who;
if (me->value())
 {
 me->clear();
 us->wiz->prev();
 }
else
 {
 me->value(1);
 us->wiz->next();
 }
}

WeightCharts::WeightCharts (int x, int y, int w, int h, Fl_Color widgetcolor) : Fl_Group (x, y, w, h)
{
this->color(widgetcolor);

bw = new Nut_Box(x,y,w,8*FL_NORMAL_SIZE);
wiz = new Fl_Wizard(x,y+8*FL_NORMAL_SIZE,w,h-9*FL_NORMAL_SIZE);
chartgroup1 = new Fl_Group(x,y+8*FL_NORMAL_SIZE,w,h-9*FL_NORMAL_SIZE);
n = new Fl_Chart(x,y+8*FL_NORMAL_SIZE,w,h-9*FL_NORMAL_SIZE);
chartgroup1->end();
chartgroup2 = new Fl_Group(x,y+8*FL_NORMAL_SIZE,w,h-9*FL_NORMAL_SIZE);
l = new Nut_LineChart(x,y+8*FL_NORMAL_SIZE,w,(h-11*FL_NORMAL_SIZE)/2);
f = new Nut_LineChart(x,y+8*FL_NORMAL_SIZE+(h-15*FL_NORMAL_SIZE/2)/2,w,(h-11*FL_NORMAL_SIZE)/2);
l->box(FL_UP_BOX);
l->color(widgetcolor);
l->labelfont(FL_BOLD);
l->align(FL_ALIGN_BOTTOM);
f->box(FL_UP_BOX);
f->color(widgetcolor);
f->labelfont(FL_BOLD);
f->align(FL_ALIGN_BOTTOM);

chartgroup2->end();
wiz->end();
wiz->box(FL_NO_BOX);
chartgroup1->color(widgetcolor);
chartgroup2->color(widgetcolor);
longshort = new Nut_Button(x+17*w/22, y+h/14, w/5, h/9, "Long Term");
longshort->box(FL_DIAMOND_UP_BOX);
longshort->labelcolor(fl_darker(fl_darker(widgetcolor)));
longshort->clear_visible_focus();
longshort->callback(longshort_cb,this);
longshort->value(0);

bw->labelcolor(FL_BLACK);

n->type(FL_FILL_CHART);
n->box(FL_UP_BOX);
n->color(widgetcolor);
n->autosize(1);
n->labelfont(FL_BOLD);
n->align(FL_ALIGN_BOTTOM);

this->end();
}

void WeightCharts::populate(int *nweight, int *nfat, float *weightslope, float *fatslope, float *weightyintercept, float *fatyintercept, float *earliestrdate)
{
n->clear();

float leanslope = *weightslope - *fatslope;
float leanyintercept = *weightyintercept - *fatyintercept;
sprintf(nlabel,"Lean Mass and Fat Mass Linear Regressions Graph");
sprintf(ttip,"Graph will appear after 2 daily weight measurements that include body fat percentage");

if (*nfat > 1)
 {
 sprintf(bwlabel,"Based on the trend of %d data points so far...\nPredicted daily weight change:  %+0.3f\nPredicted daily lean mass change:  %+0.3f\nPredicted daily fat mass change:  %+0.3f\nPredicted lean mass today:  %0.1f\nPredicted fat mass today:  %0.1f  (%0.1f%%)",*nweight,*weightslope,leanslope,*fatslope,leanyintercept,*fatyintercept,100 * *fatyintercept / *weightyintercept); 

 sprintf(nlabel,"Lean Mass and Fat Mass Linear Regressions for past %0.0f days",-(*earliestrdate-1));

 if (options.autocal == 2)
  {
  if (leanslope > 0 && *fatslope > 0) sprintf(ttip,"Gained %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.  Calorie Auto-Set Mode = Cutting.",-(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",-(*earliestrdate * *fatslope));
  else if (leanslope > 0 && *fatslope < 0) sprintf(ttip,"Gained %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.  Calorie Auto-Set Mode = Bulking.",-(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",(*earliestrdate * *fatslope));
  else if (leanslope < 0 && *fatslope < 0) sprintf(ttip,"Lost %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.  Calorie Auto-Set Mode = Bulking.",(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",(*earliestrdate * *fatslope));
  else if (leanslope < 0 && *fatslope > 0 && !options.wlpolarity) sprintf(ttip,"Lost %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.  Calorie Auto-Set Mode = Cutting!",(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",-(*earliestrdate * *fatslope));
  else if (leanslope < 0 && *fatslope > 0 && options.wlpolarity) sprintf(ttip,"Lost %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.  Calorie Auto-Set Mode = Bulking!",(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",-(*earliestrdate * *fatslope));
  else ttip[0] = '\0';
  }
 else
  {
  if (leanslope > 0 && *fatslope > 0) sprintf(ttip,"Gained %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.",-(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",-(*earliestrdate * *fatslope));
  else if (leanslope > 0 && *fatslope < 0) sprintf(ttip,"Gained %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.",-(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",(*earliestrdate * *fatslope));
  else if (leanslope < 0 && *fatslope < 0) sprintf(ttip,"Lost %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.",(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",(*earliestrdate * *fatslope));
  else if (leanslope < 0 && *fatslope > 0) sprintf(ttip,"Lost %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.",(*earliestrdate * leanslope),-(*earliestrdate), -(*earliestrdate) == 1 ? "day" : "days",-(*earliestrdate * *fatslope));
  else ttip[0] = '\0';
  }
 }

else if (*nweight > 1) sprintf(bwlabel,"Based on the trend of %d data points so far...\nPredicted daily weight change:  %+0.3f\nPredicted daily lean mass change:  ?\nPredicted daily fat mass change:  ?\nPredicted lean mass today:  ?\nPredicted fat mass today:  ?",*nweight,*weightslope); 

else sprintf(bwlabel,"Based on the trend of %d data %s so far...\nPredicted daily weight change:  ?\nPredicted daily lean mass change:  ?\nPredicted daily fat mass change:  ?\nPredicted lean mass today:  ?\nPredicted fat mass today:  ?",*nweight, *nweight == 1 ? "point" : "points"); 

bw->label(bwlabel);
n->label(nlabel);
chartgroup1->tooltip(ttip);

if (*nfat < 2) return;

n->add(0, 0, FL_RED);
for (int i = (int) *earliestrdate; i < 1; i++)
 {
 n->add(i * leanslope + leanyintercept, 0, FL_RED);
 n->add(i * leanslope + leanyintercept, 0, FL_RED);
 n->add(-(i * *fatslope + *fatyintercept), 0, FL_MAGENTA);
 n->add(-(i * *fatslope + *fatyintercept), 0, FL_MAGENTA);
 n->add(-(i * *fatslope + *fatyintercept), 0, FL_RED);
 }

}

void WeightCharts::resize(int x, int y, int w, int h)
{
n->labelsize(FL_NORMAL_SIZE);
Fl_Group::resize(x, y, w, h);
}

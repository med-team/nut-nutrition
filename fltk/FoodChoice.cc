/* Class FoodChoice */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "FoodChoice.h"
#include <FL/Fl.H>
#include <string.h>
#include <ctype.h>

static FoodChoice *fcobj[10];
static int fcobjcount = 0;

void sync_food_buttons(int foodno, FoodChoice *fc)
{
for (int i = 0; i < fcobjcount; i++) if (fcobj[i] != fc) fcobj[i]->modify_food_button_non_recursive(foodno);
}

static char tokenbuf[20][15];
static char abtokenbuf[20][15];
static bool tokenchange[20], its_changing;
static int tokencount;

static void search_cb(Fl_Widget *which, void *who)
{
char buf[160], *token;
int i, j, length, count = 0;
Nut_Input *input = (Nut_Input *) which;
FoodChoice *who_we_are = (FoodChoice *) who;
its_changing = 0;
strcpy(buf,input->value());
length = strlen(buf);
for (i = 0; i < length; i++) buf[i] = toupper(buf[i]);
token = strtok(buf, ", ");
if (token == NULL)
 {
 tokenbuf[0][0] = '\0';
 abtokenbuf[0][0] = '\0';
 tokenchange[0] = 1;
 }
if (token != NULL && strcmp(token,tokenbuf[count]) != 0)
 {
 strcpy(tokenbuf[count],token);
 tokenchange[count] = 1;
 }
count++;
while (token != NULL)
 {
 token = strtok(NULL,", ");
 if (token == NULL) break;
 if (strcmp(token,tokenbuf[count]) != 0)
  {
  strcpy(tokenbuf[count],token);
  tokenchange[count] = 1;
  }
 if (tokenchange[count-1] == 1) tokenchange[count] = 1;
 count++;
 }
tokencount = count;
tokenbuf[tokencount][0] = '\0';
abtokenbuf[tokencount][0] = '\0';
for (i = 0; i < tokencount; i++)
 {
 if (!tokenchange[i]) continue;
 for (j = 0; j < Abbrev_Count; j++) if (strcmp(tokenbuf[i],Abbreviations[j]) == 0) break;
 if (j < Abbrev_Count) strcpy(abtokenbuf[i],Abbreviations[j % 2 == 0 ? j+1 : j-1]);
 else abtokenbuf[i][0] = '\0';
 }
while (Fl::get_key(FL_BackSpace) && strcmp("",input->value()) != 0)
 {
 its_changing = 1;
 }
if (its_changing)
 {
 its_changing = 0;
 search_cb(which, who);
 return;
 }
else who_we_are->search();
}

static void immediate_add_to_meal_cb(Fl_Widget *w, void *data)
{
FoodButton *who_we_are = (FoodButton *) w;
struct food *foodptr = (struct food *) data;
skip_show_food(foodptr, who_we_are->grams());
}

FoodChoice::FoodChoice (int x, int y, int w, int h, Fl_Color widgetcolor, bool sheader) : Fl_Group (x, y, w, h)
{
int i;
specialheader = sheader;
fcobj[fcobjcount++] = this;
this->color(widgetcolor);
 {
 Nut_Box *o = new Nut_Box(8*w/27, 2*h/245+8*h/77, 5*w/9, h/22, "Food Search:");
 o->align(FL_ALIGN_INSIDE|FL_ALIGN_LEFT);
 }
 {
 search_box = new Nut_Input(8*w/27, 2*h/245+12*h/77, 5*w/9, h/22);
 search_box->color(fl_lighter(fl_lighter(fl_lighter(widgetcolor))));
 search_box->when(FL_WHEN_CHANGED|FL_WHEN_NOT_CHANGED);
 search_box->callback(search_cb,this);
 }
 {
 ancient_scroll = new Nut_Scroll(x,2*h/245+12*h/77+h/22,2*w/3,18*h/22);
 ancient_scroll->type(Fl_Scroll::VERTICAL);
 ancient_scroll->color(scheme_color(widgetcolor));
 foodpack = new Nut_Pack(x,2*h/245+12*h/77+h/22,17*w/27,20*h/23);
 foodpack->end();
 ancient_scroll->end();
 }
 {
 cancel_button = new CancelWidget(x+5*w/6,y+2*h/3,3*w/24,h/15,widgetcolor);
 cancel_button->off();
 }
for (i = 0; i < FoodCount; i++)
 {
 FoodButtonArray[i] = new FoodButton(x,y,17*w/27,h/23, FoodIndex[i]);
 FoodButtonArray[i]->grams(FoodIndex[i]->grams);
 if (specialheader) FoodButtonArray[i]->callback(immediate_add_to_meal_cb,FoodIndex[i]);
 foodpack->add(FoodButtonArray[i]);
 foodpack->remove(FoodButtonArray[i]);
 }
this->end();
for (i = 0; i < FoodCount; i++) foodsieve[i] = -1;
}

void FoodChoice::search_focus(void)
{
for (int j = 0; j < FoodCount; j++) foodpack->remove(FoodButtonArray[j]);
search_box->value("");
search_box->take_focus();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
for (int i = 0; i < FoodCount; i++) foodsieve[i] = -1;
}

void FoodChoice::search(void)
{
int i, j;
char search1[20], search2[20];

for (i = 0; i <= tokencount; i++)
 {
 if (!tokenchange[i]) continue;
 tokenchange[i] = 0;
 strcpy(search1,tokenbuf[i]);
 strcpy(search2,abtokenbuf[i]);
 for (j = 0; j < FoodCount; j++) if (foodsieve[j] > i-1) foodsieve[j] = i-1;

 if (search1[0] == '\0' && tokencount == 1)
  {
  for (j = 0; j < FoodCount; j++) foodsieve[j] = -1;
  food_present(-1);
  foodpack->redraw();
  ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
  ancient_scroll->position(0,0);
#else
  ancient_scroll->scroll_to(0,0);
#endif
  Fl::flush();
  return;
  }

 if (search2[0] == '\0')
  {
  for (j = 0; j < FoodCount; j++)
   {
   if (foodsieve[j] < i-1 || strstr(FoodIndex[j]->name,search1) == NULL) continue;
   foodsieve[j] = i;
   }
  }
 if (search2[0] != '\0')
  {
  for (j = 0; j < FoodCount; j++)
   {
   if (foodsieve[j] < i-1 || (strstr(FoodIndex[j]->name,search1) == NULL && strstr(FoodIndex[j]->name,search2) == NULL)) continue;
   foodsieve[j] = i;
   }
  }
 }
food_present(-1);
for (i = 0; i < FoodCount; i++) if (foodsieve[i] >= tokencount-1) food_present(i);
foodpack->redraw();
ancient_scroll->redraw();
#if FL_MAJOR_VERSION == 1 && FL_MINOR_VERSION == 1
ancient_scroll->position(0,0);
#else
ancient_scroll->scroll_to(0,0);
#endif
}

void FoodChoice::food_present(int foodno)
{
static int count = 0;
static FoodButton *array[600];

if (foodno == -1)
 {
 for (int i = 0; i < count; i++) foodpack->remove(array[i]);
 count = 0;
 }

if (foodno > -1 && count < 600)
 {
 foodpack->add(FoodButtonArray[foodno]);
 array[count] = FoodButtonArray[foodno];
 count++;
 }
}

void FoodChoice::modify_food_button(int foodno)
{
FoodButtonArray[foodno]->grams(FoodIndex[foodno]->grams);
sync_food_buttons(foodno, this);
}

void FoodChoice::modify_food_button_non_recursive(int foodno)
{
FoodButtonArray[foodno]->grams(FoodIndex[foodno]->grams);
}

void FoodChoice::reindex_foodbuttons(int foodnum)
{
for (int i = FoodCount; i > foodnum; i--) FoodButtonArray[i] = FoodButtonArray[i-1];
FoodButtonArray[foodnum] = new FoodButton(x(),y(),17*w()/27,h()/23, FoodIndex[foodnum]);
FoodButtonArray[foodnum]->grams(FoodIndex[foodnum]->grams);
if (specialheader) FoodButtonArray[foodnum]->callback(immediate_add_to_meal_cb,FoodIndex[foodnum]);
foodpack->add(FoodButtonArray[foodnum]);
foodpack->remove(FoodButtonArray[foodnum]);
}

void FoodChoice::resize(int x, int y, int w, int h)
{
Fl_Group::resize(x, y, w, h);
for (int i = 0; i < FoodCount; i++) FoodButtonArray[i]->resize(x,y,17*w/27,h/23);
}

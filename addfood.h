/* addfood.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ADDFOOD_H
#define ADDFOOD_H


#ifdef __cplusplus
extern "C" {
#endif

void add_foods(int);
void new_recipe_show(void);
void get_recipe_qty(float *result);
int recipe_show(int, int);
void add_labels(void);
void screen_subvert_label(int);
void set_nut_table(void);
void label_nut_list(int);
void get_nut_value(float *);
void guess_recipe();
void solve_it(int, int *, int *, int *);
void modify_servings(void);

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif

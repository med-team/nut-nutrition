/* menu.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menu.h"
#include "db.h"
#include "util.h"
#include "recmeal.h"
#include "anameal.h"
#include "remmeal.h"
#include "viewfood.h"
#include "addfood.h"
#include "ranking.h"
#include "prtmenu.h"
#include "theusual.h"
#include "trendy.h"

void menu()
{
int menu_choice;
for ( ; ; )
 {
 header("NUT:  Main Menu");
 printf("                  1  --  Record Meals\n\n");
 printf("                  2  --  Analyze Meals and Food Suggestions\n\n");
 printf("                  3  --  Delete Meals and Set Meals Per Day\n\n");
 printf("                  4  --  View Foods\n\n");
 printf("                  5  --  Add Foods and Modify Serving Sizes\n\n");
 printf("                  6  --  View Nutrients and Rank Foods\n\n");
 printf("                  7  --  Set Personal Options and Log Weight\n\n");
 printf("                  8  --  Plot Daily and Monthly Trends\n\n");
 printf("                  9  --  Record \"The Usual\"--Customary Meals\n\n");
 printf("                  P  --  Print Menus from Meal Database\n\n");
 spacer(19);
 printf("Enter your choice (\"Q\" to quit):  ");
 menu_choice = get_char();
 switch (menu_choice)
  {
  case 'Q' :
#ifndef DOS
  case 'q' : printf("\033[2JNUT has ended.\n\n"); 
#else
  case 'q' : printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nNUT has ended.\n");
#endif
             return;
             break;
  case '1' : record_meals();
             break; 
  case '2' : analyze_meals(&meal_root,0);
             break; 
  case '3' : remove_meals();
             break; 
  case '4' : view_foods();
             break; 
  case '5' : add_foods_menu();
             break; 
  case '6' : view_nuts_menu();
             break; 
  case '7' : personal_menu();
             break; 
  case '8' : screen_subvert_trendy();
             break; 
  case '9' : theusual();
             break; 
  case 'P' : 
  case 'p' : print_menus(1);
             break; 
  default  : break;
  }
 }
}

void view_nuts_menu()
{
int menu_choice;

if (DVnotOK)
 {
 menu_choice = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = menu_choice;
 }

for ( ; ; )
 {
 header("NUT:  View Nutrients and Rank Foods");
 printf("\n\n                          1  --  Per 100 Grams\n\n");
 printf("                          2  --  Per 100 Grams Dry Weight\n\n");
 printf("                          3  --  Per 100 Grams within Food Group ?\n\n");
 printf("                          4  --  Per 100 Calories\n\n");
 printf("                          5  --  Per Serving\n\n");
 printf("                          6  --  Per Serving, Minimize ?\n\n");
 printf("                          7  --  Per Daily Recorded Meals\n\n");
 spacer(16);
 printf("\nEnter your choice (just <enter> to quit):  ");
 menu_choice = get_char();
 Minut = -1;
 Fdgrp = -1;
 switch (menu_choice)
  {
  case '1' : screen_subvert_rank(1);
             break; 
  case '2' : screen_subvert_rank(3);
             break; 
  case '3' : screen_subvert_rank(6);
             break; 
  case '4' : screen_subvert_rank(0);
             break; 
  case '5' : screen_subvert_rank(4);
             break; 
  case '6' : screen_subvert_rank(5);
             break; 
  case '7' : screen_subvert_rank(2);
             break; 
  default  : return;
  }
 }
}

void add_foods_menu()
{
int menu_choice;

if (DVnotOK)
 {
 menu_choice = options.screen;
 options.screen = 0;          
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = menu_choice;
 }

header("NUT:  Add Foods and Modify Serving Sizes");
printf("\n\n                           1  --  Add a Recipe\n\n");
printf("                           2  --  Add a Labeled Food\n\n");
printf("                           3  --  Modify Serving Sizes\n\n");
spacer(8);
printf("\nEnter your choice (just <enter> to quit):  ");
menu_choice = get_char();
switch (menu_choice)
 {
 case '1' : add_foods(0);
            break; 
 case '2' : add_foods(1);
            break; 
 case '3' : modify_servings();
            break; 
 default  : return;
 }
}

void screen_subvert_rank(int choice)
{
int screen_choice, i;
view_nuts(choice);
for ( ; ; )
 {
 header("NUT:  View Nutrients and Rank Foods");
 printf("\n      Change group of nutrients to view?\n\n\n");
 for (i = 0; i < MAX_SCREEN; i++) printf("                    %d  --  %s\n\n",i+1,ScreenTitle[i]);
 spacer(MAX_SCREEN*2+4);
 printf("\nEnter your choice (just <enter> to quit):  ");
 screen_choice = get_int();
 if (screen_choice < 1 || screen_choice > MAX_SCREEN) return;
 options.screen = screen_choice - 1;
 write_OPTIONS();
 view_nuts(choice);
 }
}

void screen_subvert_trendy()
{
int screen_choice, i;
char mode = 'd';

if (DVnotOK)
 {
 i = options.screen;
 options.screen = 0;
 load_foodwork(options.defanal,options.temp_meal_root);
 auto_cal();
 efa_dynamics();
 options.screen = i;
 }

mode = trendy(mode, -1);
if (mode == 'X') return;
for ( ; ; )
 {
 if (mode == 'd') header("NUT:  Plot Daily Trends");
 if (mode != 'd') header("NUT:  Plot Monthly Trends");
 printf("\n      Change group of nutrients to view?\n\n\n");
 for (i = 0; i < MAX_SCREEN; i++) printf("                    %d  --  %s\n\n",i+1,ScreenTitle[i]);
 spacer(MAX_SCREEN*2+4);
 printf("\nEnter your choice (just <enter> to quit):  ");
 screen_choice = get_int();
 if (screen_choice < 1 || screen_choice > MAX_SCREEN) return;
 options.screen = screen_choice - 1;
 write_OPTIONS();
 mode = trendy(mode, -1);
 }
}

void personal_menu()
{
int menu_choice;
options.comparison = 0;
 
menu_choice = options.screen;
options.screen = 0;          
load_foodwork(options.defanal,options.temp_meal_root);
auto_cal();
efa_dynamics();
options.screen = menu_choice;

for ( ; ; )
 {
 header("NUT:  Set Personal Options");
 printf("            1  --  Calories              ");
 current_settings(1);
 printf("            2  --  Total Fat             ");
 current_settings(2);
 printf("            3  --  Protein               ");
 current_settings(3);
 printf("            4  --  Non-Fiber Carb        ");
 current_settings(4);
 printf("            5  --  Fiber                 ");
 current_settings(5);
 printf("            6  --  Saturated Fat         ");
 current_settings(6);
 printf("            7  --  Essential Fatty Acids ");
 current_settings(7);
 printf("            8  --  Omega-6/3 Balance     ");
 current_settings(8);
 printf("            0  --  Restore All Defaults\n\n");
 printf("            W  --  Weight Log Regression\n\n");
 spacer(20);
 printf("\nEnter your choice (just <enter> to quit):  ");
 menu_choice = get_char();
 switch (menu_choice)
  {
  case '1' : set_calories();
             break; 
  case '2' : set_fat();
             break; 
  case '3' : set_protein();
             break; 
  case '4' : set_carb();
             break; 
  case '5' : set_fiber();
             break; 
  case '6' : set_sat();
             break; 
  case '7' : set_efa();
             break; 
  case '8' : set_n6();
             break; 
  case '0' : restore_defaults();
             break; 
  case 'w' :
  case 'W' : weight_log();
             break;
  default  : return;
  }
 }
}

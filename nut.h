/* nut.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#define PROCNT 0
#define FAT 1
#define CHOCDF 2
#define ASH 3
#define ENERC_KCAL 4
#define STARCH 5
#define SUCS 6
#define GLUS 7
#define FRUS 8
#define LACS 9
#define MALS 10
#define ALC 11
#define WATER 12
#define ADPROT 13
#define CAFFN 14
#define THEBRN 15
#define ENERC_KJ 16
#define SUGAR 17
#define GALS 18
#define FIBTG 19
#define CA 20
#define FE 21
#define MG 22
#define P 23
#define K 24
#define NA 25
#define ZN 26
#define CU 27
#define FLD 28
#define MN 29
#define SE 30
#define VITA_IU 31
#define RETOL 32
#define VITA_RAE 33
#define CARTB 34
#define CARTA 35
#define TOCPHA 36
#define VITD 37
#define ERGCAL 38
#define CHOCAL 39
#define VITD_BOTH 40
#define CRYPX 41
#define LYCPN 42
#define LUT_ZEA 43
#define TOCPHB 44
#define TOCPHG 45
#define TOCPHD 46
#define TOCTRA 47
#define TOCTRB 48
#define TOCTRG 49
#define TOCTRD 50
#define VITC 51
#define THIA 52
#define RIBF 53
#define NIA 54
#define PANTAC 55
#define VITB6A 56
#define FOL 57
#define VITB12 58
#define CHOLN 59
#define MK4 60
#define VITK1D 61
#define VITK1 62
#define FOLAC 63
#define FOLFD 64
#define FOLDFE 65
#define BETN 66
#define TRP_G 67
#define THR_G 68
#define ILE_G 69
#define LEU_G 70
#define LYS_G 71
#define MET_G 72
#define CYS_G 73
#define PHE_G 74
#define TYR_G 75
#define VAL_G 76
#define ARG_G 77
#define HISTN_G 78
#define ALA_G 79
#define ASP_G 80
#define GLU_G 81
#define GLY_G 82
#define PRO_G 83
#define SER_G 84
#define HYP 85
#define VITE_ADDED 86
#define VITB12_ADDED 87
#define CHOLE 88
#define FATRN 89
#define FASAT 90
#define F4D0 91
#define F6D0 92
#define F8D0 93
#define F10D0 94
#define F12D0 95
#define F14D0 96
#define F16D0 97
#define F18D0 98
#define F20D0 99
#define F18D1 100
#define F18D2 101
#define F18D3 102
#define F20D4 103
#define F22D6 104
#define F22D0 105
#define F14D1 106
#define F16D1 107
#define F18D4 108
#define F20D1 109
#define F20D5 110
#define F22D1 111
#define F22D5 112
#define PHYSTR 113
#define STID7 114
#define CAMD5 115
#define SITSTR 116
#define FAMS 117
#define FAPU 118
#define F15D0 119
#define F17D0 120
#define F24D0 121
#define F16D1T 122
#define F18D1T 123
#define F22D1T 124
#define F18D2T 125
#define F18D2I 126
#define F18D2TT 127
#define F18D2CLA 128
#define F24D1C 129
#define F20D2CN6 130
#define F16D1C 131
#define F18D1C 132
#define F18D2CN6 133
#define F22D1C 134
#define F18D3CN6 135
#define F17D1 136
#define F20D3 137
#define FATRNM 138
#define FATRNP 139
#define F13D0 140
#define F15D1 141
#define F18D3CN3 142
#define F20D3N3 143
#define F20D3N6 144
#define F20D4N6 145
#define F18D3I 146
#define F21D5 147
#define F22D4 148
#define F18D1TN7 149
#define PROT_KCAL 150
#define FAT_KCAL 151
#define CHO_KCAL 152
#define CHO_NONFIB 153
#define LA 154
#define AA 155
#define ALA 156
#define EPA 157
#define DHA 158
#define OMEGA6 159
#define SHORT6 160
#define LONG6 161
#define OMEGA3 162
#define SHORT3 163
#define LONG3 164
#define VITE 165

#define NUTRIENT_COUNT 166
#define DERIVED 16
#define NUTRNO_ARRAYSIZE 860

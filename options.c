/* options.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "options.h"
#include "util.h"
#include "db.h"
#include "recmeal.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct opt options;

float DefaultTarget = 50, DefaultEPADHA = 1.0;

void initialize_options()
{
int c;
options.delopt = -1;
options.defanal = 0;
options.defanalanal = 0;
options.defanalrec = 0;
options.screen = 0;
options.custom = 0;
options.next_recipe = 99000;
for (c=0; c < NUTRIENT_COUNT; c++) options.abnuts[c] = 0;
options.mealsperday = 3;
options.grams = 0;
options.autocal = 0;
options.comparison = 0;
options.temp_meal_root = &meal_root;
options.lastwlw = 0;
options.lastwlbfp = 0;
strncpy(options.lastwldate,"00000000",9);
options.wltweak = 0;
options.wlpolarity = 0;
}

void current_settings(int setting)
{
switch (setting)
 {
 case 1 : if (options.abnuts[ENERC_KCAL] >= 0)
           {
           if (options.autocal == 2) printf("(AUTO-SET:  %0.1f kcal)\n\n",DV[ENERC_KCAL]);
           else if (DV[ENERC_KCAL] != 2000) printf("(SET:  %0.1f kcal)\n\n",DV[ENERC_KCAL]);
           else printf("(SET:  \"Daily Value 2000.0 kcal\")\n\n");
           }
          else if (options.abnuts[ENERC_KCAL] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else printf("(SET:  \"Daily Value 2000.0 kcal\")\n\n");
          break;

 case 2 : if (options.abnuts[FAT] > 0) printf("(SET:  %0.1f grams)\n\n",DV[FAT]);
          else if (options.abnuts[FAT] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else 
           {
           if (options.abnuts[CHO_NONFIB] == 0) printf("(SET:  \"Daily Value 30%% of Calories\")\n\n");
           else printf("(SET:  \"Balance of Calories\")\n\n");
           }
          break;

 case 3 : if (options.abnuts[PROCNT] > 0) printf("(SET:  %0.1f grams)\n\n",DV[PROCNT]);
          else if (options.abnuts[PROCNT] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else printf("(SET:  \"Daily Value 10%% of Calories\")\n\n");
          break;

 case 4 : if (options.abnuts[CHO_NONFIB] > 0) printf("(SET:  %0.1f grams)\n\n",DV[CHO_NONFIB]);
          else if (options.abnuts[CHO_NONFIB] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else printf("(SET:  \"Balance of Calories\")\n\n");
          break;

 case 5 : if (options.abnuts[FIBTG] > 0) printf("(SET:  %0.1f grams)\n\n",DV[FIBTG]);
          else if (options.abnuts[FIBTG] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else printf("(SET:  \"Daily Value %0.1f grams\")\n\n",DV[FIBTG]);
          break;

 case 6 : if (options.abnuts[FASAT] > 0) printf("(SET:  %0.1f grams)\n\n",DV[FASAT]);
          else if (options.abnuts[FASAT] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else printf("(SET:  \"Daily Value 10%% of Calories\")\n\n");
          break;

 case 7 : if (options.abnuts[FAPU] > 0) printf("(SET:  %0.1f grams)\n\n",DV[FAPU]);
          else if (options.abnuts[FAPU] < 0) printf("(SET:  \"Adjust to my meals\")\n\n");
          else printf("(SET:  \"Default 4%% of Calories\")\n\n");
          break;

 case 8 : if (options.n6hufa > 0) printf("(SET:  %2.0f/%2.0f)\n\n",options.n6hufa,100-options.n6hufa);
          else printf("(SET:  \"Default 50/50\")\n\n");
          break;

 default : break;
 }
}

void set_calories(void)
{
float buf = 0;
int choice;
header("NUT:  Set Calories");
printf("\n      Currently  ");
current_settings(1);

if (!options.autocal && options.abnuts[ENERC_KCAL] >= 0)
 {
 printf("\n  1.  I will type a new calorie level.\n");
 printf("\n  2.  Automatically adjust the calorie level to my meals.\n");
 printf("\n  3.  Determine the optimal calorie level from my daily weight log entries\n");
 printf("      that include body fat percentage and clear the weight log upon success\n");
 printf("      to start new cycles of logging and calorie adjustments.  I will eat\n");
 printf("      according to the calorie level shown.\n");
 spacer(12);
 }

if (options.abnuts[ENERC_KCAL] < 0)
 {
 printf("\n  1.  I will type a new calorie level.\n");
 printf("\n  2.  Determine the optimal calorie level from my daily weight log entries\n");
 printf("      that include body fat percentage and clear the weight log upon success\n");
 printf("      to start new cycles of logging and calorie adjustments.  I will eat\n");
 printf("      according to the calorie level shown.\n");
 spacer(10);
 }

if (options.autocal && options.abnuts[ENERC_KCAL] >= 0)
 {
 printf("\n  1.  I will type a new calorie level.\n");
 printf("\n  2.  Automatically adjust the calorie level to my meals.\n");
 printf("\n  3.  Turn off the calorie auto-set feature.\n");
 spacer(9);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
if (options.abnuts[ENERC_KCAL] >= 0) switch (choice)
 {
 case 1 :
  header("NUT:  Set Personal Calorie Level");
  spacer(0);
  printf("\nType new calorie level (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[ENERC_KCAL] = buf;
  if (options.abnuts[ENERC_KCAL] < 0) options.abnuts[ENERC_KCAL] = 0;
  break;
 case 2 :
  options.abnuts[ENERC_KCAL] = -1;
  options.autocal = 0;
  options.wltweak = 0;
  options.wlpolarity = 0;
  break;
 case 3 :
  if (options.autocal)
   {
   options.autocal = 0;
   options.wltweak = 0;
   options.wlpolarity = 0;
   }
  else 
   {
   options.autocal = 2;
   if (options.abnuts[ENERC_KCAL] <= 0) options.abnuts[ENERC_KCAL] = DV[ENERC_KCAL];
   }
  break;
 default :
  break;
 }

else switch (choice)
 {
 case 1 :
  header("NUT:  Set Personal Calorie Level");
  spacer(0);
  printf("\nType new calorie level (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[ENERC_KCAL] = buf;
  break;
 case 2 :
  options.autocal = 2;
  options.abnuts[ENERC_KCAL] = food_work.nutrient[ENERC_KCAL];
  break;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Calories");
new_nut_levels();
write_OPTIONS();
}
   
void set_fat(void)
{
float buf = 0;
int choice;
header("NUT:  Set Total Fat");
printf("\n      Currently  ");
current_settings(2);

if (options.abnuts[FAT] >= 0)
 {
 printf("\n  1.  I will type a new fat level in grams.\n");
 printf("\n  2.  Automatically adjust the required fat level to my meals.\n");
 if (options.abnuts[CHO_NONFIB] != 0) printf("\n  3.  Return to the Daily Value 30%% of calories.\n\n");
 else printf("\n  3.  Make the fat level be the balance of calories after other requirements\n      are met.\n");
 spacer(10);
 }

if (options.abnuts[FAT] < 0)
 {
 printf("\n  1.  I will type a new fat level in grams.\n");
 if (options.abnuts[CHO_NONFIB] != 0) printf("\n  2.  Make the fat level be the balance of calories after other requirements\n      are met.\n");
 else printf("\n  2.  Return to the Daily Value 30%% of calories.\n");
 spacer(9);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
switch (choice)
 {
 case 1 :
  header("NUT:  Set Total Fat");
  spacer(0);
  printf("\nType new fat level in grams (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[FAT] = buf;
  if (options.abnuts[FAT] < 0) options.abnuts[FAT] = 0;
  options.abnuts[CHO_NONFIB] = 0;
  break;
 case 2 :
  if (options.abnuts[FAT] >= 0)
   {
   options.abnuts[FAT] = -1;
   if (options.abnuts[CHO_NONFIB] != -1) options.abnuts[CHO_NONFIB] = 0;
   }
  else options.abnuts[FAT] = 0;
  break;
 case 3 :
  options.abnuts[FAT] = 0;
  if (options.abnuts[CHO_NONFIB] != 0) options.abnuts[CHO_NONFIB] = 0;
  else options.abnuts[CHO_NONFIB] = -1;
  break;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Total Fat");
new_nut_levels();
write_OPTIONS();
}
   
void set_protein(void)
{
float buf = 0;
int choice;
header("NUT:  Set Protein");
printf("\n      Currently  ");
current_settings(3);

if (options.abnuts[PROCNT] >= 0)
 {
 printf("\n  1.  I will type a new protein level in grams.\n");
 printf("\n  2.  Automatically adjust the required protein level to my meals.\n");
 if (options.abnuts[PROCNT] > 0)
  {
  printf("\n  3.  Return to the Daily Value 10%% of calories.\n\n");
  spacer(10);
  }
 else spacer(7);
 }

if (options.abnuts[PROCNT] < 0)
 {
 printf("\n  1.  I will type a new protein level in grams.\n");
 printf("\n  2.  Return to the Daily Value 10%% of calories.\n");
 spacer(7);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
switch (choice)
 {
 case 1 :
  header("NUT:  Set Protein");
  spacer(0);
  printf("\nType new protein level in grams (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[PROCNT] = buf;
  if (options.abnuts[PROCNT] < 0) options.abnuts[PROCNT] = 0;
  break;
 case 2 :
  if (options.abnuts[PROCNT] >= 0) options.abnuts[PROCNT] = -1;
  else options.abnuts[PROCNT] = 0;
  break;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Protein");
new_nut_levels();
write_OPTIONS();
}
   
void set_carb(void)
{
float buf = 0;
int choice;
header("NUT:  Set Non-Fiber Carb");
printf("\n      Currently  ");
current_settings(4);

if (options.abnuts[CHO_NONFIB] >= 0)
 {
 printf("\n  1.  I will type a new non-fiber carb level in grams.\n");
 printf("\n  2.  Automatically adjust the required non-fiber carb level to my meals.\n");
 if (options.abnuts[CHO_NONFIB] != 0)
  {
  printf("\n  3.  Make the non-fiber carb level be the balance of calories after other\n      requirements are met.\n");
  spacer(10);
  }
 else spacer(7);
 }

if (options.abnuts[CHO_NONFIB] < 0)
 {
 printf("\n  1.  I will type a new non-fiber carb level in grams.\n");
 printf("\n  2.  Make the non-fiber carb level be the balance of calories after other\n      requirements are met.\n");
 spacer(8);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
switch (choice)
 {
 case 1 :
  header("NUT:  Set Non-Fiber Carb");
  spacer(0);
  printf("\nType new non-fiber carb level in grams (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[CHO_NONFIB] = buf;
  if (options.abnuts[CHO_NONFIB] < 0) options.abnuts[CHO_NONFIB] = 0;
  if (options.abnuts[CHO_NONFIB] > 0) options.abnuts[FAT] = 0;
  break;
 case 2 :
  if (options.abnuts[CHO_NONFIB] == -1) options.abnuts[CHO_NONFIB] = 0;
  else
   {
   options.abnuts[CHO_NONFIB] = -1;
   options.abnuts[FAT] = 0;
   }
  break;
 case 3 :
  options.abnuts[CHO_NONFIB] = 0;
  break;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Non-Fiber Carb");
new_nut_levels();
write_OPTIONS();
}
   
void set_fiber(void)
{
float buf = 0;
int choice;
header("NUT:  Set Fiber");
printf("\n      Currently  ");
current_settings(5);

if (options.abnuts[FIBTG] >= 0)
 {
 printf("\n  1.  I will type a new fiber level in grams.\n");
 printf("\n  2.  Automatically adjust the required fiber level to my meals.\n");
 if (options.abnuts[FIBTG] > 0)
  {
  printf("\n  3.  Return to the Daily Value %0.1f grams.\n\n",25 * DV[ENERC_KCAL] / 2000);
  spacer(10);
  }
 else spacer(8);
 }

if (options.abnuts[FIBTG] < 0)
 {
 printf("\n  1.  I will type a new fiber level in grams.\n");
 printf("\n  2.  Return to the Daily Value %0.1f grams.\n\n",25 * DV[ENERC_KCAL] / 2000);
 spacer(8);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
switch (choice)
 {
 case 1 :
  header("NUT:  Set Fiber");
  spacer(0);
  printf("\nType new fiber level in grams (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[FIBTG] = buf;
  if (options.abnuts[FIBTG] < 0) options.abnuts[FIBTG] = 0;
  break;
 case 2 :
  if (options.abnuts[FIBTG] >= 0) options.abnuts[FIBTG] = -1;
  else options.abnuts[FIBTG] = 0;
  break;
 case 3 :
  options.abnuts[FIBTG] = 0;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Fiber");
new_nut_levels();
write_OPTIONS();
}
   
void set_sat(void)
{
float buf = 0;
int choice;
header("NUT:  Set Saturated Fat");
printf("\n      Currently  ");
current_settings(6);

if (options.abnuts[FASAT] >= 0)
 {
 printf("\n  1.  I will type a new saturated fat level in grams.\n");
 printf("\n  2.  Automatically adjust the required saturated fat level to my meals.\n");
 if (options.abnuts[FASAT] > 0)
  {
  printf("\n  3.  Return to the Daily Value 10%% of calories.\n\n");
  spacer(10);
  }
 else spacer(7);
 }

if (options.abnuts[FASAT] < 0)
 {
 printf("\n  1.  I will type a new saturated fat level in grams.\n");
 printf("\n  2.  Return to the Daily Value 10%% of calories.\n\n");
 spacer(8);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
switch (choice)
 {
 case 1 :
  header("NUT:  Set Saturated Fat");
  spacer(0);
  printf("\nType new saturated fat level in grams (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[FASAT] = buf;
  if (options.abnuts[FASAT] < 0) options.abnuts[FASAT] = 0;
  break;
 case 2 :
  if (options.abnuts[FASAT] >= 0) options.abnuts[FASAT] = -1;
  else options.abnuts[FASAT] = 0;
  break;
 case 3 :
  options.abnuts[FASAT] = 0;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Saturated Fat");
new_nut_levels();
write_OPTIONS();
}
   
void set_efa(void)
{
float buf = 0;
int choice;
header("NUT:  Set Essential Fatty Acids");
printf("\n      Currently  ");
current_settings(7);

if (options.abnuts[FAPU] >= 0)
 {
 printf("\n  1.  I will type a new essential fatty acid level in grams.\n");
 printf("\n  2.  Automatically adjust the required essential fatty acid level to my meals.\n");
 if (options.abnuts[FAPU] > 0)
  {
  printf("\n  3.  Return to the default 4%% of calories.\n\n");
  spacer(10);
  }
 else spacer(7);
 }

if (options.abnuts[FAPU] < 0)
 {
 printf("\n  1.  I will type a new essential fatty acid level in grams.\n");
 printf("\n  2.  Return to the default 4%% of calories.\n\n");
 spacer(8);
 }

printf("\nEnter your choice (just <enter> to retain current setting):  ");
choice = get_int();
switch (choice)
 {
 case 1 :
  header("NUT:  Set Essential Fatty Acids");
  spacer(0);
  printf("\nType new essential fatty acid level in grams (just <enter> for default):  ");
  get_cals(&buf);
  options.abnuts[FAPU] = buf;
  if (options.abnuts[FAPU] < 0) options.abnuts[FAPU] = 0;
  break;
 case 2 :
  if (options.abnuts[FAPU] >= 0) options.abnuts[FAPU] = -1;
  else options.abnuts[FAPU] = 0;
  break;
 case 3 :
  options.abnuts[FAPU] = 0;
 default :
  break;
 }
auto_cal();
efa_dynamics();
header("NUT:  Set Essential Fatty Acids");
new_nut_levels();
write_OPTIONS();
}

void set_n6(void)
{
float buf = 0;
header("NUT:  Set Omega-6/3 Balance");
printf("\n      Currently  ");
current_settings(8);
spacer(3);

printf("\nType new Omega-6 target (16-89) or just <enter> for default):  ");
get_cals(&buf);
options.n6hufa = buf;
if (options.n6hufa < 0) options.n6hufa = 0;
else if (options.n6hufa > 89) options.n6hufa = 89;
else if (options.n6hufa > 0 && options.n6hufa < 16) options.n6hufa = 16;
auto_cal();
efa_dynamics();
header("NUT:  Set Omega-6/3 Balance");
new_nut_levels();
write_OPTIONS();
}

void screen(void)
{
options.screen++;
options.screen %= MAX_SCREEN;
write_OPTIONS();
}

void screen_previous(void)
{
    if (--options.screen < 0) {
        options.screen = MAX_SCREEN - 1;
    }
    write_OPTIONS();
}

void get_cals(float *cals)
{
char buff[128];
fgets(buff,128,stdin);
*cals = (float) atof(buff);
}

void auto_cal()
{
int i;
float factor = 0, alccals = 0;
float pratio = 4, fratio = 9, cratio = 4, faratio = 0.94615385;
if (options.comparison) return;
for (i = 1; i <= DV_COUNT; i++) DV[DVMap[i]] = DVBase[DVMap[i]];
if (options.abnuts[ENERC_KCAL] > 0)
 {
 factor = options.abnuts[ENERC_KCAL] / DVBase[ENERC_KCAL];
 DV[ENERC_KCAL] = options.abnuts[ENERC_KCAL];
 DV[PROCNT] *= factor;
 DV[FAT] *= factor;
 DV[FASAT] *= factor;
 DV[FAMS] *= factor;
 DV[FAPU] *= factor;
 DV[CHO_NONFIB] *= factor;
 DV[FIBTG] *= factor;
 DV[OMEGA6] *= factor;
 DV[LA] *= factor;
 DV[AA] *= factor;
 DV[OMEGA3] *= factor;
 DV[ALA] *= factor;
 DV[EPA] *= factor;
 DV[DHA] *= factor;
 }
else if (options.abnuts[ENERC_KCAL] == -1 && food_work.nutrient[ENERC_KCAL] > 0)
 {
 factor = food_work.nutrient[ENERC_KCAL] / DVBase[ENERC_KCAL];
 DV[ENERC_KCAL] = food_work.nutrient[ENERC_KCAL];
 DV[PROCNT] *= factor;
 DV[FAT] *= factor;
 DV[FASAT] *= factor;
 DV[FAMS] *= factor;
 DV[FAPU] *= factor;
 DV[CHO_NONFIB] *= factor;
 DV[FIBTG] *= factor;
 DV[OMEGA6] *= factor;
 DV[LA] *= factor;
 DV[AA] *= factor;
 DV[OMEGA3] *= factor;
 DV[ALA] *= factor;
 DV[EPA] *= factor;
 DV[DHA] *= factor;
 }

for (i = 1; i <= DV_COUNT; i++)
 {
 if (options.abnuts[DVMap[i]] > 0) DV[DVMap[i]] = options.abnuts[DVMap[i]];
 else if (options.abnuts[DVMap[i]] < 0 && food_work.nutrient[DVMap[i]] > 0) DV[DVMap[i]] = food_work.nutrient[DVMap[i]];
 }

if (food_work.nutrient[PROCNT] > 0 && food_work.nutrient[PROT_KCAL] > 0) pratio = food_work.nutrient[PROT_KCAL] / food_work.nutrient[PROCNT];

if (food_work.nutrient[FAT] > 0 && food_work.nutrient[FAT_KCAL] > 0) fratio = food_work.nutrient[FAT_KCAL] / food_work.nutrient[FAT];

if (food_work.nutrient[CHOCDF] > 0 && food_work.nutrient[CHO_KCAL] > 0) cratio = food_work.nutrient[CHO_KCAL] / food_work.nutrient[CHOCDF];

if (food_work.nutrient[ALC] > 0) alccals = food_work.nutrient[ALC] * ALC_CAL_FACTOR;

if (options.abnuts[PROCNT] == 0 && DV[ENERC_KCAL] != 2000.0) DV[PROCNT] = 0.1 * DV[ENERC_KCAL] / pratio;

if (options.abnuts[FAT] == 0 && options.abnuts[CHO_NONFIB] != 0) DV[FAT] = (DV[ENERC_KCAL] - (pratio * DV[PROCNT]) - (cratio * (DV[FIBTG] + DV[CHO_NONFIB])) - alccals) / fratio;
else DV[CHO_NONFIB] = ((DV[ENERC_KCAL] - (pratio * DV[PROCNT]) - (fratio * DV[FAT]) - alccals) / cratio) - DV[FIBTG];

if (options.abnuts[FAT] == 0 && options.abnuts[CHO_NONFIB] == 0)
 {
 DV[FAT] = DV[ENERC_KCAL] * 0.3 / fratio;
 DV[CHO_NONFIB] = (((DV[ENERC_KCAL] - (DV[FAT] * fratio) - (DV[PROCNT] * pratio)) / cratio) - DV[FIBTG]);
 }

DV[CHOCDF] = DV[CHO_NONFIB] + DV[FIBTG];

if (food_work.nutrient[FAT] > 0 && food_work.nutrient[FASAT] > 0 && food_work.nutrient[FAMS] > 0 && food_work.nutrient[FAPU] > 0) faratio = (food_work.nutrient[FASAT] + food_work.nutrient[FAMS] + food_work.nutrient[FAPU]) / food_work.nutrient[FAT];

DV[FAMS] = (DV[FAT] * faratio) - DV[FASAT] - DV[FAPU];

if (options.abnuts[CHO_NONFIB] != 0 || options.abnuts[FAT] != 0 || options.abnuts[PROCNT] != 0 || options.abnuts[FIBTG] != 0 || options.abnuts[FASAT] != 0) options.custom = 1;
else options.custom = 0;
}

void auto_del()
{
char buf[128];
int junk;
header("NUT:  Automatic Deletion of Meals");
spacer(0);
printf("\nDo you want to have meals deleted automatically from database?  (y/n):  ");
junk = get_char();
if (junk != 'Y' && junk != 'y' && junk != 'N' && junk != 'n') return;
if (junk != 'Y' && junk != 'y')
 {
 header("NUT:  Automatic Deletion of Meals");
 spacer(0);
 options.delopt = -1;
 write_OPTIONS();
 printf("\nMeals will not be deleted automatically.  Press <enter> to continue...");
 junk = get_int();
 return;
 }
junk = 0;
header("NUT:  Automatic Deletion of Meals");
spacer(0);
printf("\nHow many meals should be kept in database?  "); 
while (junk < 1)
 {
 get_string(buf,127);
 junk = atoi(buf);
 if (junk > 0)
  {
  options.delopt = junk;
  write_OPTIONS();
  delete_meals(options.delopt);
  write_meal_db();
  header("NUT:  Automatic Deletion of Meals");
  spacer(0);
  if (junk == 1) printf("\n%d meal will be kept in database.  Press <enter> to continue...",junk); 
  else printf("\n%d meals will be kept in database.  Press <enter> to continue...",junk); 
  junk = get_int();
  return;
  }
 header("NUT:  Automatic Deletion of Meals");
 spacer(0);
 printf("\nMust keep at least one meal in database.  How many?  ");
 }
}

void new_nut_levels()
{
int junk;
printf("\n  Current Nutrient Levels:    %-15s %6.1f %-s\n",Nutrient[ENERC_KCAL],DV[ENERC_KCAL],Unit[ENERC_KCAL]);
printf("                              %-15s %6.1f %-s\n",Nutrient[FAT],DV[FAT],Unit[FAT]);
printf("                              %-15s %6.1f %-s\n",Nutrient[FASAT],DV[FASAT],Unit[FASAT]);
printf("                              %-15s %6.1f %-s\n",Nutrient[FAMS],DV[FAMS],Unit[FAMS]);
printf("                              %-15s %6.1f %-s\n",Nutrient[FAPU],DV[FAPU],Unit[FAPU]);
printf("                              %-15s %6.1f %-s\n",Nutrient[OMEGA6],DV[OMEGA6],Unit[OMEGA6]);
printf("                              %-15s %6.1f %-s\n",Nutrient[LA],DV[LA],Unit[LA]);
printf("                              %-15s %6.1f %-s\n",Nutrient[AA],DV[AA],Unit[AA]);
printf("                              %-15s %6.1f %-s\n",Nutrient[OMEGA3],DV[OMEGA3],Unit[OMEGA3]);
printf("                              %-15s %6.1f %-s\n",Nutrient[ALA],DV[ALA],Unit[ALA]);
printf("                              %-15s %6.1f %-s\n",Nutrient[EPA],DV[EPA],Unit[EPA]);
printf("                              %-15s %6.1f %-s\n",Nutrient[DHA],DV[DHA],Unit[DHA]);
printf("                              %-15s %6.1f %-s\n",Nutrient[CHOCDF],DV[CHOCDF],Unit[CHOCDF]);
printf("                              %-15s %6.1f %-s\n",Nutrient[FIBTG],DV[FIBTG],Unit[FIBTG]);
printf("                              %-15s %6.1f %-s\n",Nutrient[CHO_NONFIB],DV[CHO_NONFIB],Unit[CHO_NONFIB]);
printf("                              %-15s %6.1f %-s\n",Nutrient[PROCNT],DV[PROCNT],Unit[PROCNT]);
spacer(17);
printf("\nPress <enter> to continue...");
junk = get_int();
}

void restore_defaults()
{
options.autocal = 0;
options.wltweak = 0;
options.wlpolarity = 0;
options.n6hufa = 0;
options.abnuts[ENERC_KCAL] = 0;
options.abnuts[FAT] = 0;
options.abnuts[PROCNT] = 0;
options.abnuts[CHO_NONFIB] = 0;
options.abnuts[FIBTG] = 0;
options.abnuts[FASAT] = 0;
options.abnuts[FAPU] = 0;
auto_cal();
efa_dynamics();
write_OPTIONS();
}

void efa_dynamics(void)
{
int i;
float p3, p6, h3, h6, o, hufapct, ratio3, balancetarget = options.n6hufa;
float nonaalong, nonlashort, nonepadhalong, nonalashort;
float pufareductionfactor, pufatarget, ratio;
float increment, pincrement, hincrement, savep3;
float kludgefactor = 0.0;

if (options.comparison || meal_count(options.temp_meal_root) == 0) return;
if (balancetarget == 0) balancetarget = 50;
else if (balancetarget > 90) balancetarget = 90 - kludgefactor;
else if (balancetarget < 15) balancetarget = 15 - kludgefactor;
balancetarget += kludgefactor;

if (food_work.nutrient[FAPU] > 0) pufareductionfactor = DV[FAPU] / food_work.nutrient[FAPU];
else pufareductionfactor = 1;

p3 = 900 * food_work.nutrient[SHORT3] / DV[ENERC_KCAL];
p6 = 900 * food_work.nutrient[SHORT6] / DV[ENERC_KCAL];
h3 = 900 * food_work.nutrient[LONG3] / DV[ENERC_KCAL];
h6 = 900 * food_work.nutrient[LONG6] / DV[ENERC_KCAL];
savep3 = p3;

o  = 900 * (DV[FAPU] + DV[FAMS] + DV[FASAT] + food_work.nutrient[FATRN] - food_work.nutrient[SHORT6] - food_work.nutrient[LONG6] - food_work.nutrient[SHORT3] - food_work.nutrient[LONG3]) / DV[ENERC_KCAL];

hufapct = n6hufa(p3,p6,h3,h6,o,DV[ENERC_KCAL]);
pufatarget = (p3 + p6 + h3 + h6) * pufareductionfactor;
if (pufatarget <= 0) pufatarget = 4;
if (options.abnuts[FAPU] > 0) pufatarget = 900 * options.abnuts[FAPU] / DV[ENERC_KCAL];

if (h3 + h6 < .01)
 {
 increment = pufatarget / 2500;
 p6 = pufatarget;
 p3 = 0;

 for (i = 1; i < 2500; i++)
  {
  p3 += increment;
  p6 -= increment;
  hufapct = n6hufa(p3,p6,h3,h6,o,DV[ENERC_KCAL]);
  if (hufapct < balancetarget) break;
  }                             
 }

else
 {
 h3 = pufatarget - h6 - p6 - p3;
 if (h3 < 0)
  {
  p3 += h3;
  h3 = 0;
  if (food_work.nutrient[LA] > 0 && food_work.nutrient[AA] > 0) ratio = food_work.nutrient[AA] / food_work.nutrient[LA];
  else ratio = h6 / p6;
  if (ratio > 1)
   {
   if (food_work.nutrient[LA] > 0 && food_work.nutrient[AA] > 0) ratio = food_work.nutrient[LA] / food_work.nutrient[AA];
   else ratio = p6 / h6;
   p6 += ratio * p3;
   h6 += (1 - ratio) * p3;
   if (food_work.nutrient[ALA] > 0) p3 = savep3;
   else p3 = .72;
   if (p3 > 2) p3 = .72;
   p6 -= ratio * p3;
   h6 -= (1 - ratio) * p3;
   }
  else
   {
   h6 += ratio * p3;
   p6 += (1 - ratio) * p3;
   if (food_work.nutrient[ALA] > 0) p3 = savep3;
   else p3 = .72;
   if (p3 > 2) p3 = .72;
   h6 -= ratio * p3;
   p6 -= (1 - ratio) * p3;
   }
  }
 hincrement = h6 / 2500;
 pincrement = p6 / 2500;
 increment = hincrement + pincrement;
 hufapct = n6hufa(p3,p6,h3,h6,o,DV[ENERC_KCAL]);
 if (hufapct < balancetarget) for (i = 1; i < 2500; i++)
  {
  h6 += hincrement;
  p6 += pincrement;
  h3 -= increment;
  hufapct = n6hufa(p3,p6,h3,h6,o,DV[ENERC_KCAL]);
  if (hufapct > balancetarget) break;
  }
 else if (hufapct > balancetarget) for (i = 1; i < 2500; i++)
  {
  h6 -= hincrement;
  p6 -= pincrement;
  h3 += increment;
  hufapct = n6hufa(p3,p6,h3,h6,o,DV[ENERC_KCAL]);
  if (hufapct < balancetarget) break;
  }
 }

if (food_work.nutrient[EPA] > 0 && food_work.nutrient[DHA] > 0) ratio3 = food_work.nutrient[EPA] / (food_work.nutrient[DHA] + food_work.nutrient[EPA]);
else ratio3 = 0.5;
nonaalong = food_work.nutrient[LONG6] - food_work.nutrient[AA];
nonlashort = food_work.nutrient[SHORT6] - food_work.nutrient[LA];
nonepadhalong = food_work.nutrient[LONG3] - food_work.nutrient[EPA] - food_work.nutrient[DHA];
nonalashort = food_work.nutrient[SHORT3] - food_work.nutrient[ALA];

DV[OMEGA6] = (h6 + p6) * DV[ENERC_KCAL] / 900;
DV[LA] = (p6 * DV[ENERC_KCAL] / 900) - nonlashort;
DV[AA] = (h6 * DV[ENERC_KCAL] / 900) - nonaalong;
DV[OMEGA3] = (h3 + p3) * DV[ENERC_KCAL] / 900;
DV[ALA] = (p3 * DV[ENERC_KCAL] / 900) - nonalashort;
DV[EPA] = ratio3 * ((h3 * DV[ENERC_KCAL] / 900) - nonepadhalong);
DV[DHA] = (1 - ratio3) * ((h3 * DV[ENERC_KCAL] / 900) - nonepadhalong);

if ((DV[LA] / food_work.nutrient[LA] < 1.01 && DV[LA] / food_work.nutrient[LA] > 1) || (DV[AA] / food_work.nutrient[AA] < 1.01 && DV[AA] / food_work.nutrient[AA] > 1 ))
 {
 DV[LA] = food_work.nutrient[LA];
 DV[AA] = food_work.nutrient[AA];
 DV[OMEGA6] = food_work.nutrient[OMEGA6];
 }

if (DV[LA] > 0) DV[AA] = food_work.nutrient[AA] * DV[LA] / food_work.nutrient[LA];

if (DV[OMEGA6] != DV[OMEGA6]) DV[OMEGA6] = 0.01;
if (DV[LA] != DV[LA]) DV[LA] = 0.01;
if (DV[AA] != DV[AA]) DV[AA] = 0.01;
if (DV[OMEGA3] != DV[OMEGA3]) DV[OMEGA3] = 0.01;
if (DV[ALA] != DV[ALA]) DV[ALA] = 0.01;
if (DV[EPA] != DV[EPA]) DV[EPA] = 0.01;
if (DV[DHA] != DV[DHA]) DV[DHA] = 0.01;
if (DV[OMEGA6] < 0.01) DV[OMEGA6] = 0.01;
if (DV[LA] < 0.01) DV[LA] = 0.01;
if (DV[AA] < 0.01) DV[AA] = 0.01;
if (DV[OMEGA3] < 0.01) DV[OMEGA3] = 0.01;
if (DV[ALA] < 0.01) DV[ALA] = 0.01;
if (DV[EPA] < 0.01) DV[EPA] = 0.01;
if (DV[DHA] < 0.01) DV[DHA] = 0.01;
DVnotOK = 0;
}

void weight_log()
{
char newentry[128], meal_date[9];
int n, nfat, justmadenewentry = 0, infloop = 1;
float weightslope, fatslope, weightyintercept, fatyintercept, earliestrdate;
float weight, bf, leanslope;
time_t t;

while (infloop)
 {
 header("NUT:  Weight Log Regression");
 read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
 leanslope = weightslope - fatslope;
 if (justmadenewentry && options.autocal > 0 && nfat > 1)
  {
  if (!options.wlpolarity)
   {
   if (fatslope > 0)
    {
    options.abnuts[ENERC_KCAL] -= 20;
    options.wltweak = 1;
    }
   else if (fatslope < 0 && leanslope < 0)
    {
    options.abnuts[ENERC_KCAL] += 20;
    options.wltweak = 1;
    }
   else if (fatslope < 0 && leanslope > 0 && options.wltweak)
    {
    options.wltweak = 0;
    options.wlpolarity = 1;
    weight = 0;
    options.lastwlw = weightyintercept;
    options.lastwlbfp = 100 * fatyintercept / weightyintercept;
    if (n > 0) write_WLOG(&weight,&bf,meal_date);
    read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
    leanslope = weightslope - fatslope;
    }
   }
  else
   {
   if (fatslope > 0 && leanslope > 0)
    {
    options.abnuts[ENERC_KCAL] -= 20;
    options.wltweak = 1;
    }
   else if (leanslope < 0)
    {
    options.abnuts[ENERC_KCAL] += 20;
    options.wltweak = 1;
    }
   else if (fatslope < 0 && leanslope > 0 && options.wltweak)
    {
    options.wltweak = 0;
    options.wlpolarity = 0;
    weight = 0;
    options.lastwlw = weightyintercept;
    options.lastwlbfp = 100 * fatyintercept / weightyintercept;
    if (n > 0) write_WLOG(&weight,&bf,meal_date);
    read_WLOG(&n,&nfat,&weightslope,&fatslope,&weightyintercept,&fatyintercept,&earliestrdate);
    leanslope = weightslope - fatslope;
    }
   }
  auto_cal();
  write_OPTIONS();
  }
 justmadenewentry = 0;
 printf("                                data points:      %6d\n",n);
 printf("\n");
 printf("       Based on the trend\n");
 printf("       of data points so far...\n\n");
 if (n < 2) printf("              Predicted daily weight change:           ?\n\n");
 else printf("              Predicted daily weight change:      %+10.3f\n\n",weightslope);
 if (nfat < 2)
  {
  printf("           Predicted daily lean mass change:           ?\n\n");
  printf("            Predicted daily fat mass change:           ?\n\n");
  printf("                     Predicted weight today:           ?\n\n");
  printf("                  Predicted lean mass today:           ?\n\n");
  printf("                   Predicted fat mass today:           ?\n\n\n\n\n");
  }
 else
  {
  printf("           Predicted daily lean mass change:      %+10.3f\n\n",weightslope-fatslope);
  printf("            Predicted daily fat mass change:      %+10.3f\n\n",fatslope);
  printf("                     Predicted weight today:      %8.1f\n\n",weightyintercept);
  printf("                  Predicted lean mass today:      %8.1f\n\n",weightyintercept-fatyintercept);
  printf("                   Predicted fat mass today:      %8.1f    (%0.1f%%)\n\n",fatyintercept,100*fatyintercept/weightyintercept);
  if (options.autocal > 0)
   {
   if (leanslope > 0 && fatslope > 0) printf("\n       Gained %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.\n       Calorie Auto-Set Mode = Cutting.\n",-(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",-(earliestrdate * fatslope));
   else if (leanslope > 0 && fatslope < 0) printf("\n       Gained %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.\n       Calorie Auto-Set Mode = Bulking.\n",-(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",(earliestrdate * fatslope));
   else if (leanslope < 0 && fatslope < 0) printf("\n       Lost %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.\n       Calorie Auto-Set Mode = Bulking.\n",(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",(earliestrdate * fatslope));
   else if (leanslope < 0 && fatslope > 0 && !options.wlpolarity) printf("\n       Lost %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.\n       Calorie Auto-Set Mode = Cutting!\n",(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",-(earliestrdate * fatslope));
   else if (leanslope < 0 && fatslope > 0 && options.wlpolarity) printf("\n       Lost %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.\n       Calorie Auto-Set Mode = Bulking!\n",(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",-(earliestrdate * fatslope));
   else printf("\n\n\n");
   }
  else
   {
   if (leanslope > 0 && fatslope > 0) printf("\n       Gained %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.\n\n",-(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",-(earliestrdate * fatslope));
   else if (leanslope > 0 && fatslope < 0) printf("\n       Gained %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.\n\n",-(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",(earliestrdate * fatslope));
   else if (leanslope < 0 && fatslope < 0) printf("\n       Lost %0.3f lean mass over %0.0f %s and lost %0.3f fat mass.\n\n",(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",(earliestrdate * fatslope));
   else if (leanslope < 0 && fatslope > 0) printf("\n       Lost %0.3f lean mass over %0.0f %s and gained %0.3f fat mass.\n\n",(earliestrdate * leanslope),-(earliestrdate), -earliestrdate == 1 ? "day" : "days",-(earliestrdate * fatslope));
   else printf("\n\n\n");
   }
  }
 spacer(19);
 today(meal_date);
 if (strcmp(meal_date,options.lastwldate) != 0)
  {
 if (n > 0 && options.autocal == 0) printf("Type daily weight and body fat %% (! to clear log, <enter> to quit):  ");
 else if (n > 0 && options.autocal > 0) printf("Type daily weight and body fat %% (<enter> to quit):  ");
 else printf("Type daily weight and body fat %% (<enter> to quit):  ");
  }
else
  {
 if (n > 0 && options.autocal == 0) printf("Type ! to clear log, <enter> to quit:  ");
 else printf("Press <enter> to quit:  ");
  }
 get_string(newentry,127);
 if (strlen(newentry) == 0 || newentry[0] == ' ') return;
 if (strchr(newentry,'!') != NULL)
  {
  weight = 0;
  if (n > 0) write_WLOG(&weight,&bf,meal_date);
  }
 else
  {
  time(&t);
  if (strchr(newentry,' ') == NULL || newentry[strlen(newentry)-1] == ' ')
   {
   weight   = atof(strtok(newentry," "));
   bf = 0;
   }
  else
   {
   weight   = atof(strtok(newentry," "));
   bf       = atof(strtok(NULL," "));
   justmadenewentry = 1;
   }
  today(meal_date);
  if (weight > 0) write_WLOG(&weight,&bf,meal_date);
  }
 }
}

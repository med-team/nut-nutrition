/* main.c */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "menu.h"
#include "db.h"
#include "options.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
food_root.next = NULL;
meal_root.next = NULL;
recipe_root.next = NULL;
theusual_root.next = NULL;
initializations();
if (argc > 1) make_filenames(argv[1]);
else make_filenames("");
if ( version(0) == MAJVERSION && version(1) == MINVERSION && read_food_db() )
 {
 read_meal_db();
 read_theusual_db();
 read_OPTIONS(0);
 }
else read_food_files();
if (version(0) != MAJVERSION || version(1) != MINVERSION) write_version();
menu();
meal_export();
exit(0);
}

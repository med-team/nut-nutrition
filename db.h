/* db.h */

/*
    NUT nutrition software 
    Copyright (C) 1996-2014 by Jim Jozwiak.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DB_H
#define DB_H

#define MAJVERSION 20
#define MINVERSION 1
#define VERSIONQ "20.1"
#define USDAVERSION "SR27"
#define USDAVERSIONLC "sr27"

#include "food.h"

extern char subuser[];
extern char nutdir[];

#ifdef __cplusplus
extern "C" {
#endif

void read_food_files(void);
void read_joined_db(char *);
void read_FOOD(char *);
void read_NUT(char *);
void read_WEIGHT(char *);
void read_weightlib(int, char *, int, char **, int *);
void write_food_db(void);
int read_food_db(void);
void read_meal_db(void);
void read_theusual_db(void);
void write_meal_db(void);
void write_theusual_db(void);
void write_OPTIONS(void);
void write_recipe(struct food *);
void write_serving(struct food *);
void read_OPTIONS(int);
void read_WLOG(int *, int *, float *, float *, float *, float *, float *);
void write_WLOG(float *, float *, char *);
void make_filenames(const char *);
int fontsize(void);
void write_fontsize(int);
int version(int);
void write_version(void);
void meal_export(void);

#ifdef __cplusplus
} /* closing brace for extern "C" */
#endif

#endif
